<?php

namespace App\Console\Commands;

use App\Models\Notification;

use Illuminate\Console\Command;

class NotificationDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Delete Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $last = \Carbon\Carbon::today()->subDays(7);
        $notifications = Notification::whereDate('date', '<', $last)->delete();
        $this->info('Notification deleted at ' . \Carbon\Carbon::now());
    }
}
