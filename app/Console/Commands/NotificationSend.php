<?php

namespace App\Console\Commands;

use App\Models\Notification;
use App\Notifications\SendPushNotification;

use Illuminate\Console\Command;

class NotificationSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Send Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Notification started at ' . \Carbon\Carbon::now());

        $now = \Carbon\Carbon::now();
        $notifications = Notification::with('receivers')
            ->where('is_send', '0')
            ->where('date', '<=', $now)
            ->with('receivers')->get();

        foreach ($notifications as $notification) {
            $tTopic = '';
            foreach ($notification->receivers as $receiver) {
                /* It's to prevent duplicate notification. */
                if ($tTopic == $receiver->topic) {
                    continue;
                }
                $tTopic = $receiver->topic;
                $notifdata = [
                    'topic' => $tTopic,
                    'notification' => [
                        'title' => $notification->title,
                        'body'  => $notification->desc,
                    ],
                ];

                try {
                    new SendPushNotification($notifdata);
                } catch (\Throwable $th) {
                    $this->info('Error at ' . $th);
                }

                $notification->is_send = 1;
                $notification->save();
            }
        }

        $this->info('Notification sended at ' . \Carbon\Carbon::now());
    }
}
