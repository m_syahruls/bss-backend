<?php

namespace App\Console\Commands;

use App\Http\Controllers\Notification\NotificationController;
use App\Models\Schedule;
use App\Whatsapp\SendWhatsappNotification;
use Carbon\Carbon;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use stdClass;

class ScheduleReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule reminder for H-2, H-1, The H day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Schedule started at ' . Carbon::now());

        // get time now for imidiate notification
        $now = Carbon::now();
        $targetTime    = $now->format('H:i');

        // get time for target
        $targetH    = Carbon::now()->addHours(2);
        $targetTimeH = $targetH->format('H:i');     // because of sooner time

        $targetH1   = Carbon::now()->addDay();
        $targetH2   = Carbon::now()->addDays(2);


        $schedulesH     = Schedule::whereDate('date', $targetH)->where('time', 'like', $targetTimeH . '%')->get();
        $schedulesH1    = Schedule::whereDate('date', $targetH1)->where('time', 'like', $targetTime . '%')->get();
        $schedulesH2    = Schedule::whereDate('date', $targetH2)->where('time', 'like', $targetTime . '%')->get();

        $whatsappdata = [];
        $w = 0;
        $notifdata = [];

        // ! Can't do loop queries due to different Notifications body
        // loop for schedules queries
        foreach ($schedulesH as $schedule) {
            // loop for schedules students
            foreach ($schedule->Group->Students as $student) {
                // whatsapp

                $whatsappdata[$w++] = [
                    'target' => $student->mother_phone . ',' . $student->father_phone,
                    //'type' => 'text',
                    'message' => 'Today, ' . date('l d F Y', strtotime($schedule->date)) . ' is the day of ' . $schedule->Group->name . ' ' . $schedule->title . ' for ' . $student->nick_name,
                    'schedule' => '0'
                ];
            }

            $tNotif = new stdClass;
            $tNotif->title      = '🔥 ' . $schedule->title . ' 🔥 Check your schedule';
            $tNotif->desc       = 'Today is the day of ' . $schedule->title;
            $tNotif->date       = $now;
            $tNotif->topic      = ['GROUP_' . $schedule->Group->id, 'ADMIN'];   // add ADMIN topic
            $tNotif->receiver   = [$schedule->Group->id, ''];                   // add admin as empty

            array_push($notifdata, $tNotif);
        }

        foreach ($schedulesH1 as $schedule) {
            // loop for schedules students
            foreach ($schedule->Group->Students as $student) {
                // whatsapp
                $whatsappdata[$w++] = [
                    'target' => $student->mother_phone . ',' . $student->father_phone,
                    //'type' => 'text',
                    'message' => '1 day to go before ' . $schedule->title . ' for ' . $student->nick_name,
                    'schedule' => '0'
                ];
            }

            $tNotif = new stdClass;
            $tNotif->title      = '🔥 ' . $schedule->title . ' 🔥';
            $tNotif->desc       = 'Check your schedule, 1 day to go for the ' . $schedule->title;
            $tNotif->date       = $now;
            $tNotif->topic      = ['GROUP_' . $schedule->Group->id, 'ADMIN'];   // add ADMIN topic
            $tNotif->receiver   = [$schedule->Group->id, ''];                   // add admin as empty

            array_push($notifdata, $tNotif);
        }

        foreach ($schedulesH2 as $schedule) {
            // loop for schedules students
            foreach ($schedule->Group->Students as $student) {
                // whatsapp
                $whatsappdata[$w++] = [
                    'target' => $student->mother_phone . ',' . $student->father_phone,
                    //'type' => 'text',
                    'message' => '2 days to go before ' . $schedule->title . ' for ' . $student->nick_name,
                    'schedule' => '0'
                ];
            }

            $tNotif = new stdClass;
            $tNotif->title      = '🔥 ' . $schedule->title . ' 🔥';
            $tNotif->desc       = 'Check your schedule, 2 days to go for the ' . $schedule->title;
            $tNotif->date       = $now;
            $tNotif->topic      = ['GROUP_' . $schedule->Group->id, 'ADMIN'];   // add ADMIN topic
            $tNotif->receiver   = [$schedule->Group->id, ''];                   // add admin as empty

            array_push($notifdata, $tNotif);
        }

        $this->info('Schedule queried at ' . Carbon::now());

        // create notif and send wa
        foreach ($notifdata as $request) {
            // documentation in  app/Controllers/Admin/AdminNotificationController
            $data = new Request([
                'title'     => $request->title,
                'desc'      => $request->desc,
                'date'      => $request->date,
                'topic'     => $request->topic,
                'receiver'  => $request->receiver,
                'category'  => 'AUTOMATED',
            ]);

            try {
                $notification = new NotificationController;
                $result = $notification->createNotification($data);

                if ($result) {
                    $this->info('Error sending notification ' . json_encode($result));
                }
            } catch (\Throwable $th) {
                $this->info('Error sending notification ' . $th);
            }
        }

        foreach ($whatsappdata as $data) {
            try {
                new SendWhatsappNotification($data);
            } catch (\Throwable $th) {
                $this->info('Error sending whatsapp ' . $th);
            }
        }

        $this->info('Schedule sended at ' . Carbon::now());
    }
}
