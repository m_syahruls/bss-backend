<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Coach;
use App\Models\Group;
use App\Models\Notification;
use App\Models\NotificationReceiver;
use App\Models\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * The above function is used to create a notification.
     *
     * @param Request request the request object
     */
    /**
     * it's internal controller, no need endpoint documentation
     *
     * This block code is template for another create notification
     * create request for easier validation
     * $request = new Request([
     *      'admins_id' => $user_auth->Admin->id,    // int, optional
     *      'title'     => $request->title,         // string
     *      'desc'      => $request->desc,          // string
     *      'date'      => $request->date,          // datetime, if immidiate use \Carbon\Carbon::now();
     *      'topic'     => $request->topic,         // array, [COACH, COACH_ID_X, STUDENT, STUDENT_ID_X, GROUP_Y] X = users_id Y = groups_id
     *      'receiver'  => $request->receiver,      // array, [1,2,3,4,5,6,7,8,9,0] users_id
     *      'category'  => 'ANNOUNCMENT',           // string, "ANNOUNCMENT/AUTOMATED"
     * ]);
     *
     * */
    public function createNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|string',
            'desc'          => 'required|string',
            'date'          => 'required|date',
            'topic'         => 'required',
            'receiver'      => 'required',
            'category'      => 'required|string|in:ANNOUNCMENT,AUTOMATED',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'Data'      => $validator->errors(),
                ],
                400
            );
        }

        /* This is to check if the request is an array or not. If it is an array, it will be stored in
        the variable. If it is not an array, it will be stored in an array. */
        try {
            $topics = $request->topic;
            $receivers = $request->receiver;
            $countTopics = count($request->topic);
        } catch (\Throwable $th) {
            $topics = [$request->topic];
            $receivers = [$request->receiver];
            $countTopics = 1;
        }

        try {
            // store new notification
            $notification = new Notification();
            $notification->admins_id    = $request->admins_id;
            $notification->title        = $request->title;
            $notification->desc         = $request->desc;
            $notification->date         = $request->date;
            $notification->category     = $request->category;
            $notification->save();

            for ($i = 0; $i < $countTopics; $i++) {
                if (str_contains($topics[$i], 'ID')) {
                    // store for TOPIC with ID
                    $notifReceiver           = new NotificationReceiver();
                    $notifReceiver->user_id  = $receivers[$i];
                    $notifReceiver->topic    = $topics[$i];
                    $notification->receivers()->save($notifReceiver);
                } elseif (str_contains($topics[$i], 'GROUP')) {
                    // store for GROUP TOPIC
                    $group = Group::where('id', $receivers[$i])->with('Students')->with('Coaches')->first();

                    if (!$group) {
                        return response()->json([
                            'Status' => false,
                            'Message' => 'Group not found',
                        ], 404);
                    }

                    $users = [];
                    foreach ($group->Students as $student) {
                        array_push($users, $student->users_id);
                    }
                    foreach ($group->Coaches as $coach) {
                        array_push($users, $coach->users_id);
                    }

                    foreach ($users as $user) {
                        $notifReceiver           = new NotificationReceiver();
                        $notifReceiver->user_id  = $user;
                        $notifReceiver->topic    = $topics[$i];
                        $notification->receivers()->save($notifReceiver);
                    }
                } elseif ($topics[$i] == 'STUDENT') {
                    // store for STUDENT TOPIC
                    $students = Student::all();
                    foreach ($students as $student) {
                        $notifReceiver           = new NotificationReceiver();
                        $notifReceiver->user_id  = $student->users_id;
                        $notifReceiver->topic    = $topics[$i];
                        $notification->receivers()->save($notifReceiver);
                    }
                } elseif ($topics[$i] == 'COACH') {
                    // store for COACH TOPIC
                    $coaches = Coach::all();
                    foreach ($coaches as $coach) {
                        $notifReceiver           = new NotificationReceiver();
                        $notifReceiver->user_id  = $coach->users_id;
                        $notifReceiver->topic    = $topics[$i];
                        $notification->receivers()->save($notifReceiver);
                    }
                } elseif ($topics[$i] == 'ADMIN') {
                    // store for ADMIN TOPIC
                    $admins = Admin::all();
                    foreach ($admins as $admin) {
                        $notifReceiver           = new NotificationReceiver();
                        $notifReceiver->user_id  = $admin->users_id;
                        $notifReceiver->topic    = $topics[$i];
                        $notification->receivers()->save($notifReceiver);
                    }
                }
            }

            // immediate check schedule:run
            // Artisan::call('schedule:run');
            // schedule:run make multiple schedule:reminder, now bypass to notification:send
            Artisan::call('notification:send');
            // !PLS FIX UR CRON JOB TO RUN EVERYMINUTE as supposed to do

            // !REVERSE RETURN CONDITION
            // fail validator return true because of there is data to carry
            // successfull create notification return false because of no need to return any data
            return false;
        } catch (\Throwable $th) {
            return response()->json(
                [
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'Data'      => $th,
                ],
                400
            );
        }
    }
}
