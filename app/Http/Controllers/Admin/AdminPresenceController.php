<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Presence;
use App\Models\Schedule;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminPresenceController extends Controller
{
    /**
     * The above function is used to get the presence of students.
     *
     * @param Request request The request object.
     *
     * @return Presence found
     */
    /**
     * @OA\Tag(
     *     name="Admin/Presence",
     *     description="API Endpoints of Admin Presences"
     * )
     *
     * @OA\Get(
     *      path="/admin/precense/get",
     *      tags={"Admin/Presence"},
     *      summary="Get all/selected precense",
     *      description="An endpoint that response all or selected Presence based on id",
     *      operationId="GetPresenceAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="id_presence",
     *          description="id_presence to get specific precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="students_id",
     *          description="students_id to get specific precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="schedules_id",
     *          description="schedules_id to get specific precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="groups_id",
     *          description="groups_id to get specific precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="year",
     *          description="year to get specific precense",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          description="month to get specific precense",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="day",
     *          description="day to get specific precense",
     *          example="20",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging precense",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Presence found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getpresence(Request $request)
    {
        try {
            $year = $request->year;
            $month = $request->month;
            $day = $request->day;

            $id_presence = $request->id_presence;
            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $groups_id = $request->groups_id;
            $admin = $request->admin;
            $id = $request->id;

            $limit = $request->limit;

            $presence = new Student;

            if ($id_presence) {
                $presenceid = Presence::where('id', $id_presence);
                $presence = $presence->whereIn('id', $presenceid->pluck('students_id'));
            }

            if ($id) {
                $presence = Presence::with('Schedule')->where('id', $id);
            }

            if ($students_id) {
                $presence = $presence->where('id', $students_id);
            }

            if ($groups_id) {
                $presence = $presence->where('groups_id', $groups_id);
            }

            if ($year && $month && $day) {
                // $presencedate = Presence::whereDay('created_at', $day)->whereMonth('created_at', $month)->whereYear('created_at', $year);
                $presence = $presence->with(['Presence' => function ($query) use ($year, $month, $day) {
                    $query->whereYear('created_at', $year)->whereMonth('created_at', $month)->whereDay('created_at', $day);
                }]);
            }

            if ($schedules_id) {
                $presence = $presence->with(['Presence' => function ($query) use ($schedules_id) {
                    $query->where('schedules_id', $schedules_id);
                }]);
            }

            if (!$presence->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Presence not found',
                ], 200);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $presence->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Presence found',
                'Data' => $presence->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create a presence.
     *
     * @param Request request the request object
     *
     * @return It returns the presence data.
     */
    /**
     * @OA\Post(
     *      path="/admin/presence/create",
     *      tags={"Admin/Presence"},
     *      summary="Create new presence",
     *      description="An endpoint that create new Presence",
     *      operationId="CreatePresence",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="students_id", description="students_id of selected presence", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="schedules_id", description="schedules_id of selected presence", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="temperature", description="temperature of selected presence", example="36.5", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="time_attended", description="time_attended of selected presence", example="10:00", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="time",
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Presence created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createpresence(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'students_id'   => 'required',
                'schedules_id'  => 'required',
                'temperature'   => 'required',
                'time_attended' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status'    => false,
                        'Message'   => 'Something wrong',
                        'Data'      => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $admins_id = User::with('Admin')->where('id', Auth::id())->first();
            $temperature = $request->temperature;
            $time_attended = $request->time_attended;
            $is_attended = $request->is_attended;

            $date_now = date('Y-m-d');

            $jadwal = Schedule::where('id', $schedules_id)->first();

            if ($jadwal->date != $date_now) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Cant create presence',
                ], 400);
            }

            $presence = Presence::create([
                'students_id'   => $students_id,
                'schedules_id'  => $schedules_id,
                'admins_id'     => $admins_id->Admin->id,
                'temperature'   => $temperature,
                'time_attended' => $time_attended,
                'is_attended'   => $is_attended,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Presence created successfully',
                'data_old'          => null,
                'data_new'          => $presence,
            ]);

            // push notif
            // documentation in  app/Controllers/Admin/AdminNotificationController
            $data = new Request([
                'title'     => 'Dear ' . $presence->Student->nick_name,
                'desc'      => "Here is your today's attendance log",
                'date'      => \Carbon\Carbon::now(),
                'topic'     => 'STUDENT_ID_' . $presence->Student->users_id,
                'receiver'  => $presence->Student->users_id,
                'category'  => 'AUTOMATED',
            ]);

            try {
                $notification = new NotificationController;
                $result = $notification->createNotification($data);
                if ($result) {
                    return $result;
                }
            } catch (\Throwable $th) {
                return response()->json([
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'error'     => $th,
                ]);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Presence create successfully',
                'Data' => $presence,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Presence create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the presence of a student.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/presence/update",
     *      tags={"Admin/Presence"},
     *      summary="Update selected presence",
     *      description="An endpoint that update existing Presence based on id",
     *      operationId="UpdatePresence",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected presence", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="students_id", description="students_id of selected presence", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="schedules_id", description="schedules_id of selected presence", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="temperature", description="temperature of selected presence", example="36.5", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="time_attended", description="time_attended of selected presence", example="10:00", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="time",
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Presence updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Presence not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatepresence(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $presence = Presence::where('id', $id)->first();
            if (!$presence) {
                return response()->json([
                    'Message' => 'Presence not found'
                ], 404);
            }
            /* Used to create a copy of the presence object. */
            $tempPresence = $presence->replicate();

            $validator = Validator::make($request->all(), [
                'students_id'   => 'required',
                'schedules_id'  => 'required',
                'temperature'   => 'required',
                'time_attended' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $admins_id = User::with('Admin')->where('id', Auth::id())->first();
            $temperature = $request->temperature;
            $time_attended = $request->time_attended;
            $is_attended = $request->is_attended;


            $presence->update([
                'students_id'   => $students_id,
                'schedules_id'  => $schedules_id,
                'admins_id'     => $admins_id->Admin->id,
                'temperature'   => $temperature,
                'time_attended' => $time_attended,
                'is_attended'   => $is_attended,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Presence updated successfully',
                'data_old'          => $tempPresence,
                'data_new'          => $presence,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $presence,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Presence update failed',
                'data_old'          => $presence,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    public function createpresencemanual(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'students_id'   => 'required',
                'schedules_id'  => 'required',
                'temperature'   => 'required',
                'time_attended' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status'    => false,
                        'Message'   => 'Something wrong',
                        'Data'      => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $admins_id = $request->admins_id;
            $coaches_id = $request->coaches_id;
            $temperature = $request->temperature;
            $time_attended = $request->time_attended;
            $is_attended = $request->is_attended;
            $created_at = $request->created_at;


            $presence = Presence::create([
                'students_id'   => $students_id,
                'schedules_id'  => $schedules_id,
                'admins_id'     => $admins_id,
                'coaches_id'    => $coaches_id,
                'temperature'   => $temperature,
                'time_attended' => $time_attended,
                'is_attended'   => $is_attended,
                'created_at'    => $created_at,
            ]);


            return response()->json([
                'Status' => true,
                'Message' => 'Presence create successfully',
                'Data' => $presence,
            ]);
        } catch (\Exception $error) {

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    public function deletepresencemanual(Request $request)
    {
        try {

            $id = $request->id;

            $presence = Presence::where('id', $id)->first();
            if (!$presence) {
                return response()->json([
                    'Message' => 'Presence not found'
                ], 404);
            }

            $presence->delete();

            return response()->json([
                'Status' => true,
                'Message' => 'Presence deleted successfully',
            ]);
        } catch (\Exception $error) {

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
