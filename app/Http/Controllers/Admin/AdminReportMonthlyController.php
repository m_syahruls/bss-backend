<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Controller;
use App\Models\ReportMonthlys;
use App\Models\ReportMonthlySkill;
use App\Models\ReportMonthlyRolePositions;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminReportMonthlyController extends Controller
{
    /**
     * The above function is used to get the report monthly.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/ReportMonthly",
     *     description="API Endpoints of Admin ReportMonthly"
     * )
     *
     * @OA\Get(
     *      path="/admin/report/monthly/get",
     *      tags={"Admin/ReportMonthly"},
     *      summary="Get all/selected report/monthly",
     *      description="An endpoint that response all or selected ReportMonthly based on id",
     *      operationId="GetReportMonthlyAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific report/monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="students_id",
     *          description="students_id to get specific report/monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="year",
     *          description="year to get specific report/monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          description="month to get specific report/monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging report/monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportMonthly found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getreportmonthly(Request $request)
    {
        try {
            $id = $request->id;
            $students_id = $request->students_id;
            $year = $request->year;
            $month = $request->month;
            $limit = $request->limit;

            $reportmonthly = ReportMonthlys::with('Admin:id,name')->with(['Student' => function ($query) {
                $query->with('User');
            }])->with('Coach:id,name', 'Report_monthly_skills')->with(['Report_monthly_role_positions' => function ($query) {
                $query->with('Role_position');
            }]);

            if ($id) {
                $reportmonthly = $reportmonthly->where('id', $id);

                if (!$reportmonthly->exists()) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Report monthly not found',
                    ], 200);
                }
            }

            if ($students_id) {
                $reportmonthly = $reportmonthly->where('students_id', $students_id);
            }

            if ($month) {
                $reportmonthly = $reportmonthly->where('month', $month);
            }

            if ($year) {
                $reportmonthly = $reportmonthly->where('year', $year);
            }

            if (!$reportmonthly->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report monthly not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Report monthly found',
                'Data' => $reportmonthly->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to create a report monthly.
     *
     * @param Request request the request object
     */
    /**
     * The above function is used to create an exam report.
     *
     * @param Request request the request object
     */
    /**
     * @OA\Post(
     *      path="/admin/report/monthly/create",
     *      tags={"Admin/ReportMonthly"},
     *      summary="Create new report/monthly",
     *      description="An endpoint that create new ReportMonthly",
     *      operationId="CreateReportMonthly",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="students_id", description="students_id of selected report/monthly", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="month", description="month of selected report/monthly", example="2", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="year", description="year  of selected report/monthly", example="2022", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportMonthly created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createreportmonthly(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'month' => 'required',
                'year'  => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $admins_id =  User::with('Admin')->where('id', Auth::id())->first();
            $comment = $request->comment;
            $month = $request->month;
            $year = $request->year;

            $students = Student::with('User')->where('id', $students_id)->first();

            $age = $students->category;

            $reportmonthlycheck = ReportMonthlys::where('students_id', $students_id)->where('month', $month)->where('year', $year)->first();
            if ($reportmonthlycheck) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report monthly already exists',
                ], 400);
            }

            $reportmonthlyskill  = ReportMonthlys::create([
                'students_id' => $students_id,
                'admins_id' => $admins_id->Admin->id,
                'month'     => $month,
                'year'      => $year,
                'age'       => $age,
            ]);


            $role_positions_id = $request->role_positions_id;



            foreach ($request->title as $key => $value) {
                $reportmonthlyskillarray = [
                    'report_monthlys_id' => $reportmonthlyskill->id,
                    'title'     => $request->title[$key],
                    'category'  => $request->category[$key],
                    'grade'     => $request->grade[$key],
                    'desc'      => $request->desc[$key],
                    'icon'      => $request->icon[$key],
                ];
                ReportMonthlySkill::create($reportmonthlyskillarray);
            }

            $loop = ["loop1", "loop2"];

            foreach ($loop as $key => $value) {
                $roleposition = [
                    'role_positions_id' => $role_positions_id[$key],
                    'report_monthlys_id' => $reportmonthlyskill->id,
                ];

                ReportMonthlyRolePositions::create($roleposition);
            }


            $reportmonthlyskill->update([
                'comment' => $comment,
            ]);


            $reportmonthlyresult = ReportMonthlys::with('Report_monthly_skills', 'Report_monthly_role_positions')->latest()->first();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly created successfully',
                'data_old'          => null,
                'data_new'          => $reportmonthlyresult,
            ]);

            // push notif
            // documentation in  app/Controllers/Admin/AdminNotificationController
            $data = new Request([
                'title'     => '⚽ Monthly REPORT ⚽',
                'desc'      => 'Your ' . \Carbon\Carbon::createFromDate($reportmonthlyskill->year, $reportmonthlyskill->month, 1)->format('F') . ' report is available!',
                'date'      => \Carbon\Carbon::now(),
                'topic'     => 'STUDENT_ID_' . $reportmonthlyskill->Student->users_id,
                'receiver'  => $reportmonthlyskill->Student->users_id,
                'category'  => 'AUTOMATED',
            ]);

            try {
                $notification = new NotificationController;
                $result = $notification->createNotification($data);
                if ($result) {
                    return $result;
                }
            } catch (\Throwable $th) {
                return response()->json([
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'error'     => $th,
                ]);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Report monthly created',
                'Data' => $reportmonthlyresult,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the report monthly.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/report/monthly/update",
     *      tags={"Admin/ReportMonthly"},
     *      summary="Update selected report/monthly",
     *      description="An endpoint that update existing ReportMonthly based on id",
     *      operationId="UpdateReportMonthly",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected report/monthly", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="students_id", description="students_id of selected report/monthly", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="month", description="month of selected report/monthly", example="2", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="year", description="year  of selected report/monthly", example="2022", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportMonthly updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="ReportMonthly not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatereportmonthly(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $reportmonthly = ReportMonthlys::where('id', $id)->with('Report_monthly_skills', 'Report_monthly_role_positions')->first();
            if (!$reportmonthly) {
                return response()->json([
                    'message' => 'Report Monthly not found!'
                ], 404);
            }
            /* Used to create a copy of the reportmonthly object. */
            $tempReportmonthly = $reportmonthly->replicate();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'month' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $admins_id =  User::with('Admin')->where('id', Auth::id())->first();
            $comment = $request->comment;
            $month = $request->month;
            $year = $request->year;

            $students = Student::with('User')->where('id', $students_id)->first();

            $age = $students->category;

            $reportmonthly->update([
                'students_id' => $students_id,
                'admins_id' => $admins_id->Admin->id,
                'month' => $month,
                'year' => $year,
                'age' => $age,
            ]);

            $reportmonthlyskillupdate = ReportMonthlySkill::where('report_monthlys_id', $reportmonthly->id)->get();


            foreach ($reportmonthlyskillupdate as $key => $value) {

                $reportmonthlyskillarray = [
                    'report_monthlys_id' => $reportmonthly->id,
                    'title' => $request->title[$key],
                    'category' => $request->category[$key],
                    'grade' => $request->grade[$key],
                    'desc' => $request->desc[$key],
                    'icon' => $request->icon[$key],
                ];
                ReportMonthlySkill::where('id', $value->id)->update($reportmonthlyskillarray);
            }

            $role_positions_id = $request->role_positions_id;

            $reportmonthlyroleposition = ReportMonthlyRolePositions::where('report_monthlys_id', $reportmonthly->id)->get();

            foreach ($reportmonthlyroleposition as $key => $value) {
                $rolepositionarray = [
                    'role_positions_id' => $role_positions_id[$key],
                    'report_monthlys_id' => $reportmonthly->id,
                ];

                ReportMonthlyRolePositions::where('id', $value->id)->update($rolepositionarray);
            }

            $reportmonthly->update([
                'comment' => $comment,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly updated successfully',
                'data_old'          => $tempReportmonthly,
                'data_new'          => $reportmonthly,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $reportmonthly,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly update failed',
                'data_old'          => $reportmonthly,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
