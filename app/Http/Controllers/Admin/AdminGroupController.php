<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachGroup;
use App\Models\Group;
use App\Models\Student;
use App\Models\Schedule;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminGroupController extends Controller
{

    /**
     * It returns a list of groups.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Group",
     *     description="API Endpoints of Admin Group Management"
     * )
     *
     * @OA\Get(
     *      path="/admin/group/get",
     *      tags={"Admin/Group"},
     *      summary="Get all/selected group",
     *      description="An endpoint that response all or selected Group based on id/groups_id",
     *      operationId="GetGroupAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific group",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging group",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Group found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Group not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    function getgroup(Request $request)
    {
        $id = $request->id;
        $limit = $request->limit;

        $group = Group::with('Coaches');
        if ($id) {
            $group = $group->where('id', $id);
        }


        if (!$group->exists()) {
            return response()->json([
                'Status' => false,
                'Message' => 'Group not found',
            ], 200);
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Group found',
            'Data' => $group->paginate($limit),
        ]);
    }

    /**
     * The above function is used to create a group.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/group/create",
     *      tags={"Admin/Group"},
     *      summary="Create new group",
     *      description="An endpoint that create new Group",
     *      operationId="CreateGroup",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="name", description="name of selected group", example="Group Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Group created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function creategroup(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;

            $cekgroup = Group::where('name', $name);

            if ($cekgroup->exists()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'class already taken',
                    ],
                    400
                );
            }

            $group = Group::create([
                'name' => $name,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group created successfully',
                'data_old'          => null,
                'data_new'          => $group,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Group create successfully',
                'Data' => $group,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the group.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/group/update",
     *      tags={"Admin/Group"},
     *      summary="Update selected group",
     *      description="An endpoint that update existing Group based on id",
     *      operationId="UpdateGroup",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected group", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected group", example="Group Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Group updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Group not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updategroup(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $group = Group::where('id', $id)->first();
            if (!$group) {
                return response()->json([
                    'Message' => 'Group not found'
                ], 404);
            }
            /* Used to create a copy of the group object. */
            $tempGroup = $group->replicate();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;

            $cekgroup = Group::where('name', $name);

            if ($cekgroup->exists()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'class already taken',
                    ],
                    400
                );
            }

            $group->update([
                'name' => $name,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group updated successfully',
                'data_old'          => $tempGroup,
                'data_new'          => $group,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $group,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group update failed',
                'data_old'          => $group,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to delete a group
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/group/delete",
     *      tags={"Admin/Group"},
     *      summary="Delete selected group",
     *      description="An endpoint that delete existing Group based on id",
     *      operationId="DeleteGroup",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected group", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Group deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Group not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deletegroup(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $group = Group::where('id', $id)->first();
            if (!$group) {
                return response()->json([
                    'Message' => 'Group not found'
                ], 404);
            }
            /* Used to create a copy of the group object. */
            $tempGroup = $group->replicate();

            $cekgroupstudent = Student::where('groups_id', $id);
            $cekgroupcoach = CoachGroup::where('groups_id', $id);
            $cekgroupschedule = Schedule::where('groups_id', $id);

            if ($cekgroupstudent->exists() || $cekgroupcoach->exists() || $cekgroupschedule->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Group already use on other table',
                ], 400);
            }

            $group->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group deleted successfully',
                'data_old'          => $tempGroup,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'group',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Group delete failed',
                'data_old'          => $group,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
