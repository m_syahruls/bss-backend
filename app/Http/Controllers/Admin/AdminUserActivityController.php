<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserActivity;

use Illuminate\Http\Request;

class AdminUserActivityController extends Controller
{
    /**
     * It gets the user activity from the database and returns it as a paginated response
     *
     * @param Request request The request object.
     *
     * @return The response is being returned in JSON format.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Activity",
     *     description="API Endpoints of Admin User Activities"
     * )
     *
     * @OA\Get(
     *      path="/admin/activity/get",
     *      tags={"Admin/Activity"},
     *      summary="Get all/selected user activities",
     *      description="An endpoint that response all or selected Activity based on id/date",
     *      operationId="GetActivityAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific activity",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging activity",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="start_date",
     *          description="start date to get all selected activity",
     *          example="2022-02-02",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_date",
     *          description="end date to get all selected activity",
     *          example="2022-02-22",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date",
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Activity found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Activity not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getActivity(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $start_date = $request->start_date;
            $end_date = $request->end_date;

            // query
            $activities = UserActivity::query();

            if ($id) {
                $activities = $activities->where('id', $id);
            }

            // this allow filter date one at time, or both
            if ($start_date) {
                $activities = $activities->where('created_at', '>=', $start_date);
            }

            if ($end_date) {
                $activities = $activities->where('created_at', '<=', $end_date);
            }

            /* If the limit is not set, then it will set the limit to the total number of activities. */
            if (!$limit) {
                $limit = $activities->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Activity found',
                'Data' => $activities->orderBy('created_at', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
