<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Study;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminStudyController extends Controller
{
    /**
     * This function is used to get the study data.
     *
     * @param Request request The request object.
     *
     * @return Study is being returned
     */
    /**
     * @OA\Tag(
     *     name="Admin/Study",
     *     description="API Endpoints of Admin Studies"
     * )
     *
     * @OA\Get(
     *     path="/admin/study/get",
     *     tags={"Admin/Study"},
     *     summary="Get study for admin",
     *     description="Get all study material for admin",
     *     operationId="getStudyAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="is_pdf",
     *          description="To choose whether pdf or not",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Study found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Study not found",
     *     ),
     * )
     */
    public function getstudy(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $coaches_id = $request->coaches_id;
            $groups_id = $request->groups_id;
            $month = $request->month;
            $year = $request->year;
            $admin = $request->admin;
            $is_pdf = $request->is_pdf;

            $study = Study::with('Admin:id,name,img_url', 'Coach:id,name,img_url', 'Group');

            if ($id) {
                $study = $study->where('id', $id);
            }

            if ($coaches_id) {
                $study = $study->where('coaches_id', $coaches_id);
            }

            if ($groups_id) {
                $study = $study->where('groups_id', $groups_id);
            }

            if ($month) {
                $study = $study->whereMonth('created_at', $month);
            }

            if ($year) {
                $study = $study->whereYear('created_at', $year);
            }

            if (!$study->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Study found',
                'Data' => $study->orderBy('created_at', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create a study.
     *
     * @param Request request The request object.
     *
     * @return A study is being created.
     */
    /**
     * @OA\Post(
     *     path="/admin/study/create",
     *     tags={"Admin/Study"},
     *     summary="Create study for admin",
     *     description="Create study for admin",
     *     operationId="createStudyAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Study create successfully",
     *     ),
     * )
     */
    public function createstudy(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'groups_id' => 'required',
                'title' => 'required',
                'desc' => 'required',
                'file_url' => 'required',
                'file_name' => 'required',
                'is_pdf' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $user = User::with('Admin')->where('id', Auth::id())->first();
            $groups_id = $request->groups_id;
            $title = $request->title;
            $desc = $request->desc;
            $file_name = $request->file_name;
            $is_pdf = $request->is_pdf;


            if ($is_pdf == 1) {
                $validator = Validator::make($request->all(), [
                    'file_url' => 'max:10240|mimes:pdf',
                ]);

                if ($validator->fails()) {
                    return response()->json(
                        [
                            'Status' => false,
                            'Message' => 'Something wrong',
                            'Data' => $validator->errors(),
                        ],
                        400
                    );
                }
                $imgstudy = $request->file('file_url');
                $namaimgstudy = $imgstudy->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/study',
                    $imgstudy,
                    $namaimgstudy
                );

                $path = config('app.url') . Storage::url($loc);
            } else if ($is_pdf == 0) {
                $file_url = $request->file_url;
                $path = $file_url;
                $namaimgstudy = $file_url;
            } else {
                $path = '';
                $namaimgstudy = '';
            }

            $study = Study::create([
                'admins_id' => $user->Admin->id,
                'groups_id' => $groups_id,
                'title' => $title,
                'desc' => $desc,
                'file_url' => $path,
                'file_name' => $namaimgstudy,
                'is_pdf' => $is_pdf,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study created successfully',
                'data_old'          => null,
                'data_new'          => $study,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Study create successfully',
                'Data' => $study,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the study.
     *
     * @param Request request The request object.
     *
     * @return a response in JSON format.
     */
    /**
     * @OA\Post(
     *     path="/admin/study/update",
     *     tags={"Admin/Study"},
     *     summary="Update study for admin",
     *     description="Update study for admin",
     *     operationId="updateStudyAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Study update successfully",
     *     ),
     * )
     */
    public function updatestudy(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $study = Study::where('id', $id)->first();
            if (!$study) {
                return response()->json([
                    'message' => 'Schedule not found'
                ], 404);
            }
            /* Used to create a copy of the study object. */
            $tempStudy = $study->replicate();

            $validator = Validator::make($request->all(), [
                'groups_id' => 'required',
                'title' => 'required',
                'desc' => 'required',
                'file_name' => 'required',
                'is_pdf' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $user = User::with('Admin')->where('id', Auth::id())->first();
            $groups_id = $request->groups_id;
            $title = $request->title;
            $desc = $request->desc;
            $file_url = $request->file_url;
            $file_name = $request->file_name;
            $is_pdf = $request->is_pdf;

            $checkgroup = Group::where('id', $groups_id)->first();

            if (!$checkgroup) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Group not found',
                ]);
            }

            if ($is_pdf == 1) {
                if (empty($request->file('file_url'))) {
                    $path = $study->file_url;
                    $namaimgstudy = $study->file_name;
                } else {
                    $validator = Validator::make($request->all(), [
                        'file_url' => 'max:1240|mimes:pdf',
                    ]);

                    if ($validator->fails()) {
                        return response()->json(
                            [
                                'Status' => false,
                                'Message' => 'Something wrong',
                                'Data' => $validator->errors(),
                            ],
                            400
                        );
                    }
                    Storage::delete('public/study/' . $study->file_url);

                    $imgstudy = $request->file('file_url');
                    $namaimgstudy = $imgstudy->getClientOriginalName();
                    $loc = Storage::putFileAs(
                        'public/study',
                        $imgstudy,
                        $namaimgstudy
                    );
                    $path = config('app.url') . Storage::url($loc);
                }
            } else if ($is_pdf == 0) {
                if (empty($file_url = $request->file_url)) {
                    $path = $study->file_url;
                    $namaimgstudy = $study->file_name;
                } else {
                    $file_url = $request->file_url;
                    $path = $file_url;
                    $namaimgstudy = $file_url;
                }
            }

            $study->update([
                'admins_id' => $user->Admin->id,
                'groups_id' => $groups_id,
                'title' => $title,
                'desc' => $desc,
                'file_url' => $path,
                'file_name' => $namaimgstudy,
                'is_pdf' => $is_pdf,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study updated successfully',
                'data_old'          => $tempStudy,
                'data_new'          => $study,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $study,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study update failed',
                'data_old'          => $study,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to delete a study
     *
     * @param Request request The request object.
     *
     * @return The function deletestudy() is being returned.
     */
    /**
     * @OA\Post(
     *     path="/admin/study/delete",
     *     tags={"Admin/Study"},
     *     summary="Delete study for admin",
     *     description="Delete study for admin",
     *     operationId="deleteStudyAdmin",
     *     security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="For get id to delete",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Study update successfully",
     *     ),
     * )
     */
    public function deletestudy(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $study = Study::where('id', $id)->first();
            if (!$study) {
                return response()->json([
                    'message' => 'Study not found'
                ], 404);
            }
            /* Used to create a copy of the study object. */
            $tempStudy = $study->replicate();

            $study->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study deleted successfully',
                'data_old'          => $tempStudy,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Study delete failed',
                'data_old'          => $study,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to get the study that is pinned by the admin.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Get(
     *     path="/coach/study/pin/get",
     *     tags={"Admin/Study"},
     *     summary="Get study pin for admin",
     *     description="Get all study material pin for admin",
     *     operationId="getStudyPinAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Study pin found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Study pin not found",
     *     ),
     * )
     */
    public function pingetstudy(Request $request)
    {
        try {
            $is_pdf = $request->is_pdf;
            $study = Study::with('Admin:id,name,img_url', 'Coach:id,name,img_url', 'Group')->where('is_pin', 1);

            if (!$study->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Get pin successfully',
                'Data' => $study->orderBy('is_pin_time', 'desc')->get(),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to pin a study.
     *
     * @param Request request The request object.
     */

    public function pinaddstudy(Request $request)
    {
        try {
            $id = $request->id;
            $is_pin = $request->is_pin;

            $study = Study::where('id', $id)->first();

            if (!$study) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 404);
            }

            if ($study->is_pin == 0) {
                $cekstudypin = Study::where('is_pin', 1)->get();
                if (count($cekstudypin) == 6) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Cant create pin'
                    ], 404);
                }

                $study->update([
                    'is_pin' => 1,
                    'is_pin_time' => Carbon::now(),
                ]);
            } elseif ($study->is_pin  == 1) {
                $study->update([
                    'is_pin' => 0,
                    'is_pin_time' => null,
                ]);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Success',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
