<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\FormatUang;
use App\Helpers\ConvertBulan;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\NotificationController;
use App\Jobs\SendEmailJob;
use App\Jobs\SendWhatsappJob;
use App\Models\Payment;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminPaymentController extends Controller
{
    /**
     * The above function is used to get the payment details of the student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Payment",
     *     description="API Endpoints of Admin Payments"
     * )
     *
     * @OA\Get(
     *      path="/admin/payment/get",
     *      tags={"Admin/Payment"},
     *      summary="Get all/selected payment",
     *      description="An endpoint that response all or selected Payment based on id",
     *      operationId="GetPaymentAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id_payment",
     *          description="id_payment to get specific payment",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="students_id",
     *          description="students_id to get specific payment",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="groups_id",
     *          description="groups_id to get specific payment",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          description="name to get specific payment",
     *          example="idk what name",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="year",
     *          description="year to get specific payment",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          description="month to get specific payment",
     *          example="February",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="all",
     *          description="all to get specific payment",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging payment",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Payment found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getpayment(Request $request)
    {
        $id_payment = $request->id_payment;
        $students_id = $request->students_id;
        $groups_id = $request->groups_id;
        $name = $request->name;
        $year =  $request->year;
        $month = $request->month;

        $all = $request->all;
        $limit = $request->limit;

        if ($id_payment) {
            $payment = Payment::with('Student')->where('id', $id_payment)->where('month', $month)->where('year', $year);
        } else {
            $payment = new Student;

            if ($name) {
                $payment = $payment->where('name', 'like', '%' . $name . '%');
            }

            if ($students_id) {
                $payment = $payment->where('id', $students_id);
            }

            if ($groups_id) {
                $payment = $payment->where('groups_id', $groups_id);
            }

            if ($all) {
                $payment = $payment->with('Payment');
            }

            if ($month && $year) {
                $payment = $payment->with(['Payment' => function ($query) use ($month, $year) {
                    $query->where('month', $month)->where('year', $year);
                }]);
            }
        }

        if (!$payment->exists()) {
            return response()->json([
                'Status' => false,
                'Message' => 'Payment not found',
            ], 200);
        }

        // if there is no $limit request
        if (!$limit) {
            $limit = $payment->count();
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Payment found',
            'Data' => $payment->paginate($limit)
        ]);
    }

    /**
     * The above function is used to create a payment.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/payment/create",
     *      tags={"Admin/Payment"},
     *      summary="Create new payment",
     *      description="An endpoint that create new Payment",
     *      operationId="CreatePayment",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="students_id", description="students_id of selected payment", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="month", description="month of selected payment", example="February", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="year", description="year of selected payment", example="2022", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="total", description="total of selected payment", example="500000", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Payment created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createpayment(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'month' => 'required',
                'year' => 'required',
                'total' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $month = $request->month;
            $year = $request->year;
            $total = $request->total;
            $date_payment_received = $request->date_payment_received;

            $formatuang = FormatUang::format_uang($total);

            $payment = Payment::create([
                'students_id'           => $students_id,
                'month'                 => $month,
                'year'                  => $year,
                'total'                 => $total,
                'date_payment_received' => $date_payment_received,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'payment',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Payment created successfully',
                'data_old'          => null,
                'data_new'          => $payment,
            ]);

            $student = Student::with('User')->with('Groups')->where('id', $students_id)->first();

            $email =  User::with('Student')->where('username', $student->User->username)->first();

            $month = ConvertBulan::convert_bulan($month);

            //email
            $emailpayment = [
                'title'         => 'Your payment',
                'email'         => $email->email,
                'name'          => $student->name,
                'parent_name'   => $student->father_name,
                'group'         => $student->Groups->name,
                'total'         => $formatuang,
                'month'         => $month,
                'year'          => $year,
                'markdown'      => 'create',
            ];
            try {
                $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                dispatch($job);
            } catch (\Throwable $th) {
                $response_mail = false;
            }

            $monthupper = strtoupper($month);

            //whatsapp
            $phone = [$student->mother_phone, $student->father_phone];
            $parent_name = [$student->mother_name, $student->father_name];
            for ($kunci = 0; $kunci < 2; $kunci++) {
                $whatsappdata = [
                    'target' => $phone[$kunci],
                    //'type' => 'text',
                    'message' => '*[No-reply message]*' . PHP_EOL . PHP_EOL .
                        'INFORMASI *MONTHLY FEE ' . $monthupper . ' ' . $year . ' BSS BINTARO*' .  PHP_EOL . PHP_EOL .
                        'Dear Bapak/ibu *' . $parent_name[$kunci] . '* orangtua dari *' .  $student->name . '*' . PHP_EOL . PHP_EOL .

                        'Berikut kami informasikan bahwa periode pembayaran iuran bulan ' . $month . ' ' . $year . ' sejumlah IDR *' . $formatuang . '* jatuh tempo paling lambat tanggal 15 ' . $month . ' ' . $year . PHP_EOL . PHP_EOL .

                        'Transaksi dapat dilakukan dengan cara transfer ke rekening Maybank 2145 263656 *a.n. PT. Brasilindo Soccer* dan mohon untuk mengirimkan bukti transaksi via WA ke no. 082 1551 98 171 (BSS Bintaro).' . PHP_EOL . PHP_EOL .

                        '*Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses pada aplikasi BSS Bintaro.*' . PHP_EOL . PHP_EOL .

                        'Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran *Monthly Fee* melalui aplikasi *BSS Bintaro Student* pada menu bagian *Payment* atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student *(untuk Android)*' . PHP_EOL . PHP_EOL .

                        'atau' . PHP_EOL . PHP_EOL .

                        'https://apps.apple.com/id/app/bss-bintaro/id1614410288 *(untuk IOS)* .' . PHP_EOL . PHP_EOL .

                        'Atas perhatian dan kerjasamanya kami ucapkan terima kasih banyak.',

                    'schedule' => '0'
                ];
                $jobwa = new SendWhatsappJob($whatsappdata);
                dispatch($jobwa);
            }

            // push notif
            // documentation in  app/Controllers/Admin/AdminNotificationController
            $data = new Request([
                'title'     => 'Information of ' . ucfirst($payment->month) . ' monthly fee',
                'desc'      => 'Hi, your fee payment is due on ' . ucfirst($payment->month) . ' 15th',
                'date'      => \Carbon\Carbon::now(),
                'topic'     => 'STUDENT_ID_' . $payment->Student->users_id,
                'receiver'  => $payment->Student->users_id,
                'category'  => 'AUTOMATED',
            ]);

            try {
                $notification = new NotificationController;
                $result = $notification->createNotification($data);
                if ($result) {
                    return $result;
                }
            } catch (\Throwable $th) {
                return response()->json([
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'error'     => $th,
                ]);
            }


            return response()->json([
                'Status' => true,
                'Message' => 'Payment create successfully',
                'Data' => $payment,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'payment',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Payment create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the payment data.
     *
     * @param Request request the request object
     *
     * @return the updated payment data.
     */
    /**
     * @OA\Post(
     *      path="/admin/payment/update",
     *      tags={"Admin/Payment"},
     *      summary="Update selected payment",
     *      description="An endpoint that update existing Payment based on id",
     *      operationId="UpdatePayment",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected payment", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="year", description="year of selected payment", example="2022", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="total", description="total of selected payment", example="500000", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="date_payment_received", description="date payment received of selected payment", example="2022-02-20", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Payment updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Payment not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatepayment(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            $year = $request->year;
            $total = $request->total;
            $date_payment_received = $request->date_payment_received;

            $payment = Payment::with('Student')->where('id', $id)->first();
            $student = Student::with('User')->with('Groups')->where('id', $payment->students_id)->first();
            /* Used to create a copy of the payment object. */
            $tempPayment = $payment->replicate();

            $email =  User::with('Student')->where('username', $student->User->username)->first();

            if (!$payment) {
                return response()->json([
                    'message' => 'Payment not found'
                ], 404);
            }

            $formatuang = FormatUang::format_uang($total);
            $formatuangpay = FormatUang::format_uang($payment->total);

            $month = $payment->month;
            $month = ConvertBulan::convert_bulan($month);
            $monthupper = strtoupper($month);

            if (!$date_payment_received) {
                $payment->update([
                    'total' => $total,
                ]);

                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'admin',
                    'model_activity'    => 'payment',
                    'causer'            => $user_auth->Admin->name,
                    'log'               => 'Payment updated successfully',
                    'data_old'          => $tempPayment,
                    'data_new'          => $payment,
                ]);

                //email
                $emailpayment = [
                    'title'     => 'Your payment',
                    'email' => $email->email,
                    'name'        => $student->name,
                    'parent_name' => $student->father_name,
                    'group'       => $student->Groups->name,
                    'total'      => $formatuang,
                    'month'  => $month,
                    'year'     => $payment->year,
                    'markdown' => 'create',
                ];
                try {
                    $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                    dispatch($job);
                } catch (\Throwable $th) {
                    $response_mail = false;
                }

                //whatsapp
                $phone = [$student->mother_phone, $student->father_phone];
                $parent_name = [$student->mother_name, $student->father_name];
                for ($kunci = 0; $kunci < 2; $kunci++) {
                    $whatsappdata = [
                        'target' => $phone[$kunci],
                        //'type' => 'text',
                        'message' => '*[No-reply message]*' . PHP_EOL . PHP_EOL .
                            'INFORMASI *MONTHLY FEE ' . $monthupper . ' ' . $year . ' BSS BINTARO*' .  PHP_EOL . PHP_EOL .
                            'Dear Bapak/ibu *' . $parent_name[$kunci] . '* orangtua dari *' .  $student->name . '*' . PHP_EOL . PHP_EOL .

                            'Berikut kami informasikan bahwa periode pembayaran iuran bulan ' . $month  . ' ' . $payment->year . ' sejumlah IDR *' . $formatuang . '* jatuh tempo paling lambat tanggal 15 ' . $payment->month . ' ' . $payment->year . PHP_EOL . PHP_EOL .

                            'Transaksi dapat dilakukan dengan cara transfer ke rekening Maybank 2145 263656 *a.n. PT. Brasilindo Soccer* dan mohon untuk mengirimkan bukti transaksi via WA ke no. 082 1551 98 171 (BSS Bintaro).' . PHP_EOL . PHP_EOL .

                            '*Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses pada aplikasi BSS Bintaro.*' . PHP_EOL . PHP_EOL .

                            'Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran *Monthly Fee* melalui aplikasi *BSS Bintaro Student* pada menu bagian *Payment* atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student *(untuk Android)*' . PHP_EOL . PHP_EOL .

                            'atau' . PHP_EOL . PHP_EOL .

                            'https://apps.apple.com/id/app/bss-bintaro/id1614410288 *(untuk IOS)* .' . PHP_EOL . PHP_EOL .

                            'Atas perhatian dan kerjasamanya kami ucapkan terima kasih banyak.',
                        'delay' => '0',
                        'schedule' => '0'
                    ];
                    $jobwa = new SendWhatsappJob($whatsappdata);
                    dispatch($jobwa);
                }

                // push notif
                // documentation in  app/Controllers/Admin/AdminNotificationController
                $data = new Request([
                    'title'     => 'Information of ' . ucfirst($payment->month) . ' monthly fee',
                    'desc'      => 'Hi, your fee payment is due on ' . ucfirst($payment->month) . ' 15th',
                    'date'      => \Carbon\Carbon::now(),
                    'topic'     => 'STUDENT_ID_' . $payment->Student->users_id,
                    'receiver'  => $payment->Student->users_id,
                    'category'  => 'AUTOMATED',
                ]);

                try {
                    $notification = new NotificationController;
                    $result = $notification->createNotification($data);
                    if ($result) {
                        return $result;
                    }
                } catch (\Throwable $th) {
                    return response()->json([
                        'Status'    => false,
                        'Message'   => 'Something wrong',
                        'error'     => $th,
                    ]);
                }
            } else {
                $payment->update([
                    'date_payment_received' => $date_payment_received,
                ]);

                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'admin',
                    'model_activity'    => 'payment',
                    'causer'            => $user_auth->Admin->name,
                    'log'               => 'Payment updated successfully',
                    'data_old'          => $tempPayment,
                    'data_new'          => $payment,
                ]);

                $dateformat = \Carbon\Carbon::createFromDate($date_payment_received)->isoFormat('D MMMM Y');

                //email
                $emailpayment = [
                    'title'         => 'Your payment',
                    'email'         => $email->email,
                    'name'          => $student->name,
                    'parent_name'   => $student->father_name,
                    'group'         => $student->Groups->name,
                    'total'         => $formatuangpay,
                    'month'         => $month,
                    'year'          => $payment->year,
                    'date_payment_received' => $dateformat,
                    'markdown'      => 'paid',
                ];
                try {
                    $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                    dispatch($job);
                } catch (\Throwable $th) {
                    $response_mail = false;
                }

                //whatsapp
                $phone = [$student->mother_phone, $student->father_phone];
                $parent_name = [$student->mother_name, $student->father_name];
                for ($kunci = 0; $kunci < 2; $kunci++) {
                    $whatsappdata = [
                        'target' => $phone[$kunci],
                        //'type' => 'text',
                        'message' => '*[No-reply message]*' . PHP_EOL . PHP_EOL .
                            'Dear Bapak/ibu ' . $parent_name[$kunci]  . ' orangtua dari ' . $student->name . PHP_EOL . PHP_EOL .

                            'Kami informasikan bahwa pembayaran tagihan Monthly Fee bulan *' .  $month . '* atas nama *' . $student->name . '* kelas ' . $student->Groups->name . ' Brazilian Soccer Schools Bintaro sejumlah *IDR ' .  $formatuangpay . ' telah kami terima* pada *' .  $dateformat . '*' . PHP_EOL . PHP_EOL .

                            'Untuk informasi lebih lanjut, anda dapat melihat di dalam melalui aplikasi *BSS Bintaro Student* pada menu bagian *Payment* atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student *(untuk Android)*' . PHP_EOL . PHP_EOL .

                            'atau' . PHP_EOL . PHP_EOL .

                            'https://apps.apple.com/id/app/bss-bintaro/id1614410288 *(untuk IOS)* .' . PHP_EOL . PHP_EOL .

                            'Atas perhatian dan kepercayaan anda kepada Brazilian Soccer Schools Bintaro, kami ucapkan terima kasih.',

                        'delay' => '0',
                        'schedule' => '0'
                    ];
                    $jobwa = new SendWhatsappJob($whatsappdata);
                    dispatch($jobwa);
                }

                // push notif
                // documentation in  app/Controllers/Admin/AdminNotificationController
                $data = new Request([
                    'title'     => 'Payment received 😊',
                    'desc'      => ucfirst($payment->month) . ' monthly fee has been received',
                    'date'      => \Carbon\Carbon::now(),
                    'topic'     => 'STUDENT_ID_' . $payment->Student->users_id,
                    'receiver'  => $payment->Student->users_id,
                    'category'  => 'AUTOMATED',
                ]);

                try {
                    $notification = new NotificationController;
                    $result = $notification->createNotification($data);
                    if ($result) {
                        return $result;
                    }
                } catch (\Throwable $th) {
                    return response()->json([
                        'Status'    => false,
                        'Message'   => 'Something wrong',
                        'error'     => $th,
                    ]);
                }
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $payment,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'payment',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Payment update failed',
                'data_old'          => $payment,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create a payment for all students in a group.
     *
     * @param Request request the request object
     *
     * @return A response in JSON format.
     */
    /**
     * @OA\Post(
     *      path="/admin/payment/create/all",
     *      tags={"Admin/Payment"},
     *      summary="Create all new payment !! NOT FINISH",
     *      description="An endpoint that create/all new Payment",
     *      operationId="CreateAllPayment",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="month", description="month of selected payment", example="February", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="year", description="year of selected payment", example="2022", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="total", description="total of selected payment", example="500000", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Payment created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    // !! Kurang paham isi $request/parameter yang dibutuhkan
    public function createallpayment(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'month' => 'required',
                'year' => 'required',
                'total' => 'required',
            ]);

            $is_student = $request->is_student;
            $month = $request->month;

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }
            $formatuang = FormatUang::format_uang($request->total);

            $month = ConvertBulan::convert_bulan($month);
            $monthupper = strtoupper($month);

            if ($is_student == 1) {

                foreach ($request->students_id as $key => $value) {
                    $paymentlooparray = [
                        'students_id' => $request->students_id[$key],
                        'month' => $request->month,
                        'year' => $request->year,
                        'total' => $request->total,
                        'date_payment_received' => $request->date_payment_received
                    ];

                    $cekpayment = Payment::where('students_id', $request->students_id[$key])->where('month', $request->month)->where('year', $request->year)->first();
                    if (!$cekpayment) {
                        $payment =  Payment::firstOrCreate($paymentlooparray);

                        /* Used to create a log of the user activity. */
                        UserActivity::create([
                            'model_user_role'   => 'admin',
                            'model_activity'    => 'payment',
                            'causer'            => $user_auth->Admin->name,
                            'log'               => 'Payment created successfully',
                            'data_old'          => null,
                            'data_new'          => $payment,
                        ]);

                        $students = Student::with('User')->where('id', $request->students_id[$key])->first();
                        $email =  User::with('Student')->where('username', $students->User->username)->first();


                        //email
                        $emailpayment = [
                            'title'     => 'Your payment',
                            'email'     => $email->email,
                            'name'        => $students->name,
                            'parent_name' => $students->father_name,
                            'group'       => $students->Groups->name,
                            'total'      => $formatuang,
                            'month'  => $month,
                            'year'     => $request->year,
                            'markdown' => 'create',
                        ];
                        try {
                            $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                            dispatch($job);
                        } catch (\Throwable $th) {
                            $response_mail = false;
                        }

                        //whatsapp
                        $phone = [$students->mother_phone, $students->father_phone];
                        $parent_name = [$students->mother_name, $students->father_name];
                        for ($kunci = 0; $kunci < 2; $kunci++) {
                            $whatsappdata = [
                                'target' => $phone[$kunci],
                                //'type' => 'text',
                                'message' => '*[No-reply message]*' . PHP_EOL . PHP_EOL .
                                    'INFORMASI *MONTHLY FEE ' . $monthupper . ' ' . $request->year . ' BSS BINTARO*' .  PHP_EOL . PHP_EOL .
                                    'Dear Bapak/ibu *' . $parent_name[$kunci] . '* orangtua dari *' .  $students->name . '*' . PHP_EOL . PHP_EOL .

                                    'Berikut kami informasikan bahwa periode pembayaran iuran bulan ' . $month . ' ' . $request->year . ' sejumlah IDR *' . $formatuang . '* jatuh tempo paling lambat tanggal 15 ' . $request->month  . ' ' . $request->year . PHP_EOL . PHP_EOL .

                                    'Transaksi dapat dilakukan dengan cara transfer ke rekening Maybank 2145 263656 *a.n. PT. Brasilindo Soccer* dan mohon untuk mengirimkan bukti transaksi via WA ke no. 082 1551 98 171 (BSS Bintaro).' . PHP_EOL . PHP_EOL .

                                    '*Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses pada aplikasi BSS Bintaro.*' . PHP_EOL . PHP_EOL .

                                    'Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran *Monthly Fee* melalui aplikasi *BSS Bintaro Student* pada menu bagian *Payment* atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student *(untuk Android)*' . PHP_EOL . PHP_EOL .

                                    'atau' . PHP_EOL . PHP_EOL .

                                    'https://apps.apple.com/id/app/bss-bintaro/id1614410288 *(untuk IOS)* .' . PHP_EOL . PHP_EOL .

                                    'Atas perhatian dan kerjasamanya kami ucapkan terima kasih banyak.',
                                'delay' => '0',
                                'schedule' => '0'
                            ];
                            $jobwa = new SendWhatsappJob($whatsappdata);
                            dispatch($jobwa);
                        }

                        // push notif
                        // documentation in  app/Controllers/Admin/AdminNotificationController
                        $data = new Request([
                            'title'     => 'Information of ' . ucfirst($payment->month) . ' monthly fee',
                            'desc'      => 'Hi, your fee payment is due on ' . ucfirst($payment->month) . ' 15th',
                            'date'      => \Carbon\Carbon::now(),
                            'topic'     => 'STUDENT_ID_' . $payment->Student->users_id,
                            'receiver'  => $payment->Student->users_id,
                            'category'  => 'AUTOMATED',
                        ]);

                        try {
                            $notification = new NotificationController;
                            $result = $notification->createNotification($data);
                            if ($result) {
                                return $result;
                            }
                        } catch (\Throwable $th) {
                            return response()->json([
                                'Status'    => false,
                                'Message'   => 'Something wrong',
                                'error'     => $th,
                            ]);
                        }
                    } else {
                        $payment = '';
                    }
                }
            } else if ($is_student == 0) {
                $groups_id = $request->groups_id;

                $student = Student::where('groups_id', $groups_id)->get();

                foreach ($student->pluck('id') as $key => $value) {
                    $paymentlooparray = [
                        'students_id' => $student->pluck('id')[$key],
                        'month' => $request->month,
                        'year' => $request->year,
                        'total' => $request->total,
                        'date_payment_received' => $request->date_payment_received
                    ];

                    $cekpayment = Payment::where('students_id', $student->pluck('id')[$key])->where('month', $request->month)->where('year', $request->year)->first();
                    if (!$cekpayment) {
                        $payment =  Payment::firstOrCreate($paymentlooparray);

                        /* Used to create a log of the user activity. */
                        UserActivity::create([
                            'model_user_role'   => 'admin',
                            'model_activity'    => 'payment',
                            'causer'            => $user_auth->Admin->name,
                            'log'               => 'Payment created successfully',
                            'data_old'          => null,
                            'data_new'          => $payment,
                        ]);

                        $students = Student::with('User')->where('id', $student->pluck('id')[$key])->first();
                        $email =  User::with('Student')->where('username', $students->User->username)->first();

                        //email
                        $emailpayment = [
                            'title'     => 'Your payment',
                            'email'     => $email->email,
                            'name'        => $students->name,
                            'parent_name' => $students->father_name,
                            'group'       => $students->Groups->name,
                            'total'      => $formatuang,
                            'month'  => $month,
                            'year'     => $request->year,
                            'markdown' => 'create',
                        ];
                        try {
                            $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                            dispatch($job);
                        } catch (\Throwable $th) {
                            $response_mail = false;
                        }

                        //whatsapp
                        $phone = [$students->mother_phone, $students->father_phone];
                        $parent_name = [$students->mother_name, $students->father_name];
                        for ($kunci = 0; $kunci < 2; $kunci++) {
                            $whatsappdata = [
                                'target' => $phone[$kunci],
                                //'type' => 'text',
                                'message' => '*[No-reply message]*' . PHP_EOL . PHP_EOL .
                                    'INFORMASI *MONTHLY FEE ' . $monthupper . ' ' . $request->year . ' BSS BINTARO*' .  PHP_EOL . PHP_EOL .
                                    'Dear Bapak/ibu *' . $parent_name[$kunci] . '* orangtua dari *' .  $students->name . '*' . PHP_EOL . PHP_EOL .

                                    'Berikut kami informasikan bahwa periode pembayaran iuran bulan ' . $month . ' ' . $request->year . ' sejumlah IDR *' . $formatuang . '* jatuh tempo paling lambat tanggal 15 ' . $request->month  . ' ' . $request->year . PHP_EOL . PHP_EOL .

                                    'Transaksi dapat dilakukan dengan cara transfer ke rekening Maybank 2145 263656 *a.n. PT. Brasilindo Soccer* dan mohon untuk mengirimkan bukti transaksi via WA ke no. 082 1551 98 171 (BSS Bintaro).' . PHP_EOL . PHP_EOL .

                                    '*Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses pada aplikasi BSS Bintaro.*' . PHP_EOL . PHP_EOL .

                                    'Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran *Monthly Fee* melalui aplikasi *BSS Bintaro Student* pada menu bagian *Payment* atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student *(untuk Android)*' . PHP_EOL . PHP_EOL .

                                    'atau' . PHP_EOL . PHP_EOL .

                                    'https://apps.apple.com/id/app/bss-bintaro/id1614410288 *(untuk IOS)* .' . PHP_EOL . PHP_EOL .

                                    'Atas perhatian dan kerjasamanya kami ucapkan terima kasih banyak.',
                                'delay' => '0',
                                'schedule' => '0'
                            ];
                            $jobwa = new SendWhatsappJob($whatsappdata);
                            dispatch($jobwa);
                        }

                        // push notif
                        // documentation in  app/Controllers/Admin/AdminNotificationController
                        $data = new Request([
                            'title'     => 'Information of ' . ucfirst($payment->month) . ' monthly fee',
                            'desc'      => 'Hi, your fee payment is due on ' . ucfirst($payment->month) . ' 15th',
                            'date'      => \Carbon\Carbon::now(),
                            'topic'     => 'STUDENT_ID_' . $payment->Student->users_id,
                            'receiver'  => $payment->Student->users_id,
                            'category'  => 'AUTOMATED',
                        ]);

                        try {
                            $notification = new NotificationController;
                            $result = $notification->createNotification($data);
                            if ($result) {
                                return $result;
                            }
                        } catch (\Throwable $th) {
                            return response()->json([
                                'Status'    => false,
                                'Message'   => 'Something wrong',
                                'error'     => $th,
                            ]);
                        }
                    } else {
                        $payment = '';
                    }
                }
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Payment create successfully',
                'Data' => $payment,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'payment',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Payment create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to send an email to the user when the user has made a payment.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/payment/notif;email",
     *      tags={"Admin/Payment"},
     *      summary="Notif selected payment",
     *      description="An endpoint that notif existing Payment based on id",
     *      operationId="NotifPayment",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected payment", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Payment notified successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Payment not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function notifpaymentemail(Request $request)
    {
        try {
            $id = $request->id;
            $payment = Payment::where('id', $id)->first();
            $month = $payment->month;
            $year = $payment->year;
            $total = $payment->total;
            $formatuang = FormatUang::format_uang($total);
            $student = Student::with('User')->with('Groups')->where('id', $payment->students_id)->first();

            $email =  User::with('Student')->where('username', $student->User->username)->first();

            $month = ConvertBulan::convert_bulan($month);

            //email
            $emailpayment = [
                'title'         => 'Your payment',
                'email'         => $email->email,
                'name'          => $student->name,
                'parent_name'   => $student->father_name,
                'group'         => $student->Groups->name,
                'total'         => $formatuang,
                'month'         => $month,
                'year'          => $year,
                'markdown'      => 'create',
            ];
            try {
                $job = (new SendEmailJob($emailpayment))->delay(now()->addSeconds(2));
                dispatch($job);
            } catch (\Throwable $th) {
                $response_mail = false;
            }


            return response()->json([
                'Status' => true,
                'Message' => 'Email send successfully',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
