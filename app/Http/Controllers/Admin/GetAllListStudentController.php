<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;

use Illuminate\Http\Request;

class GetAllListStudentController extends Controller
{
    /**
     * This function is used to get all student report.
     *
     * @param Request request The request object.
     *
     * @return All student found
     */
    /**
     * @OA\Get(
     *     path="/admin/all/student/get",
     *     tags={"Admin get exam report and exam monthly"},
     *     summary="Get get exam report and exam monthly admin",
     *     description="Get exam report and exam monthly for admin",
     *     operationId="getExamReportAndExamMonthlyAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For find specific student",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="group",
     *          description="For filter by group",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="name",
     *          description="For filter by name",
     *          example="andi",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Data found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Data not found",
     *     ),
     * )
     */
    public function getallstudentreport(Request $request)
    {
        try {
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $group = $request->group;
            $name = $request->name;
            $id = $request->id;

            $student = Student::with('Groups');

            if ($id) {
                $student = $student->where('id', $id);
            }

            if ($group) {
                $student = $student->where('groups_id', $group);
            }

            if ($name) {
                $student = $student->where('name', 'like', '%' . $name . '%');
            }

            if ($year && $month) {
                $student = $student->with('User')->with(['Report_monthlys' => function ($query) use ($year, $month) {
                    $query->where('year', $year)->where('month', $month);
                }])->with(['Report_exams' => function ($query) use ($year, $month) {
                    $query->whereYear('date', $year)->whereMonth('date', $month);
                }]);
            }

            if (!$student->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'All student not found',
                ], 200);
            }
            return response()->json([
                'Status' => true,
                'Message' => 'All student found',
                'Data' => $student->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
