<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Controller;
use App\Models\ExamLevel;
use App\Models\ExamSkill;
use App\Models\ExamSkillGroup;
use App\Models\ReportExam;
use App\Models\ReportExamLevel;
use App\Models\ReportExamSkill;
use App\Models\ReportExamSkillGroup;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminReportExamController extends Controller
{
    /**
     * It returns the exam template
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/ReportExamTemplate",
     *     description="API Endpoints of Admin ReportExamTemplates"
     * )
     *
     * @OA\Get(
     *      path="/admin/report/exam/template/get",
     *      tags={"Admin/ReportExamTemplate"},
     *      summary="Get all/selected report/exam/template",
     *      description="An endpoint that response all or selected ReportExamTemplate based on id",
     *      operationId="GetReportExamTemplateAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific report/exam/template",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging report/exam/template",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExamTemplate found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getexamtemplate(Request $request)
    {
        try {
            $limit = $request->limit;
            $id = $request->id;

            $examtemplate = ExamLevel::with(['Exam_skill_groups' => function ($query) {
                $query->with('Exam_skills');
            }]);

            if ($id) {
                $examtemplate = $examtemplate->where('id', $id);
            }

            if (!$examtemplate->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam not found',
                ], 200);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $examtemplate->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Exam template found',
                'Data' => $examtemplate->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * It returns the exam template
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/report/exam/template/create",
     *      tags={"Admin/ReportExamTemplate"},
     *      summary="Create new report/exam/template",
     *      description="An endpoint that create new ReportExamTemplate",
     *      operationId="CreateReportExamTemplate",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="title", description="title of selected report/exam/template", example="Template Title", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected report/exam/template", example="Deskripsi dari template ampuh untuk meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="level_logo_url", description="level logo url of selected report/exam/template", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExamTemplate created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createexamtemplate(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
                'level_logo_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }


            $title = $request->title;
            $desc = $request->desc;
            $level_logo_url = $request->level_logo_url;
            $examtemplatelevelid = ExamLevel::where('id', $request->id)->first();

            if ($request->id) {
                $examtemplatelevel = ExamLevel::where('id', $request->id);
                $examskiilgroup = ExamSkillGroup::where('exam_levels_id', $request->id);


                if ($examtemplatelevel->exists()) {
                    Storage::delete('public/examreport/' . $examtemplatelevelid->level_logo_url);
                    ExamSkill::whereIn('exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                    ExamSkillGroup::where('exam_levels_id', $request->id)->delete();
                    ExamLevel::where('id', $request->id)->delete();
                }
            }

            if ($request->file('level_logo_url')) {
                $imgreport = $request->file('level_logo_url');
                $namaimgreport = $imgreport->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/reportexam',
                    $imgreport,
                    $namaimgreport
                );
            } else {
                $path = '';
                $namaimgreport = '';
            }

            $examlevel = ExamLevel::create([
                'title' => $title,
                'desc' => $desc,
                'level_logo_url' => config('app.url') . Storage::url($path),
            ]);

            $index = 0;
            foreach ($request->title_skill_group as $key => $value) {
                $examskillgrouparray = [
                    'exam_levels_id' => $examlevel->id,
                    'title_skill_group' => $request->title_skill_group[$key],
                ];
                $examskillgroup = ExamSkillGroup::create($examskillgrouparray);


                $keys = count($request->name[$index]);

                for ($kunci = 0; $kunci < $keys; $kunci++) {
                    $examskillarray = [
                        'exam_skill_groups_id' => $examskillgroup->id,
                        'name' => $request->name[$index][$kunci],
                        'min_score' => $request->min_score[$index][$kunci],
                        'max_score' => $request->max_score[$index][$kunci],
                    ];
                    ExamSkill::create($examskillarray);
                }

                $index++;
            }


            $result = ExamLevel::with(['Exam_skill_groups' => function ($query) {
                $query->with('Exam_skills');
            }])->where('id', $examlevel->id)->get();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template created successfully',
                'data_old'          => null,
                'data_new'          => $result,
            ]);


            return response()->json([
                'Status' => true,
                'Message' => 'Exam level create successfully',
                'Data' =>  $result,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the exam template.
     *
     * @param Request request The request object.
     *
     * @return the exam template data.
     */
    /**
     * @OA\Post(
     *      path="/admin/report/exam/template/update",
     *      tags={"Admin/ReportExamTemplate"},
     *      summary="Update selected report/exam/template",
     *      description="An endpoint that update existing ReportExamTemplate based on id",
     *      operationId="UpdateReportExamTemplate",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected report/exam/template", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="title", description="title of selected report/exam/template", example="Template Title", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected report/exam/template", example="Deskripsi dari template ampuh untuk meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="level_logo_url", description="level logo url of selected report/exam/template", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExamTemplate updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="ReportExamTemplate not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updateexamtemplate(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $examtemplate = ExamLevel::where('id', $id)->with(['Exam_skill_groups' => function ($query) {
                $query->with('Exam_skills');
            }])->first();
            /* Used to create a copy of the examtemplate object. */
            $tempExamtemplate = $examtemplate->replicate();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
                'level_logo_url' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $examtemplate->update([
                'title' => $request->title,
                'desc' => $request->desc,
                'level_logo_url' => $request->level_logo_url,
            ]);


            foreach ($request->title_skill_group as $key => $value) {
                $examskillgrouparray = [
                    'exam_levels_id' =>  $examtemplate->id,
                    'title_skill_group' => $request->title_skill_group[$key],
                ];
                ExamSkillGroup::where('id', $request->id_exam_levels_id[$key])->update($examskillgrouparray);
            }

            foreach ($request->name as $key => $value) {
                $examskillarray = [
                    'exam_skill_groups_id' => $request->exam_skill_groups_id[$key],
                    'name' => $request->name[$key],
                    'min_score' => $request->min_score[$key],
                    'max_score' => $request->max_score[$key],
                ];
                ExamSkill::where('id', $request->id_exam_skill_groups_id[$key])->update($examskillarray);
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template updated successfully',
                'data_old'          => $tempExamtemplate,
                'data_new'          => $examtemplate,
            ]);


            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $examtemplate
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template update failed',
                'data_old'          => $examtemplate,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function deletes an exam template.
     *
     * @param Request request The request object.
     *
     * @return a json response.
     */
    /**
     * @OA\Post(
     *      path="/admin/report/exam/template/delete",
     *      tags={"Admin/ReportExamTemplate"},
     *      summary="Delete selected report/exam/template",
     *      description="An endpoint that delete existing ReportExamTemplate based on id",
     *      operationId="DeleteReportExamTemplate",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected report/exam/template", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExamTemplate deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="ReportExamTemplate not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deleteexamtemplate(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $examtemplatelevel = ExamLevel::where('id', $request->id);
            $examskiilgroup = ExamSkillGroup::where('exam_levels_id', $request->id);

            $examtemplate = ExamLevel::where('id', $request->id)->with(['Exam_skill_groups' => function ($query) {
                $query->with('Exam_skills');
            }])->first();
            /* Used to create a copy of the examtemplate object. */
            $tempExamtemplate = $examtemplate->replicate();

            if ($examtemplatelevel->exists()) {
                ExamSkill::whereIn('exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                ExamSkillGroup::where('exam_levels_id', $request->id)->delete();
                ExamLevel::where('id', $request->id)->delete();
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template deleted successfully',
                'data_old'          => $tempExamtemplate,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Exam level delete successfully',
                // 'Data' => $result,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examTemplate',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam template delete failed',
                'data_old'          => $examtemplate,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to get the exam report.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/ReportExam",
     *     description="API Endpoints of Admin ReportExams"
     * )
     *
     * @OA\Get(
     *      path="/admin/report/exam/get",
     *      tags={"Admin/ReportExam"},
     *      summary="Get all/selected report/exam",
     *      description="An endpoint that response all or selected ReportExam based on id",
     *      operationId="GetReportExamAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific report/exam",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="students_id",
     *          description="students_id to get specific report/exam",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="year",
     *          description="year to get specific report/exam",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          description="month to get specific report/exam",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging report/exam",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExam found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getexamreport(Request $request)
    {
        try {
            $id = $request->id;
            $students_id = $request->students_id;
            $year = $request->year;
            $month = $request->month;
            $limit = $request->limit;

            // $user = User::with('Coach')->where('id', Auth::user()->id)->first();
            $reportexam = ReportExam::with('Student', 'Admin')->with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }]);

            if ($id) {
                $reportexam = $reportexam->where('id', $id)->whereMonth('date', $month)->whereYear('date', $year);
            }

            if ($month) {
                $reportexam = $reportexam->whereMonth('date', $month);
            }

            if ($year) {
                $reportexam = $reportexam->whereYear('date', $year);
            }

            if ($students_id) {
                $reportexam = $reportexam->where('students_id', $students_id);
            }

            if (!$reportexam->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam not found',
                ], 200);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $reportexam->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Report exam found',
                'Data' => $reportexam->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create an exam report.
     *
     * @param Request request the request object
     */
    /**
     * @OA\Post(
     *      path="/admin/report/exam/create",
     *      tags={"Admin/ReportExam"},
     *      summary="Create new report/exam",
     *      description="An endpoint that create new ReportExam",
     *      operationId="CreateReportExam",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="coaches_id", description="coaches_id of selected report/exam", example="1", required=false, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="students_id", description="students_id of selected report/exam", example="1", required=false, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="title", description="title of selected report/exam", example="Template Title", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected report/exam", example="Deskripsi dari template ampuh untuk meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="date", description="date  of selected report/exam", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExam created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createexamreport(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc'  => 'required',
                'date'  => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $coaches_id = $request->coaches_id;
            $admins_id = User::with('Admin')->where('id', Auth::id())->first();
            $date = $request->date;
            $is_passed = $request->is_passed;

            $title = $request->title;
            $desc = $request->desc;
            $level_logo_url = $request->level_logo_url;

            if ($request->id) {
                $examreport = ReportExam::where('id', $request->id)->first();
                $examreportlevel = ReportExamLevel::where('report_exams_id', $examreport->id)->get();
                $examskiilgroup = ReportExamSkillGroup::where('report_exam_levels_id', $examreportlevel->pluck('id'))->get();
                $examreportcheck = ReportExam::where('id', $request->id);

                if ($examreportcheck->exists()) {
                    ReportExamSkill::whereIn('report_exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                    ReportExamSkillGroup::whereIn('report_exam_levels_id', $examreportlevel->pluck('id'))->delete();
                    ReportExamLevel::where('report_exams_id', $examreport->id)->delete();
                    ReportExam::where('id', $request->id)->delete();
                }
            }

            $reportexamcheck = ReportExam::where('students_id', $students_id)->where('date', $date)->first();
            if ($reportexamcheck) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam already created',
                ], 404);
            }
            // if ($reportexamcheck) {
            //     $examreport = ReportExam::where('id', $reportexamcheck->id)->first();
            //     $examreportlevel = ReportExamLevel::where('report_exams_id', $examreport->id)->get();
            //     $examskiilgroup = ReportExamSkillGroup::where('report_exam_levels_id', $examreportlevel->pluck('id'))->get();

            //     ReportExamSkill::whereIn('report_exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
            //     ReportExamSkillGroup::whereIn('report_exam_levels_id', $examreportlevel->pluck('id'))->delete();
            //     ReportExamLevel::where('report_exams_id', $examreport->id)->delete();
            //     ReportExam::where('id', $reportexamcheck->id)->delete();
            // }

            $examreport = ReportExam::create([
                'students_id'   => $students_id,
                'coaches_id'    => $coaches_id,
                'admins_id'     => $admins_id->Admin->id,
                'date'          => $date,
                'is_passed'     => $is_passed,
            ]);

            $examlevelreport = ReportExamLevel::create([
                'report_exams_id' => $examreport->id,
                'title' => $title,
                'desc' => $desc,
                'level_logo_url' => $level_logo_url,
            ]);

            $index = 0;
            foreach ($request->title_skill_group as $key => $value) {
                $examskillgrouparray = [
                    'report_exam_levels_id' => $examlevelreport->id,
                    'title_skill_group' => $request->title_skill_group[$key],
                ];
                $examskillgroup = ReportExamSkillGroup::create($examskillgrouparray);

                $keys = count($request->name[$index]);

                for ($kunci = 0; $kunci < $keys; $kunci++) {
                    $examskillarray = [
                        'report_exam_skill_groups_id' => $examskillgroup->id,
                        'name'      => $request->name[$index][$kunci],
                        'score'     => $request->score[$index][$kunci],
                        'min_score' => $request->min_score[$index][$kunci],
                        'max_score' => $request->max_score[$index][$kunci],
                    ];
                    ReportExamSkill::create($examskillarray);
                }

                $index++;
            }

            $result = ReportExam::with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }])->where('id', $examreport->id)->get();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examReport',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam report created successfully',
                'data_old'          => null,
                'data_new'          => $result,
            ]);

            // push notif
            // documentation in  app/Controllers/Admin/AdminNotificationController
            $data = new Request([
                'title'     => '⚽ EXAM REPORT ⚽',
                'desc'      => 'Your ' . \Carbon\Carbon::parse($examreport->date)->format('F') . ' EXAM result is available!',
                'date'      => \Carbon\Carbon::now(),
                'topic'     => 'STUDENT_ID_' . $examreport->Student->users_id,
                'receiver'  => $examreport->Student->users_id,
                'category'  => 'AUTOMATED',
            ]);

            try {
                $notification = new NotificationController;
                $result = $notification->createNotification($data);
                if ($result) {
                    return $result;
                }
            } catch (\Throwable $th) {
                return response()->json([
                    'Status'    => false,
                    'Message'   => 'Something wrong',
                    'error'     => $th,
                ]);
            }


            return response()->json([
                'Status' => true,
                'Message' => 'Exam report create successfully',
                'Data' => $result,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examReport',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam report create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function deletes the exam report.
     *
     * @param Request request The request object.
     *
     * @return a json response.
     */
    /**
     * @OA\Post(
     *      path="/admin/report/exam/delete",
     *      tags={"Admin/ReportExam"},
     *      summary="Delete selected report/exam",
     *      description="An endpoint that delete existing ReportExam based on id",
     *      operationId="DeleteReportExam",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected report/exam", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="ReportExam deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="ReportExam not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deleteexamreport(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $examreport = ReportExam::where('id', $request->id)->first();
            $examreportlevel = ReportExamLevel::where('report_exams_id', $examreport->id)->get();
            $examskiilgroup = ReportExamSkillGroup::where('report_exam_levels_id', $examreportlevel->pluck('id'))->get();
            $examreportcheck = ReportExam::where('id', $request->id);

            $result = ReportExam::with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }])->where('id', $examreport->id)->get();
            /* Used to create a copy of the examreport object. */
            $tempExamreport = $result->replicate();

            if ($examreportcheck->exists()) {
                ReportExamSkill::whereIn('report_exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                ReportExamSkillGroup::whereIn('report_exam_levels_id', $examreportlevel->pluck('id'))->delete();
                ReportExamLevel::where('report_exams_id', $examreport->id)->delete();
                ReportExam::where('id', $request->id)->delete();
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examReport',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam report deleted successfully',
                'data_old'          => $tempExamreport,
                'data_new'          => null,
            ]);


            return response()->json([
                'Status' => true,
                'Message' => 'Report exam delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'examReport',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Exam report delete failed',
                'data_old'          => $examreport,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
