<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachGroup;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminCoachGroupController extends Controller
{
    /**
     * The above function is used to create a coach group.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Tag(
     *     name="Admin/CoachGroup",
     *     description="API Endpoints of Admin CoachGroup Management"
     * )
     *
     * @OA\Post(
     *      path="/admin/coachgroup/create",
     *      tags={"Admin/CoachGroup"},
     *      summary="Create new group",
     *      description="An endpoint that create new CoachGroup",
     *      operationId="CreateCoachGroup",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="coaches_id", description="id of selected coach", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="groups_id", description="id of selected group", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Coach Group created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createcoachgroup(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'coaches_id' => 'required',
                'groups_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $cekcoachgroup = CoachGroup::where('coaches_id', $request->coaches_id);
            if ($cekcoachgroup->exists()) {
                CoachGroup::where('coaches_id', $request->coaches_id)->delete();
            }

            foreach ($request->groups_id as $key => $value) {
                $coachgrouparray = [
                    'coaches_id' => $request->coaches_id,
                    'groups_id' => $request->groups_id[$key],
                ];
                $coachgroup = CoachGroup::create($coachgrouparray);
                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'admin',
                    'model_activity'    => 'coachGroup',
                    'causer'            => $user_auth->Admin->name,
                    'log'               => 'Coach Group created successfully',
                    'data_old'          => null,
                    'data_new'          => $coachgroup,
                ]);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Presence create successfully',
                'Data' => $coachgroup,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coachGroup',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach Group create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to update the coachgroup
     *
     * @param Request request The request object.
     *
     * @return The coachgroup is being returned.
     */
    /**
     * @OA\Post(
     *      path="/admin/coachgroup/update",
     *      tags={"Admin/CoachGroup"},
     *      summary="Update selected coachgroup",
     *      description="An endpoint that update existing CoachGroup based on id",
     *      operationId="UpdateCoachGroup",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected coachgroup", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="coaches_id", description="id of selected coach", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="groups_id", description="id of selected group", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Group updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Group not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatecoachgroup(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $coachgroup = CoachGroup::where('id', $id)->first();
            if (!$coachgroup) {
                return response()->json([
                    'message' => 'Coach Group not found'
                ], 404);
            }
            /* Used to create a copy of the coachgroup object. */
            $tempCoachGroup = $coachgroup->replicate();

            $validator = Validator::make($request->all(), [
                'coaches_id' => 'required',
                'groups_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => false,
                        $validator->errors(),
                    ],
                    400
                );
            }

            CoachGroup::where('coaches_id', $id)->delete();

            foreach ($request->groups_id as $key => $value) {
                $coachgrouparray = [
                    'coaches_id' => $request->coaches_id,
                    'groups_id' => $request->groups_id[$key],
                ];
                $coachgroup = CoachGroup::create($coachgrouparray);

                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'admin',
                    'model_activity'    => 'coachGroup',
                    'causer'            => $user_auth->Admin->name,
                    'log'               => 'Coach Group updated successfully',
                    'data_old'          => $tempCoachGroup,
                    'data_new'          => $coachgroup,
                ]);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $coachgroup,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coachGroup',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach Group update failed',
                'data_old'          => $coachgroup,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
