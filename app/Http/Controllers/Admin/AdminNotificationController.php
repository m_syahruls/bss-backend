<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\NotificationController;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminNotificationController extends Controller
{
    /**
     * It gets the notifications of the logged in user
     *
     * @param Request request The request object.
     *
     * @return A list of notifications
     */
    /**
     * @OA\Tag(
     *     name="Admin/Notification",
     *     description="API Endpoints of Admin Notification Management"
     * )
     *
     * @OA\Get(
     *      path="/admin/notification/get",
     *      tags={"Admin/Notification"},
     *      summary="Get all notification",
     *      description="An endpoint that response all Notification",
     *      operationId="GetNotificationAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging notification",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Notification list found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Notification list not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getNotification(Request $request)
    {
        $user_auth = User::with('Admin')->where('id', Auth::id())->first();

        $limit = $request->limit;
        $notifications = Notification::where('admins_id', $user_auth->Admin->id)->with('receivers.user')->orderBy('created_at', 'DESC')->paginate($limit);

        if (count($notifications) == 0) {
            return response()->json([
                'Status'    => false,
                'Message'   => 'Notification list not found',
                'Data'      => [],
            ], 200);
        }

        foreach ($notifications as $notification) {
            $i = 0;
            $names = [];
            foreach ($notification->receivers as $receiver) {
                $names[$i++] = $receiver->user->name;
                unset($receiver->user);
            }
            unset($notification->receivers);
            $notification->receivers = $names;
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Notification list found',
            'Data' => $notifications,
        ]);
    }

    /**
     * The above function is used to create an announcement.
     *
     * @param Request request
     *
     * @return object
     */
    /**
     * @OA\Post(
     *      path="/admin/notification/create",
     *      tags={"Admin/Notification"},
     *      summary="Create new notification",
     *      description="An endpoint that create new Notification",
     *      operationId="CreateNotification",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="title", description="title of selected notification", example="Title of Notifcation", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected notification", example="Description of this notification, maybe idk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="date", description="date of selected notification", example="2022-02-02 20:22:02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime"
     *          )
     *      ),
     *      @OA\Parameter(name="topic[]", description="topic of selected notification", required=true, in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(type="string"),
     *              example={"STUDENT", "COACH_ID_1", "GROUP_1"}
     *          )
     *      ),
     *      @OA\Parameter(name="receiver[]", description="receiver of selected notification based on selected topic", required=true, in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(type="integer"),
     *              example={null,1,1}
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Notification created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createAnnouncment(Request $request)
    {
        $user_auth = User::with('Admin')->where('id', Auth::id())->first();

        // this block code is template for another create notification
        // create request for easier validation
        $data = new Request([
            'admins_id' => $user_auth->Admin->id,    // int, optional
            'title'     => $request->title,         // string
            'desc'      => $request->desc,          // string
            'date'      => $request->date,          // datetime, if immidiate use \Carbon\Carbon::now();
            'topic'     => $request->topic,         // array, [COACH, COACH_ID_X, STUDENT, STUDENT_ID_X, GROUP_Y] X = users_id Y = groups_id
            'receiver'  => $request->receiver,      // array, [1,2,3,4,5,6,7,8,9,0] users_id
            'category'  => 'ANNOUNCMENT',           // string, "ANNOUNCMENT/AUTOMATED"
        ]);

        // try to create notification from another controller
        try {
            $notification = new NotificationController;
            $result = $notification->createNotification($data);

            // !REVERSE RETURN CONDITION
            // fail validator return true because of there is data to carry
            // successfull create notification return false because of no need to return any data
            // successfull notif will pass this condition below
            if ($result) {
                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'admin',
                    'model_activity'    => 'notification',
                    'causer'            => $user_auth->Admin->name,
                    'log'               => 'Announcment create failed',
                    'data_old'          => null,
                    'data_new'          => $result,
                ]);
                return $result;
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'notification',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Announcment created successfully',
                'data_old'          => null,
                'data_new'          => $data->all(),
            ]);

            // return just for this func, optional for another controller
            return response()->json([
                'Status' => true,
                'Message' => 'Notification created successfully',
                'Data' => $data->all(),
            ]);
        } catch (\Throwable $th) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'notification',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Announcment create failed',
                'data_old'          => null,
                'data_new'          => $th,
            ]);

            return response()->json([
                'Status'    => false,
                'Message'   => 'Something wrong',
                'error'     => $th,
            ]);
        }
    }
}
