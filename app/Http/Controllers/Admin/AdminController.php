<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendWhatsappJob;
use App\Mail\Bssmail;
use App\Models\Admin;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * The above function is used to register the admin.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Auth",
     *     description="API Endpoints of Admin Auth"
     * )
     *
     * @OA\Post(
     *      path="/admin/registeradmin",
     *      tags={"Admin/Auth"},
     *      summary="Register new admin",
     *      description="An endpoint that register new Admin",
     *      operationId="RegisterAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="username", description="username of selected admin", example="adminusername1", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected admin", example="admin@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="password", description="password of selected admin", example="passwordadmin", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="password",
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="admin", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected admin", example="Admin Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected admin", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="phone", description="phone of selected admin", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="address", description="address of selected admin", example="Jl. Jengki bukan Bintaro", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(name="role", description="role of selected admin", example="Admin", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected admin", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Admin register successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function register(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'status' => 'required',
                'name' => 'required',
                'birthday' => 'required',
                'target' => 'required',
                'role' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username = $request->username;
            $email = $request->email;
            $password = $request->password;
            $hashpass = Hash::make($password);
            $status = $request->status;
            $name = $request->name;
            $birthday = $request->birthday;
            $phone = $request->phone;
            $role = $request->role;
            $img_url = $request->img_url;
            $is_active = $request->is_active;


            if ($request->file('img_url')) {
                $imgadmin = $request->file('img_url');
                $namaimgadmin = $imgadmin->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/admin',
                    $imgadmin,
                    $namaimgadmin
                );
                $imgname = $namaimgadmin;
            } else {
                $path = '';
                $imgname = '';
            }


            $user = User::create([
                'username' => $username,
                'email' => $email,
                'password' => $hashpass,
                'status' => $status,
            ]);

            // $useradmin = User::where('email', $request->email)->first();

            $admin = Admin::create([
                'users_id' => $user->id,
                'name' => $name,
                'birthday' => $birthday,
                'target' => $phone,
                'role' => $role,
                'img_url' => config('app.url') . Storage::url($path),
                'img_name' => $imgname,
                'is_aktif' => $is_active,
            ]);

            $data = User::where('id', $user->id)->with('Admin')->first();

            //adminregisterphone
            $whatsappdata = [
                'target' => $data->Admin->phone,
                //'type' => 'text',
                'message' => '*Dear ' .  $data->Admin->name . '*' . PHP_EOL . PHP_EOL .

                    'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* anda untuk mengakses aplikasi BSS Bintaro admin.' . PHP_EOL . PHP_EOL .

                    'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                    '- BSS Bintaro – ',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdata);
            dispatch($jobwa);

            //adminphone
            $whatsappdatas = [
                'target' => '082155198171',
                //'type' => 'text',
                'message' => '*Dear Admin*' . PHP_EOL . PHP_EOL .

                    'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* dari ' . $username . ' , untuk mengakses aplikasi BSS Bintaro admin.' . PHP_EOL . PHP_EOL .

                    'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                    '- BSS Bintaro – ',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdatas);
            dispatch($jobwa);


            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user->Admin->name,
                'log'               => 'Admin created successfully',
                'data_old'          => null,
                'data_new'          => $admin,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Register successfully',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user->Admin->name,
                'log'               => 'Admin create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is a login function that is used to login to the admin account.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/admin/loginadmin",
     *     tags={"Admin/Auth"},
     *     summary="Login admin",
     *     description="Login admin",
     *     operationId="LoginAdmin",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username","password"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *              @OA\Property(property="password", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Login successfully",
     *     ),
     * )
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $credentials = request(['username', 'password']);

            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username or password is wrong',
                ]);
            }

            $usercek = User::where('username', $request->username)->first();
            $admincek = Admin::where('users_id', $usercek->id)->first();

            if (!$admincek) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'You are not admin',
                    ]
                );
            }

            $useradmin = User::where('username', $request->username)->first();

            if (!Hash::check($request->password, $useradmin->password, [])) {
                throw new \Exception('Invalid Credentials');
            }

            $data = User::where('id', $useradmin->id)->with('Admin')->first();

            $token = $useradmin->createToken('authToken')->plainTextToken;


            return response()->json([
                'Status' => true,
                'Message' => 'Login successfully',
                'access_token' => $token,
                'token_type' => 'Bearer',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Cant access',
                'error' => $error,
            ]);
        }
    }

    /**
     * It deletes the current access token of the user
     *
     * @param Request request The request object.
     *
     * @return A JSON response with a status of true and a message of "Token deleted"
     */
    /**
     * @OA\Post(
     *     path="/admin/logoutadmin",
     *     tags={"Admin/Auth"},
     *     summary="Logout admin",
     *     description="Logout admin",
     *     operationId="LogoutAdmin",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Logout successfully",
     *     ),
     * )
     */
    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();
        return response()->json([
            'Status' => true,
            'Message' => 'Token deleted',
        ]);
    }


    /**
     * The above function is used to update the password of the admin.
     *
     * @param Request request The request object.
     *
     * @return a json response.
     */
    /**
     * @OA\Post(
     *     path="/admin/update/password",
     *     tags={"Admin/Auth"},
     *     summary="Update password admin",
     *     description="Update password admin",
     *     operationId="UpdatePasswordAdmin",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"newpassword"},
     *              @OA\Property(property="newpassword", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Update successfully",
     *     ),
     * )
     */
    public function newpassword(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $user = User::with('Admin')->where('id', Auth::user()->id)->first();
            $admin = Admin::with('User')->where('id', $user->admin->id)->first();
            /* Creating a copy of the admin object. */
            $tempAdmin = $admin->replicate();

            $newpassword = $request->newpassword;
            $hashpass = Hash::make($newpassword);

            if ($user->has_changed_password == 1) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'You have already changed your password',
                ]);
            }

            $validator = Validator::make($request->all(), [
                'newpassword' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $user->update([
                'password' => $hashpass,
                'has_changed_password' => 1,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin password updated successfully',
                'data_old'          => $tempAdmin,
                'data_new'          => $admin,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $admin,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin password update failed',
                'data_old'          => $admin,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to send an email to the user to reset the password.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/admin/resetpass",
     *     tags={"Admin/Auth"},
     *     summary="Reset password admin",
     *     description="Reset password admin",
     *     operationId="ResetPasswordAdmin",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Reset successfully",
     *     ),
     * )
     */
    public function sendemailresetpassword(Request $request)
    {
        try {
            $username = $request->username;

            $user = User::with('Admin')->where('username', $username)->first();

            $email = $user->email;

            if (!$user) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username not found',
                ], 400);
            }

            $resetpassword = [
                'parent' => '',
                'namestudent' => $user->Admin->name,
                'email' => $email,
                'username' => $username,
                'role' => 'admin',
            ];

            Mail::to($email)->send(new Bssmail($resetpassword));

            return response()->json([
                'Status' => true,
                'Message' => 'Email sent successfully',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * It returns the admin data.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin",
     *     description="API Endpoints of Admin"
     * )
     *
     * @OA\Get(
     *      path="/admin/getadmin",
     *      tags={"Admin"},
     *      summary="Get all/selected admin",
     *      description="An endpoint that response all or selected Admin based on id/name",
     *      operationId="GetAdminAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific admin",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging admin",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Admin found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Admin not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getadmin(Request $request)
    {
        $id = $request->id;
        $limit = $request->limit;

        $admin = Admin::with('User');


        if ($id) {
            $admin = $admin->where('id', $id);
        }

        if (!$admin->exists()) {
            return response()->json([
                'Status' => false,
                'Message' => 'Data not found',
            ], 200);
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Admin found',
            'Data' => $admin->paginate($limit),
        ]);
    }

    /**
     * The above function is used to update the admin data.
     *
     * @param Request request The request object.
     *
     * @return the response of the update admin.
     */
    /**
     * @OA\Post(
     *      path="/admin/updateadmin",
     *      tags={"Admin"},
     *      summary="Update admin",
     *      description="An endpoint that update Admin",
     *      operationId="UpdateAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected admin", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected admin", example="admin@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="admin", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected admin", example="Admin Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected admin", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="phone", description="phone of selected admin", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(name="role", description="role of selected admin", example="Admin", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected admin", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Admin register successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updateadmin(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            // $idadmin = Auth::user()->with('Admin');

            $admin = Admin::where('id', $id)->with('User')->first();
            if (!$admin) {
                return response()->json([
                    'Status' => false,
                    'message' => 'Admin not found!'
                ], 404);
            }
            /* Used to create a copy of the admin object. */
            $tempAdmin = $admin->replicate();

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'status' => 'required',
                'name' => 'required',
                'birthday' => 'required',
                'target' => 'required',
                'role' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username = $request->username;
            $email = $request->email;
            $status = $request->status;
            $name = $request->name;
            $birthday = $request->birthday;
            $phone = $request->phone;
            $role = $request->role;
            $img_url = $request->img_url;
            $is_active = $request->is_active;

            if (empty($request->file('img_url'))) {
                $path = $admin->img_url;
                $imgname = $admin->img_name;
            } else {
                Storage::delete('public/admin/' . $admin->img_name);

                $imgadmin = $request->file('img_url');
                $namaimgadmin = $imgadmin->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/admin',
                    $imgadmin,
                    $namaimgadmin
                );
                $path = config('app.url') . Storage::url($loc);
                $imgname = $namaimgadmin;
            }

            if ($username != $admin->User->username) {
                $cekusername = User::where('username', $username)->first();
                if ($cekusername) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Username already exist'
                    ], 400);
                }
            }

            if ($email != $admin->User->email) {
                $cekemail = User::where('email', $email)->first();
                if ($cekemail) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Email already exist'
                    ], 400);
                }
            }

            $admin->update([
                'name' => $name,
                'birthday' => $birthday,
                'target' => $phone,
                'role' => $role,
                'img_url' => $path,
                'img_name' => $imgname,
                'is_active' => $is_active,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin updated successfully',
                'data_old'          => $tempAdmin,
                'data_new'          => $admin,
            ]);

            $admin->User->username = $username;
            $admin->User->email = $email;
            $admin->User->status = $status;
            $admin->push();

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $admin,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin update failed',
                'data_old'          => $admin,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the status of the admin.
     *
     * @param Request request The request object.
     *
     * @return the status of the admin.
     */
    /**
     * @OA\Post(
     *      path="/admin/updatestatusadmin",
     *      tags={"Admin"},
     *      summary="Update status selected admin",
     *      description="An endpoint that update status selected Admin",
     *      operationId="UpdateAdminStatus",
     *      @OA\Parameter(name="id", description="id of selected admin", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="is_active", description="active status of selected admin", example="true", required=true, in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Admin update status successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Admin not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatestatusadmin(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            // $idadmin = Auth::user()->with('Admin');

            $admin = Admin::where('id', $id)->with('User')->first();
            if (!$admin) {
                return response()->json([
                    'message' => 'Admin not found!'
                ], 404);
            }
            /* Creating a copy of the admin object. */
            $tempAdmin = $admin->replicate();

            $validator = Validator::make($request->all(), [
                'is_active' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => false,
                        $validator->errors(),
                    ],
                    400
                );
            }

            $is_active = $request->is_active;

            $admin->update([
                'is_active' => $is_active,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin status updated successfully',
                'data_old'          => $tempAdmin,
                'data_new'          => $admin,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $admin,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin status update failed',
                'data_old'          => $admin,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function deletes an admin.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/deleteadmin",
     *      tags={"Admin"},
     *      summary="Delete selected admin",
     *      description="An endpoint that delete existing Admin based on id",
     *      operationId="DeleteAdmin",
     *      @OA\Parameter(name="id", description="id of selected admin", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Admin deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Admin not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function admindelete(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $user = User::with('Admin')->where('id', Auth::id())->first();
            $id = $request->id;

            $admin = Admin::where('id', $id)->first();
            $user = User::where('id', $admin->users_id)->first();
            if (!$admin) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Admin not found',
                ], 200);
            }
            /* Creating a copy of the admin object. */
            $tempAdmin = $admin->with('User')->replicate();

            $admin->delete();
            $user->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin deleted successfully',
                'data_old'          => $tempAdmin,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'admin',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Admin delete failed',
                'data_old'          => $admin,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
