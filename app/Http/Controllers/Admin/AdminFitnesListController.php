<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FitnesList;
use App\Models\Student;

use Illuminate\Http\Request;

class AdminFitnesListController extends Controller
{
    /**
     * It returns the list of students who are fit or not fit.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Fitnes",
     *     description="API Endpoints of Admin Fitness"
     * )
     *
     * @OA\Get(
     *      path="/admin/fitnes/get",
     *      tags={"Admin/Fitnes"},
     *      summary="Get all/selected fitnes",
     *      description="An endpoint that response all or selected Fitnes based on id",
     *      operationId="GetFitnesAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="groups_id",
     *          description="groups_id to get specific fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="year",
     *          description="year to get specific fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          description="month to get specific fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="day",
     *          description="day to get specific fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging fitnes",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Fitnes found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getfitneslist(Request $request)
    {
        try {
            $id = $request->id;
            $groups_id = $request->groups_id;
            $year = $request->year;
            $month = $request->month;
            $day = $request->day;

            $limit = $request->limit;

            $fitneslist = new Student;

            if ($groups_id) {
                $fitneslist = $fitneslist->where('groups_id', $groups_id);
            }

            if ($year && $month) {
                $fitneslistmonth = FitnesList::whereMonth('created_at', $month)->whereYear('created_at', $year);
                $fitneslist = $fitneslist->with(['Fitnes_list' => function ($query) use ($year, $month) {
                    $query->whereYear('created_at', $year)->whereMonth('created_at', $month);
                }])->whereIn('id', $fitneslistmonth->pluck('students_id'));
            }

            if ($year && $month && $day) {
                $fitneslistdate = FitnesList::whereDay('created_at', $day)->whereMonth('created_at', $month)->whereYear('created_at', $year);
                $fitneslist = $fitneslist->with(['Fitnes_list' => function ($query) use ($year, $month, $day) {
                    $query->orderBy('is_fit', 'DESC')->whereYear('created_at', $year)->whereMonth('created_at', $month)->whereDay('created_at', $day);
                }])->whereIn('id', $fitneslistdate->pluck('students_id'));
            }

            if ($id) {
                $fitneslist = FitnesList::whereDay('created_at', $day)->whereMonth('created_at', $month)->whereYear('created_at', $year)->where('id', $id);
            }

            if (!$fitneslist->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Fitness List not found',
                ], 200);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $fitneslist->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Fitness list found',
                'Data' => $fitneslist->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
