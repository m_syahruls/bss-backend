<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendWhatsappJob;
use App\Models\User;
use App\Models\Coach;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminCoachController extends Controller
{
    /**
     * It returns a list of coaches.
     *
     * @param Request request The request object.
     *
     * @return A list of coaches
     */
    /**
     * @OA\Tag(
     *     name="Admin/Coach",
     *     description="API Endpoints of Admin Coach Management"
     * )
     *
     * @OA\Get(
     *      path="/admin/coach/get",
     *      tags={"Admin/Coach"},
     *      summary="Get all/selected coach",
     *      description="An endpoint that response all or selected Coach based on id/name",
     *      operationId="GetCoachAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific coach",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging coach",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          description="name to get all coach",
     *          example="Coach Name",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Coach found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Coach not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getcoach(Request $request)
    {
        $id = $request->id;
        $limit = $request->limit;
        $name = $request->name;

        $coach = Coach::with('User', 'Groups');
        if ($id) {
            $coach = $coach->where('id', $id);
        }

        if ($name) {
            $coach = $coach->where('name', 'like', '%' . $name . '%');
        }

        // if there is no $limit request
        if (!$limit) {
            $limit = $coach->count();
        }

        if (!$coach->exists()) {
            return response()->json([
                'Status' => false,
                'Message' => 'Coach not found',
            ], 200);
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Coach found',
            'Data' => $coach->paginate($limit),
        ]);
    }

    /**
     * The above function is used to register a coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/coach/register",
     *      tags={"Admin/Coach"},
     *      summary="Register new coach",
     *      description="An endpoint that register new Coach",
     *      operationId="RegisterCoach",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="username", description="username of selected coach", example="coachusername1", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected coach", example="coach@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="password", description="password of selected coach", example="passwordcoach", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="password",
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="coach", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected coach", example="Coach Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected coach", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="phone", description="phone of selected coach", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="address", description="address of selected coach", example="Jl. Jengki bukan Bintaro", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(name="is_active", description="active status of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected coach", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Coach register successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function registercoach(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'status' => 'required',
                'name' => 'required',
                'birthday' => 'required',
                'target' => 'required',
                'address' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username   = $request->username;
            $email      = $request->email;
            $password   = $request->password;
            $hashpass   = Hash::make($password);
            $status     = $request->status;
            $name       = $request->name;
            $birthday   = $request->birthday;
            $phone      = $request->phone;
            $img_url    = $request->img_url;
            $is_active  = $request->is_active;
            $address    = $request->address;
            $created_at = $request->created_at;


            if ($request->file('img_url')) {
                $imgcoach = $request->file('img_url');
                $namaimgcoach = $imgcoach->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/coach',
                    $imgcoach,
                    $namaimgcoach
                );
                $imgname = $namaimgcoach;
            } else {
                $path = '';
                $imgname = '';
            }

            $user = User::create([
                'username' => $username,
                'email' => $email,
                'password' => $hashpass,
                'status' => $status,
            ]);

            $coach = Coach::create([
                'users_id' => $user->id,
                'name' => $name,
                'birthday' => $birthday,
                'target' => $phone,
                'img_url' => config('app.url') . Storage::url($path),
                'img_name' => $imgname,
                'is_active' => $is_active,
                'address' => $address,
            ]);

            $data = User::where('id', $user->id)->with('Coach')->first();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach created successfully',
                'data_old'          => null,
                'data_new'          => $data,
            ]);

            //coachphone
            $whatsappdata = [
                'target' => $data->Coach->phone,
                //'type' => 'text',
                'message' => '*Dear ' .  $data->Coach->name .  '*' . PHP_EOL . PHP_EOL .

                    'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* anda untuk mengakses aplikasi BSS Bintaro coach.' . PHP_EOL . PHP_EOL .

                    'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                    '- BSS Bintaro – ',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdata);
            dispatch($jobwa);

            //adminphone
            $whatsappdatas = [
                'target' => '082155198171',
                //'type' => 'text',
                'message' => '*Dear Admin*' . PHP_EOL . PHP_EOL .

                    'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* dari ' . $username . ' , untuk mengakses aplikasi BSS Bintaro coach.' . PHP_EOL . PHP_EOL .

                    'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                    '- BSS Bintaro – ',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdatas);
            dispatch($jobwa);


            return response()->json([
                'Status' => true,
                'Message' => 'Register successfully',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the coach data.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/coach/update",
     *      tags={"Admin/Coach"},
     *      summary="Update selected coach",
     *      description="An endpoint that update selected Coach",
     *      operationId="UpdateCoach",
     *      @OA\Parameter(name="id", description="id of selected coach", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="username", description="username of selected coach", example="coachusername1", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected coach", example="coach@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="password", description="password of selected coach", example="passwordcoach", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="password",
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="coach", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected coach", example="Coach Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected coach", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="phone", description="phone of selected coach", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="address", description="address of selected coach", example="Jl. Jengki bukan Bintaro", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="is_active", description="active status of selected coach", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected coach", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Coach update successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Coach not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatecoach(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $coach = Coach::where('id', $id)->with('User')->first();
            if (!$coach) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Coach not found!'
                    ],
                    404
                );
            }
            /* Used to create a copy of the coach object. */
            $tempCoach = $coach->replicate();

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'status' => 'required',
                'name' => 'required',
                'birthday' => 'required',
                'target' => 'required',
                'address' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username = $request->username;
            $email = $request->email;
            $status = $request->status;
            $name = $request->name;
            $birthday = $request->birthday;
            $phone = $request->phone;
            $img_url = $request->img_url;
            $is_active = $request->is_active;
            $address = $request->address;
            $created_at = $request->created_at;

            if (empty($request->file('img_url'))) {
                $path = $coach->img_url;
                $imgname = $coach->img_name;
            } else {
                Storage::delete('public/coach/' . $coach->img_name);

                $imgcoach = $request->file('img_url');
                $namaimgcoach = $imgcoach->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/coach',
                    $imgcoach,
                    $namaimgcoach
                );
                $path = config('app.url') . Storage::url($loc);
                $imgname = $namaimgcoach;
            }


            if ($username != $coach->User->username) {
                $checkusername = User::where('username', $username)->first();
                if ($checkusername) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Username already exist',
                    ], 400);
                }
            }

            if ($email != $coach->User->email) {
                $checkemail = User::where('email', $email)->first();
                if ($checkemail) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Email already exist',
                    ], 400);
                }
            }

            $coach->update([
                'name' => $name,
                'birthday' => $birthday,
                'target' => $phone,
                'img_url' => $path,
                'img_name' => $imgname,
                'is_active' => $is_active,
                'address' => $address,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach updated successfully',
                'data_old'          => $tempCoach,
                'data_new'          => $coach,
            ]);

            $coach->User->username = $username;
            $coach->User->email = $email;
            $coach->User->status = $status;
            $coach->push();

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $coach,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach update failed',
                'data_old'          => $coach,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to update the status of the coach
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/coach/update/status",
     *      tags={"Admin/Coach"},
     *      summary="Update status selected coach",
     *      description="An endpoint that update status selected Coach",
     *      operationId="UpdateCoachStatus",
     *      @OA\Parameter(name="id", description="id of selected coach", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="is_active", description="active status of selected coach", example="true", required=true, in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Coach update status successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Coach not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatestatuscoach(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $coach = Coach::where('id', $id)->with('User')->first();
            if (!$coach) {
                return response()->json([
                    'Message' => 'Coach not found!'
                ], 404);
            }
            /* Used to create a copy of the coach object. */
            $tempCoach = $coach->replicate();

            $validator = Validator::make($request->all(), [
                'is_active' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $is_active = $request->is_active;

            $coach->update([
                'is_active' => $is_active,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach updated successfully',
                'data_old'          => $tempCoach,
                'data_new'          => $coach,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $coach,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Coach update failed',
                'data_old'          => $coach,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
