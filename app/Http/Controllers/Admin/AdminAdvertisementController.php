<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminAdvertisementController extends Controller
{
    /**
     * It returns all the advertisements in the database.
     *
     * @param Request request id, limit, is_all
     */
    /**
     * @OA\Tag(
     *     name="Admin/Advertisement",
     *     description="API Endpoints of Admin Advertisements"
     * )
     *
     * @OA\Get(
     *      path="/admin/advertisement/get",
     *      tags={"Admin/Advertisement"},
     *      summary="Get all/selected advertisement",
     *      description="An endpoint that response all or selected Advertisement based on id",
     *      operationId="GetAdvertisementAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific advertisement",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging advertisement",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_all",
     *          description="parameter to get all advertisement",
     *          example="true",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Advertisement found",
     *          @OA\JsonContent(
     *              @OA\Property(property="Status", type="bool", example="true"),
     *              @OA\Property(property="Message", type="string", example="Advertisement found"),
     *              @OA\Property(property="Data",
     *                  @OA\Property(property="data", type="array",
     *                      example={
     *                          {
     *                              "id": "2",
     *                              "owner": "Owner Name 2",
     *                              "title": "Title of Ads 2",
     *                              "desc": "Lorem ipsum dolor sit amet",
     *                              "date_airing": "2022-02-02",
     *                              "url_link": "https://synapsis.id",
     *                              "image_link": "https://synapsis.id/storage/image/gambar2.jpg",
     *                              "image_name": "gambar2.jpg",
     *                              "created_at": "2022-02-02 20:02:20",
     *                              "updated_at": "2022-02-02 20:02:20",
     *                          }, {
     *                              "id": "1",
     *                              "owner": "Owner Name 1",
     *                              "title": "Title of Ads 1",
     *                              "desc": "Lorem ipsum dolor sit amet",
     *                              "date_airing": "2022-02-02",
     *                              "url_link": "https://synapsis.id",
     *                              "image_link": "https://synapsis.id/storage/image/gambar1.png",
     *                              "image_name": "gambar1.png",
     *                              "created_at": "2022-02-02 20:02:20",
     *                              "updated_at": "2022-02-02 20:02:20",
     *                          },
     *                      },
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="int", example=""),
     *                          @OA\Property(property="owner", type="string", example=""),
     *                          @OA\Property(property="title", type="string", example=""),
     *                          @OA\Property(property="desc", type="string", example=""),
     *                          @OA\Property(property="date_airing", type="string", example=""),
     *                          @OA\Property(property="url_link", type="string", example=""),
     *                          @OA\Property(property="image_link", type="string", example=""),
     *                          @OA\Property(property="image_name", type="string", example=""),
     *                          @OA\Property(property="created_at", type="string", example=""),
     *                          @OA\Property(property="updated_at", type="string", example=""),
     *                      ),
     *                  ),
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getAdvertisement(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $is_all = $request->is_all;

            // query
            $advertisement = Advertisement::query();

            if ($is_all) {
                // if there is no $limit request
                if (!$limit) {
                    $limit = $advertisement->count();
                }

                return response()->json([
                    'Status' => true,
                    'Message' => 'Advertisemend found',
                    'Data' => $advertisement->orderBy('created_at', 'DESC')->paginate($limit),
                ]);
            }

            // if there is $id request
            if ($id) {
                $advertisement = $advertisement->find($id);
            } else {
                // query airing ads
                $advertisement = $advertisement->whereDate('date_airing', '>=', \Carbon\Carbon::now())->get();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Advertisemend found',
                'Data' => $advertisement,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create an advertisement.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/advertisement/create",
     *      tags={"Admin/Advertisement"},
     *      summary="Create new advertisement",
     *      description="An endpoint that create new Advertisement",
     *      operationId="CreateAdvertisement",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="owner", description="owner of selected advertisement", example="Nama Pengiklan", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="title", description="title of selected advertisement", example="Judul Iklan", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected advertisement", example="Deskripsi dari iklan ampuh untu meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="url_link", description="link of selected advertisement", example="https://synapsis.id", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="image_link", description="image of selected advertisement", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Parameter(name="date_airing", description="date airing of selected advertisement", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Advertisement created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createAdvertisement(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'owner'         => 'required',
                'title'         => 'required',
                'desc'          => 'required',
                'date_airing'   => 'required',
                'url_link'      => 'required',
                'image_link'    => 'max:1240|mimes:jpeg,jpg,png',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            if ($request->file('image_link')) {
                $file = $request->file('image_link');
                $name = $file->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/advertisement',
                    $file,
                    $name
                );
            } else {
                $path = '';
                $name = '';
            }

            $advertisement = Advertisement::create([
                'owner'         => $request->owner,
                'title'         => $request->title,
                'desc'          => $request->desc,
                'date_airing'   => $request->date_airing,
                'url_link'      => $request->url_link,
                'image_link'    => config('app.url') . Storage::url($path),
                'image_name'    => $name,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement created successfully',
                'data_old'          => null,
                'data_new'          => $advertisement,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Advertisement created successfully',
                'Data' => $advertisement,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the advertisement.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/advertisement/update",
     *      tags={"Admin/Advertisement"},
     *      summary="Update selected advertisement",
     *      description="An endpoint that update existing Advertisement based on id",
     *      operationId="UpdateAdvertisement",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected advertisement", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="owner", description="owner of selected advertisement", example="Nama Pengiklan", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="title", description="title of selected advertisement", example="Judul Iklan", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected advertisement", example="Deskripsi dari iklan ampuh untu meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="url_link", description="link of selected advertisement", example="https://synapsis.id", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="image_link", description="image of selected advertisement", example="namafile.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Parameter(name="date_airing", description="date airing of selected advertisement", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Advertisement updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Advertisement not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updateAdvertisement(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $advertisement = Advertisement::where('id', $request->id)->first();
            if (!$advertisement) {
                return response()->json([
                    'message' => 'Advertisement not found'
                ], 404);
            }
            /* Used to create a copy of the advertisement object. */
            $tempAdvertisement = $advertisement->replicate();

            $validator = Validator::make($request->all(), [
                'owner'         => 'required',
                'title'         => 'required',
                'desc'          => 'required',
                'date_airing'   => 'required',
                'url_link'      => 'required',
                'image_link'    => 'max:1240|mimes:jpeg,jpg,png',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            if (empty($request->file('image_link'))) {
                $path = $advertisement->image_link;
                $name = $advertisement->image_name;
            } else {
                Storage::delete('public/advertisement/' . $advertisement->image_name);

                $file = $request->file('image_link');
                $name = $file->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/advertisement',
                    $file,
                    $name
                );

                $path = config('app.url') . Storage::url($loc);
            }

            $advertisement->update([
                'owner'         => $request->owner,
                'title'         => $request->title,
                'desc'          => $request->desc,
                'date_airing'   => $request->date_airing,
                'url_link'      => $request->url_link,
                'image_link'    => $path,
                'image_name'    => $name,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement updated successfully',
                'data_old'          => $tempAdvertisement,
                'data_new'          => $advertisement,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Advertisement updated successfully',
                'Data' => $advertisement,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement update failed',
                'data_old'          => $advertisement,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * Delete an advertisement
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/advertisement/delete",
     *      tags={"Admin/Advertisement"},
     *      summary="Delete selected advertisement",
     *      description="An endpoint that delete existing Advertisement based on id",
     *      operationId="DeleteAdvertisement",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected advertisement", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Advertisement deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Advertisement not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deleteAdvertisement(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $advertisement = Advertisement::where('id', $request->id)->first();
            if (!$advertisement) {
                return response()->json([
                    'message' => 'Advertisement not found'
                ], 404);
            }
            /* Used to create a copy of the advertisement object. */
            $tempAdvertisement = $advertisement->replicate();

            Storage::delete('public/advertisement/' . $advertisement->image_link);

            $advertisement->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement deleted successfully',
                'data_old'          => $tempAdvertisement,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Advertisement deleted successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'advertisement',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Advertisement delete failed',
                'data_old'          => $advertisement,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
