<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Reward;
use App\Models\RewardReceiver;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminRewardController extends Controller
{
    /**
     * It gets the reward from the database.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Reward",
     *     description="API Endpoints of Admin Rewards"
     * )
     *
     * @OA\Get(
     *      path="/admin/reward/get",
     *      tags={"Admin/Reward"},
     *      summary="Get all/selected reward",
     *      description="An endpoint that response all or selected Reward based on id",
     *      operationId="GetRewardAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific reward",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging reward",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Reward found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getreward(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $reward = Reward::with('RewardReceivers');

            if (!$reward->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 200);
            }

            if ($id) {
                $reward = $reward->where('id', $id);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $reward->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Reward get successfully',
                'Data' => $reward->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create a reward.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/create",
     *      tags={"Admin/Reward"},
     *      summary="Create new reward !! NOT FINISH",
     *      description="An endpoint that create new Reward",
     *      operationId="CreateReward",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="title", description="title of selected reward", example="5th year together", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected reward", example="Congrats ampuh untuk meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="reward_type", description="reward type of selected reward", example="1x 25% Discount", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="image", description="image of selected reward", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Reward created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    // !! Kurang paham isi $request/parameter yang dibutuhkan
    public function createreward(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
                'reward_type' => 'required',
                'image' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $title = $request->title;
            $desc = $request->desc;
            $reward_type = $request->reward_type;
            $image = $request->image;

            $students_id = $request->students_id;
            $status = $request->status;
            $date_claimed = $request->date_claimed;

            $is_student = $request->is_student;


            if ($request->file('image')) {
                $imgreward = $request->file('image');
                $namaimgreward = $imgreward->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/reward',
                    $imgreward,
                    $namaimgreward
                );
                $imgname = $namaimgreward;
            } else {
                $path = '';
                $imgname = '';
            }

            $reward = Reward::create([
                'title' => $title,
                'desc' => $desc,
                'reward_type' => $reward_type,
                'image' =>  config('app.url') . Storage::url($path),
                'img_name' => $imgname,
            ]);



            if ($is_student == 1) {

                foreach ($request->students_id as $key => $value) {
                    $rewardreceiverlooparray = [
                        'rewards_id' => $reward->id,
                        'students_id' => $value,
                        'status' => $status,
                        'date_claimed' => $date_claimed,
                    ];
                    RewardReceiver::firstorCreate($rewardreceiverlooparray);
                }
            } else if ($is_student == 0) {
                $groups_id = $request->groups_id;
                $cekgroup = Student::where('groups_id', $groups_id);
                if (!$cekgroup->exists()) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Student not found',
                    ], 404);
                }

                $student = Student::where('groups_id', $groups_id)->get();

                foreach ($student->pluck('id') as $key => $value) {
                    $rewardreceiverlooparray = [
                        'rewards_id' => $reward->id,
                        'students_id' => $value,
                        'status' => $status,
                        'date_claimed' => $date_claimed,
                    ];
                    RewardReceiver::firstorCreate($rewardreceiverlooparray);
                }
            }


            $rewardresult = Reward::with('RewardReceivers')->where('id', $reward->id)->first();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward created successfully',
                'data_old'          => null,
                'data_new'          => $rewardresult,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward create successfully',
                'Data' => $rewardresult,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the reward.
     *
     * @param Request request The request object.
     *
     * @return a response in JSON format.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/update",
     *      tags={"Admin/Reward"},
     *      summary="Update reward",
     *      description="An endpoint that update Reward",
     *      operationId="UpdateReward",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected reward", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="title", description="title of selected reward", example="5th year together", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="desc", description="desc of selected reward", example="Congrats ampuh untuk meredakan kantuk", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="reward_type", description="reward type of selected reward", example="1x 25% Discount", required=true, in="query",
     *          @OA\Schema(
     *              type="text"
     *          )
     *      ),
     *      @OA\Parameter(name="image", description="image of selected reward", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Reward updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatereward(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            $reward = Reward::where('id', $id)->first();
            if (!$reward) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 404);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            if (empty($request->file('image'))) {
                $path = $reward->image;
                $imgname = $reward->img_name;
            } else {
                Storage::delete('public/reward/' . $reward->img_name);

                $imgreward = $request->file('image');
                $namaimgreward = $imgreward->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/reward',
                    $imgreward,
                    $namaimgreward
                );
                $path = config('app.url') . Storage::url($loc);
                $imgname = $namaimgreward;
            }

            $reward->update([
                'title' => $request->title,
                'desc' => $request->desc,
                'reward_type' => $request->reward_type,
                'image' => $path,
                'img_name' => $imgname,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward updated successfully',
                'data_old'          => $tempReward,
                'data_new'          => $reward,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward update successfully',
                'Data' => $reward,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward update failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to delete a reward.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/delete",
     *      tags={"Admin/Reward"},
     *      summary="Delete selected reward",
     *      description="An endpoint that delete existing Reward based on id",
     *      operationId="DeleteReward",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected reward", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Reward deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Reward not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deletereward(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            $reward = Reward::where('id', $id)->first();
            if (!$reward) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 404);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            $cekreward = RewardReceiver::where('rewards_id', $id)->first();
            if ($cekreward) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward cant delete',
                ], 404);
            }

            $reward->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward deleted successfully',
                'data_old'          => $tempReward,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reward',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward delete failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * This function is used to get the reward receiver.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/RewardReceiver",
     *     description="API Endpoints of Admin RewardReceivers"
     * )
     *
     * @OA\Get(
     *      path="/admin/reward/receiver/get",
     *      tags={"Admin/RewardReceiver"},
     *      summary="Get all/selected reward/receiver",
     *      description="An endpoint that response all or selected RewardReceiver based on id",
     *      operationId="GetRewardReceiverAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="rewards_id",
     *          description="rewards_id to get specific reward/receiver",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="status",
     *          description="status to get specific reward/receiver",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging reward/receiver",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RewardReceiver found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getrewardreceiver(Request $request)
    {
        try {
            $rewards_id = $request->rewards_id;
            $status = $request->status;
            $limit = $request->limit;
            $reward = RewardReceiver::with('Student', 'Reward');

            if ($status) {
                $reward = $reward->where('status', $status);
            }

            if (!$reward->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 200);
            }

            if ($rewards_id) {
                $reward = $reward->where('rewards_id', $rewards_id);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $reward->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Reward get successfully',
                'Data' => $reward->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to create a reward receiver
     *
     * @param Request request This is the request object that contains the data that was sent to the
     * server.
     *
     * @return A list of all the rewards.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/receiver/create",
     *      tags={"Admin/RewardReceiver"},
     *      summary="Create new reward/receiver",
     *      description="An endpoint that create new RewardReceiver",
     *      operationId="CreateRewardReceiver",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="rewards_id", description="rewards_id of selected reward/receiver", example="Nama Pengiklan", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RewardReceiver created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    // !! Kurang paham isi $request/parameter yang dibutuhkan
    public function createrewardreceiver(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'rewards_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $rewards_id = $request->rewards_id;
            $students_id = $request->students_id;
            $status = $request->status;
            $date_claimed = $request->date_claimed;

            $reward = Reward::with('RewardReceivers')->where('id', $rewards_id)->first();
            if (!$reward) {
                return response()->json([
                    'message' => 'Reward not found'
                ], 404);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            foreach ($request->students_id as $key => $value) {
                $rewardreceiverlooparray = [
                    'rewards_id' => $rewards_id,
                    'students_id' => $value,
                    'status' => $status,
                    'date_claimed' => $date_claimed,
                ];
                RewardReceiver::firstorCreate($rewardreceiverlooparray);
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward receiver created successfully',
                'data_old'          => $tempReward,
                'data_new'          => $reward,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward receiver create successfully',
                'Data' => $reward,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward receiver create failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to approve a reward.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/receiver/update",
     *      tags={"Admin/RewardReceiver"},
     *      summary="Update new reward/receiver",
     *      description="An endpoint that update new RewardReceiver",
     *      operationId="UpdateRewardReceiver",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected reward/receiver", example="Nama Pengiklan", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RewardReceiver updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="RewardReceiver not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function approverewardreceiver(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            $reward = RewardReceiver::where('id', $id)->first();
            if (!$reward) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 404);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            $reward->update([
                'status' => 'REWARD_CLAIMED',
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward approved successfully',
                'data_old'          => $tempReward,
                'data_new'          => $reward,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward approved successfully',
                'Data' => $reward,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward approved failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function deletes a reward receiver.
     *
     * @param Request request The request object.
     *
     * @return A list of all the reward receivers.
     */
    /**
     * @OA\Post(
     *      path="/admin/reward/receiver/delete",
     *      tags={"Admin/RewardReceiver"},
     *      summary="Delete selected reward/receiver",
     *      description="An endpoint that delete existing RewardReceiver based on id",
     *      operationId="DeleteRewardReceiver",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected reward/receiver", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RewardReceiver deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="RewardReceiver not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deleterewardreceiver(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;
            $reward = RewardReceiver::where('id', $id)->first();
            if (!$reward) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 200);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            $reward->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward receiver deleted successfully',
                'data_old'          => $tempReward,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward receiver delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Reward receiver delete failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
