<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReportMonthlyRolePositions;
use App\Models\RolePosition;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminRolePositionController extends Controller
{
    /**
     * This function is used to get the role position.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/RolePosition",
     *     description="API Endpoints of Admin RolePositions"
     * )
     *
     * @OA\Get(
     *      path="/admin/roleposition/get",
     *      tags={"Admin/RolePosition"},
     *      summary="Get all/selected roleposition",
     *      description="An endpoint that response all or selected RolePosition based on id",
     *      operationId="GetRolePositionAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific roleposition",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging roleposition",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          description="parameter to get named roleposition",
     *          example="true",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RolePosition found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getroleposition(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $name = $request->name;

            $roleposition = new RolePosition;

            if ($id) {
                $roleposition = $roleposition->where('id', $id);
            }

            if ($name) {
                $roleposition = $roleposition->where('name', 'like', '%' . $name . '%');
            }

            if (!$roleposition->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Role Position not found',
                ], 200);
            }

            // if there is no $limit request
            if (!$limit) {
                $limit = $roleposition->count();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Role position found',
                'Data' => $roleposition->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to create a role position.
     *
     * @param Request request The request object.
     *
     * @return A response is being returned.
     */
    /**
     * @OA\Post(
     *      path="/admin/roleposition/create",
     *      tags={"Admin/RolePosition"},
     *      summary="Create new roleposition",
     *      description="An endpoint that create new RolePosition",
     *      operationId="CreateRolePosition",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="name", description="name of selected roleposition", example="Role Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="position", description="position of selected roleposition", example="CF", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="role", description="role of selected roleposition", example="Footbal Owner", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="image_link", description="image of selected roleposition", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RolePosition created successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function createroleposition(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'position'  => 'required',
                'role'      => 'required',
                'image_link' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;
            $image_link = $request->image_link;
            $position = $request->position;
            $role = $request->role;

            if ($request->file('image_link')) {
                $imgrole = $request->file('image_link');
                $namaimgrole = $imgrole->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/role',
                    $imgrole,
                    $namaimgrole
                );
                $imgname = $namaimgrole;
            } else {
                $path = '';
                $imgname = '';
            }


            $roleposition = RolePosition::create([
                'name' => $name,
                'image_link' => config('app.url') . Storage::url($path),
                'img_name' => $imgname,
                'position' => $position,
                'role' => $role,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position created successfully',
                'data_old'          => null,
                'data_new'          => $roleposition,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Role position create successfully',
                'Data' => $roleposition,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the role position.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/roleposition/update",
     *      tags={"Admin/RolePosition"},
     *      summary="Update selected roleposition",
     *      description="An endpoint that update existing RolePosition based on id",
     *      operationId="UpdateRolePosition",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected roleposition", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected roleposition", example="Role Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="position", description="position of selected roleposition", example="CF", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="role", description="role of selected roleposition", example="Footbal Owner", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="image_link", description="image of selected roleposition", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RolePosition updated successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="RolePosition not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updateroleposition(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $roleposition = RolePosition::where('id', $id)->first();
            if (!$roleposition) {
                return response()->json([
                    'message' => 'Role position not found'
                ], 404);
            }
            /* Used to create a copy of the roleposition object. */
            $tempRoleposition = $roleposition->replicate();

            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'position'  => 'required',
                'role'      => 'required',
                'image_link' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;
            $image_link = $request->image_link;
            $position = $request->position;
            $role = $request->role;

            if (empty($request->file('image_link'))) {
                $path = $roleposition->image_link;
                $img_name = $roleposition->img_name;
            } else {
                Storage::delete('public/role/' . $roleposition->img_name);

                $imgrole = $request->file('image_link');
                $namaimgrole = $imgrole->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/role',
                    $imgrole,
                    $namaimgrole
                );

                $path = config('app.url') . Storage::url($loc);
                $img_name = $namaimgrole;
            }

            $roleposition->update([
                'name' => $name,
                'image_link' => $path,
                'img_name' => $img_name,
                'position' => $position,
                'role' => $role
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position updated successfully',
                'data_old'          => $tempRoleposition,
                'data_new'          => $roleposition,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $roleposition,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position update failed',
                'data_old'          => $roleposition,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to delete a role position.
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/roleposition/delete",
     *      tags={"Admin/RolePosition"},
     *      summary="Delete selected roleposition",
     *      description="An endpoint that delete existing RolePosition based on id",
     *      operationId="DeleteRolePosition",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected roleposition", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="RolePosition deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="RolePosition not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deleteroleposition(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $roleposition = RolePosition::where('id', $id)->first();
            if (!$roleposition) {
                return response()->json([
                    'message' => 'Role position not found'
                ], 404);
            }
            /* Used to create a copy of the roleposition object. */
            $tempRoleposition = $roleposition->replicate();

            $reportmonthly = ReportMonthlyRolePositions::where('role_positions_id', $id);

            if ($reportmonthly->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Role position is used in report monthly',
                ], 404);
            }

            $roleposition->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position deleted successfully',
                'data_old'          => $tempRoleposition,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'rolePosition',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Role position delete failed',
                'data_old'          => $roleposition,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
