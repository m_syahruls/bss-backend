<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendWhatsappJob;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminStudentController extends Controller
{
    /**
     * The above function is used to get the student data.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Student",
     *     description="API Endpoints of Admin Student Management"
     * )
     *
     * @OA\Get(
     *      path="/admin/student/get",
     *      tags={"Admin/Student"},
     *      summary="Get all/selected student",
     *      description="An endpoint that response all or selected Student based on id/groups_id",
     *      operationId="GetStudentAdmin",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="id to get specific student",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit to get paging student",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="groups_id",
     *          description="groups id to get all selected student",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Student found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Student not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getstudent(Request $request)
    {
        $id = $request->id;
        $limit = $request->limit;
        $groups_id = $request->groups_id;

        $student = Student::with('User')->with('Groups');

        if ($id) {
            $student = $student->where('id', $id);
        }

        if ($groups_id) {
            $student->where('groups_id', $groups_id);
        }

        // if there is no $limit request
        if (!$limit) {
            $limit = $student->count();
        }

        if (!$student->exists()) {
            return response()->json([
                'Status' => false,
                'Message' => 'Data not found',
            ], 200);
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Student found',
            'Data' => $student->paginate($limit),
        ]);
    }

    /**
     * The above function is used to register a student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *      path="/admin/student/register",
     *      tags={"Admin/Student"},
     *      summary="Register new student",
     *      description="An endpoint that register new Student",
     *      operationId="RegisterStudent",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="username", description="username of selected student", example="studentusername1", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected student", example="student@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="password", description="password of selected student", example="passwordstudent", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="password",
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="student", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="groups_id", description="groups id of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="registration_no", description="registration number of selected user", example="128973214", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected student", example="Student Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="nick_name", description="nick name of selected student", example="Stud", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="mother_name", description="mother name of selected student", example="Mother Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="mother_phone", description="mother phone of selected student", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="father_name", description="father name of selected student", example="Father Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="father_phone", description="father phone of selected student", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected student", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="category", description="category of selected student", example="U-21", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(name="is_active", description="active status of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected student", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Student register successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function registerstudent(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'username' => 'required|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'status' => 'required',
                'groups_id' => 'required',
                'registration_no' => 'required',
                'name' => 'required',
                'nick_name' => 'required',
                'mother_name' => 'required',
                'mother_phone' => 'required',
                'father_name' => 'required',
                'father_phone' => 'required',
                'birthday' => 'required',
                'category' => 'required',
                'is_active' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username = $request->username;
            $email = $request->email;
            $password = $request->password;
            $hashpass = Hash::make($password);
            $status = $request->status;
            $groups_id = $request->groups_id;
            $registration_no = $request->registration_no;
            $name = $request->name;
            $nick_name = $request->nick_name;
            $mother_name = $request->mother_name;
            $mother_phone = $request->mother_phone;
            $father_name = $request->father_name;
            $father_phone = $request->father_phone;
            $birthday = $request->birthday;
            $img_url = $request->img_url;
            $joined_at = $request->joined_at;
            $category = $request->category;
            $is_active = $request->is_active;
            $va_number = $request->va_number;
            $created_at = $request->created_at;



            if ($request->file('img_url')) {
                $imgstudent = $request->file('img_url');
                $namaimgstudent = $imgstudent->getClientOriginalName();
                $path = Storage::putFileAs(
                    'public/student',
                    $imgstudent,
                    $namaimgstudent
                );
                $imgname = $namaimgstudent;
            } else {
                $path = '';
                $imgname = '';
            }

            $user = User::create([
                'username' => $username,
                'email' => $email,
                'password' => $hashpass,
                'status' => $status,
            ]);

            Student::create([
                'users_id' => $user->id,
                'groups_id' => $groups_id,
                'registration_no' => $registration_no,
                'name' => $name,
                'nick_name' => $nick_name,
                'mother_name' => $mother_name,
                'mother_phone' => $mother_phone,
                'father_name' => $father_name,
                'father_phone' => $father_phone,
                'birthday' => $birthday,
                'img_url' => config('app.url') . Storage::url($path),
                'img_name' => $imgname,
                'joined_at' => $joined_at,
                'category' => $category,
                'is_active' => $is_active,
                'va_number' => $va_number,
            ]);

            $data = User::where('id', $user->id)->with('Student')->first();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student created successfully',
                'data_old'          => null,
                'data_new'          => $data,
            ]);

            //parentphone
            $phone = [$data->Student->mother_phone, $data->Student->father_phone];
            $parent_name = [$data->Student->mother_name, $data->Student->father_name];
            for ($kunci = 0; $kunci < 2; $kunci++) {
                $whatsappdata = [
                    'target' => $phone[$kunci],
                    //'type' => 'text',
                    'message' => '*Dear Bapak/ibu ' .  $parent_name[$kunci] . ' orangtua dari ' . $data->Student->name . '*' . PHP_EOL . PHP_EOL .

                        'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* anda untuk mengakses aplikasi BSS Bintaro student.' . PHP_EOL . PHP_EOL .

                        'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                        'Download aplikasi BSS di:' . PHP_EOL .

                        '*playstore:* https://play.google.com/store/apps/details?id=com.synapsisid.bss_student' . PHP_EOL . PHP_EOL .

                        'atau' . PHP_EOL . PHP_EOL .

                        '*appstore:* https://apps.apple.com/id/app/bss-bintaro/id1614410288' . PHP_EOL . PHP_EOL .

                        '- BSS Bintaro – ',

                    'schedule' => '0'
                ];
                $jobwa = new SendWhatsappJob($whatsappdata);
                dispatch($jobwa);
            }


            //adminphone
            $whatsappdatas = [
                'target' => '082155198171',
                //'type' => 'text',
                'message' => '*Dear Admin*' . PHP_EOL . PHP_EOL .

                    'Berikut adalah username : *'  . $username . '* dan password: *'  . $password . '* dari ' . $username . ' , untuk mengakses aplikasi BSS Bintaro student.' . PHP_EOL . PHP_EOL .

                    'Segera ubah password anda saat login (8 digit) pertama kali di aplikasi BSS Bintaro dan jangan berikan kepada siapapun.' . PHP_EOL . PHP_EOL .

                    '- BSS Bintaro – ',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdatas);
            dispatch($jobwa);


            return response()->json([
                'Status' => true,
                'Message' => 'Register successfully',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the student data.
     *
     * @param Request request The request object.
     *
     * @return a response in JSON format.
     */
    /**
     * @OA\Post(
     *      path="/admin/student/update",
     *      tags={"Admin/Student"},
     *      summary="Update selected student",
     *      description="An endpoint that update selected Student",
     *      operationId="UpdateStudent",
     *      @OA\Parameter(name="id", description="id of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Parameter(name="username", description="username of selected student", example="studentusername1", required=false, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="email", description="email of selected student", example="student@email.com", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="password", description="password of selected student", example="passwordstudent", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="password",
     *          )
     *      ),
     *      @OA\Parameter(name="status", description="status of selected user", example="student", required=false, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="groups_id", description="groups id of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="registration_no", description="registration number of selected user", example="128973214", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(name="name", description="name of selected student", example="Student Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="nick_name", description="nick name of selected student", example="Stud", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="mother_name", description="mother name of selected student", example="Mother Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="mother_phone", description="mother phone of selected student", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="father_name", description="father name of selected student", example="Father Name", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="father_phone", description="father phone of selected student", example="081234567890", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(name="birthday", description="birthday of selected student", example="2022-02-02", required=true, in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *      @OA\Parameter(name="category", description="category of selected student", example="U-21", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(name="is_active", description="active status of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="img_url", description="image of selected student", example="filename.jpg", required=false, in="query",
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Student update successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Student not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatestudent(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $student = Student::where('id', $id)->with('User')->first();
            if (!$student) {
                return response()->json([
                    'Status' => false,
                    'message' => 'Student not found!'
                ], 404);
            }
            /* Used to create a copy of the student object. */
            $tempStudent = $student->replicate();

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'status' => 'required',
                'groups_id' => 'required',
                'registration_no' => 'required',
                'name' => 'required',
                'nick_name' => 'required',
                'mother_name' => 'required',
                'mother_phone' => 'required',
                'father_name' => 'required',
                'father_phone' => 'required',
                'birthday' => 'required',
                'category' => 'required',
                'is_active' => 'required',
                'img_url' => 'max:1240|mimes:jpeg,jpg,png,gif',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => $validator->errors(),
                    ],
                    400
                );
            }

            $username   = $request->username;
            $email      = $request->email;
            $status     = $request->status;
            $groups_id  = $request->groups_id;
            $registration_no = $request->registration_no;
            $name       = $request->name;
            $nick_name  = $request->nick_name;
            $mother_name = $request->mother_name;
            $mother_phone = $request->mother_phone;
            $father_name = $request->father_name;
            $father_phone = $request->father_phone;
            $birthday   = $request->birthday;
            $img_url    = $request->img_url;
            $joined_at  = $request->joined_at;
            $category   = $request->category;
            $is_active  = $request->is_active;
            $va_number  = $request->va_number;
            $created_at = $request->created_at;


            if (empty($request->file('img_url'))) {
                $path = $student->img_url;
                $imgname = $student->img_name;
            } else {
                Storage::delete('public/student/' . $student->img_name);

                $imgstudent = $request->file('img_url');
                $namaimgstudent = $imgstudent->getClientOriginalName();
                $loc = Storage::putFileAs(
                    'public/student',
                    $imgstudent,
                    $namaimgstudent
                );
                $imgname = $namaimgstudent;

                $path = config('app.url') . Storage::url($loc);
            }


            if ($username != $student->User->username) {
                $checkusername = User::where('username', $username)->first();
                if ($checkusername) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Username already exist!'
                    ], 400);
                }
            }

            if ($email != $student->User->email) {
                $checkemail = User::where('email', $email)->first();
                if ($checkemail) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Email already exist!'
                    ], 400);
                }
            }


            $student->update([
                'groups_id' => $groups_id,
                'registration_no' => $registration_no,
                'name' => $name,
                'nick_name' => $nick_name,
                'mother_name' => $mother_name,
                'mother_phone' => $mother_phone,
                'father_name' => $father_name,
                'father_phone' => $father_phone,
                'birthday' => $birthday,
                'img_url' => $path,
                'img_name' => $imgname,
                'joined_at' => $joined_at,
                'category' => $category,
                'is_active' => $is_active,
                'va_number' => $va_number,
            ]);

            $student->User->username = $username;
            $student->User->email = $email;
            $student->User->status = $status;
            $student->push();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student updated successfully',
                'data_old'          => $tempStudent,
                'data_new'          => $student,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $student,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student update failed',
                'data_old'          => $student,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to update the status of the student
     *
     * @param Request request The request object.
     *
     * @return a response in json format.
     */
    /**
     * @OA\Post(
     *      path="/admin/student/update/status",
     *      tags={"Admin/Student"},
     *      summary="Update status selected student",
     *      description="An endpoint that update status selected Student",
     *      operationId="UpdateStudentStatus",
     *      @OA\Parameter(name="id", description="id of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="is_active", description="active status of selected student", example="true", required=true, in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Student update status successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Student not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function updatestatusstudent(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $student = Student::where('id', $id)->with('User')->first();
            if (!$student) {
                return response()->json(['message' => 'Student not found!'], 404);
            }
            /* Used to create a copy of the student object. */
            $tempStudent = $student->replicate();

            $validator = Validator::make($request->all(), [
                'is_active' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $is_active = $request->is_active;

            $student->update([
                'is_active' => $is_active,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student updated successfully',
                'data_old'          => $tempStudent,
                'data_new'          => $student,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $student,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student update failed',
                'data_old'          => $student,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * It deletes a student from the database
     *
     * @param Request request The request object.
     *
     * @return The function deletestudent() is being returned.
     */
    /**
     * @OA\Post(
     *      path="/admin/student/delete",
     *      tags={"Admin/Student"},
     *      summary="Delete selected student",
     *      description="An endpoint that delete existing Student based on id",
     *      operationId="DeleteStudent",
     *      security={{"bearer_token":{}}},
     *      @OA\Parameter(name="id", description="id of selected student", example="1", required=true, in="query",
     *          @OA\Schema(
     *              type="int"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Student deleted successfully",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Something wrong",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Student not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function deletestudent(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $student = Student::where('id', $id)->with('User')->first();
            $username = $student->User->username;
            $user = User::where('username', $username)->first();
            if (!$student) {
                return response()->json(['message' => 'Student not found!'], 404);
            }
            /* Used to create a copy of the student object. */
            $tempStudent = $student->replicate();

            $student->delete();
            $user->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student deleted successfully',
                'data_old'          => $tempStudent,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Student delete failed',
                'data_old'          => $student,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
