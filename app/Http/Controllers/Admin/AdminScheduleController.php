<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Schedule;
use App\Models\ScheduleCategory;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminScheduleController extends Controller
{
    /**
     * It returns the schedule category.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Tag(
     *     name="Admin/ScheduleCategory",
     *     description="API Endpoints of Admin ScheduleCategories"
     * )
     *
     * @OA\Get(
     *     path="/coach/schedule/category/get",
     *     tags={"Admin/ScheduleCategory"},
     *     summary="Get schedule category for admin",
     *     description="Get schedule category for admin",
     *     operationId="getScheduleCategoryAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific schedule category",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule category found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Schedule category not found",
     *     ),
     * )
     */
    public function getschedulecategory(Request $request)
    {
        $id = $request->id;
        $limit = $request->limit;

        if ($id) {
            $schedulecategory = ScheduleCategory::where('id', $id)->first();

            if ($schedulecategory)
                return response()->json([
                    'Status' => true,
                    'Message' => 'Schedule category found',
                    'Data' => $schedulecategory,
                ]);
            else
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule category not found',
                ], 404);
        }
        $schedulecategory = new ScheduleCategory;

        if (!$schedulecategory->exists())
            return response()->json([
                'Status' => false,
                'Message' => 'Schedule category not found',
            ], 200);

        return response()->json([
            'Status' => true,
            'Message' => 'Schedule category found',
            'Data' => $schedulecategory->get(),
        ]);
    }

    /**
     * This function is used to create a schedule category.
     *
     * @param Request request The request object.
     *
     * @return A schedule category is being created.
     */
    /**
     * @OA\Post(
     *     path="/admin/schedule/category/create",
     *     tags={"Admin/ScheduleCategory"},
     *     summary="Create schedule category for admin",
     *     description="Create schedule category for admin",
     *     operationId="createScheduleCategoryAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule category create successfully",
     *     ),
     * )
     */
    public function createschedulecategory(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'color' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;
            $color = $request->color;

            $schedulecategory = ScheduleCategory::create([
                'name' => $name,
                'color' => $color,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'scheduleCategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category created successfully',
                'data_old'          => null,
                'data_new'          => $schedulecategory,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule category create successfully',
                'Data' => $schedulecategory,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'scheduleCategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * This function is used to update the schedule category
     *
     * @param Request request The request object.
     *
     * @return the schedule category.
     */
    /**
     * @OA\Post(
     *     path="/admin/schedule/category/update",
     *     tags={"Admin/ScheduleCategory"},
     *     summary="Update schedule category for admin",
     *     description="Update schedule category for admin",
     *     operationId="updateScheduleCategoryAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule category update successfully",
     *     ),
     * )
     */
    public function updateschedulecategory(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $schedulecategory = ScheduleCategory::where('id', $id)->first();
            if (!$schedulecategory) {
                return response()->json([
                    'message' => 'Schedule category not found'
                ], 404);
            }
            /* Used to create a copy of the schedulecategory object. */
            $tempSchedulecategory = $schedulecategory->replicate();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'color' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $name = $request->name;
            $color = $request->color;

            $schedulecategory->update([
                'name' => $name,
                'color' => $color,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'scheduleCategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category updated successfully',
                'data_old'          => $tempSchedulecategory,
                'data_new'          => $schedulecategory,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $schedulecategory,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'scheduleCategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category update failed',
                'data_old'          => $schedulecategory,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to delete a schedule category.
     *
     * @param Request request The request object.
     *
     * @return The function deleteschedulecategory is being returned.
     */
    /**
     * @OA\Post(
     *     path="/admin/schedule/category/delete",
     *     tags={"Admin/ScheduleCategory"},
     *     summary="Delete schedule category for admin",
     *     description="Delete schedule category for admin",
     *     operationId="deleteScheduleCategoryAdmin",
     *     security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="For get id to delete",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule category delete successfully",
     *     ),
     * )
     */
    public function deleteschedulecategory(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $schedulecategory = ScheduleCategory::where('id', $id)->first();
            if (!$schedulecategory) {
                return response()->json([
                    'message' => 'Schedule category not found'
                ], 404);
            }
            /* Used to create a copy of the schedulecategory object. */
            $tempSchedulecategory = $schedulecategory->replicate();

            $cekschedulecategory = Schedule::where('schedule_categories_id', $id);

            if ($cekschedulecategory->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule category cannot be deleted',
                ], 400);
            }

            $schedulecategory->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'scheduleCategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category deleted successfully',
                'data_old'          => $tempSchedulecategory,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'schedulCcategory',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule category delete failed',
                'data_old'          => $schedulecategory,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * A function to get the schedule data.
     *
     * @param Request request The request object.
     *
     * @return The schedule is being returned.
     */
    /**
     * @OA\Tag(
     *     name="Admin/Schedule",
     *     description="API Endpoints of Admin Schedules"
     * )
     *
     * @OA\Get(
     *     path="/admin/schedule/get",
     *     tags={"Admin/Schedule"},
     *     summary="Get schedule for admin",
     *     description="Get schedule for admin",
     *     operationId="getScheduleAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For find specific schedule",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="start_date",
     *          description="For filter by start date",
     *          example="2022-02-01",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="end_date",
     *          description="For filter by end_date",
     *          example="2022-02-07",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="closest_date",
     *          description="For filter next 5 days",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="groups",
     *          description="For filter by groups",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Schedule not found",
     *     ),
     * )
     */

    public function getschedule(Request $request)
    {
        try {
            $id = $request->id;
            $groups_id = $request->groups_id;
            $schedule_categories_id = $request->schedule_categories_id;
            $month = $request->month;
            $year = $request->year;
            $day = $request->day;


            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $closest_date = $request->closest_date;
            $now_date = date('Y-m-d', time());
            $sub_date = date('Y-m-d', strtotime($now_date . ' +5 day'));

            $schedule = Schedule::with('Group', 'Schedule_category');



            if ($id) {
                $schedule = $schedule->where('id', $id);
            }

            // if ($schedule_categories_id) {
            //     $schedule = $schedule->where('schedule_categories_id', $schedule_categories_id);
            // }

            if ($start_date && $end_date) {
                $schedule = $schedule->where('date', '>=', $start_date)->where('date', '<=', $end_date);
            }

            if ($closest_date) {
                $schedule = $schedule->where('date', '<=', $sub_date)->where('date', '>=', $now_date);
            }

            if ($groups_id) {
                $schedule = $schedule->where('groups_id', $groups_id);
            }

            if ($month) {
                $schedule = $schedule->whereMonth('date', $month);
            }

            if ($year) {
                $schedule = $schedule->whereYear('date', $year);
            }

            if ($day) {
                $schedule = $schedule->whereDay('date', $day);
            }

            $limit = $schedule->count();

            if (!$schedule->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule found',
                'Data' => $schedule->orderBy('date', 'ASC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * It returns the schedule after 7 days
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Get(
     *     path="/admin/schedule/get/7days",
     *     tags={"Admin/Schedule"},
     *     summary="Get schedule after 7days for admin",
     *     description="Get schedule after 7days for admin",
     *     operationId="getSchedule7daysAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For find specific schedule",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="groups",
     *          description="For filter by groups",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Schedule not found",
     *     ),
     * )
     */
    public function getscheduleafter7days(Request $request)
    {
        try {
            $id = $request->id;
            $limit = $request->limit;
            $groups_id = $request->groups_id;
            $schedule_categories_id = $request->schedule_categories_id;
            $month = $request->month;
            $year = $request->year;


            $now_date = date('Y-m-d', time());
            $sub_date = date('Y-m-d', strtotime($now_date . ' -7 day'));

            $schedule = Schedule::with('Group', 'Schedule_category')->where('date', '>=', $sub_date)->where('date', '<=', $now_date);

            if ($id) {
                $schedule = $schedule->where('id', $id)->whereMonth('date', $month)->whereYear('date', $year);
            }

            if ($groups_id) {
                $schedule = $schedule->where('groups_id', $groups_id);
            }

            if ($schedule_categories_id) {
                $schedule = $schedule->where('schedule_categories_id', $schedule_categories_id);
            }

            if (!$schedule->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule not found',
                ], 404);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule found',
                'Data' => $schedule->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The above function is used to create a schedule.
     *
     * @param Request request The request object.
     *
     * @return A schedule is being created.
     */
    /**
     * @OA\Post(
     *     path="/admin/schedule/create",
     *     tags={"Admin/Schedule"},
     *     summary="Create schedule for admin",
     *     description="Create schedule for admin",
     *     operationId="createScheduleAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule create successfully",
     *     ),
     * )
     */
    public function createschedule(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'groups_id' => 'required',
                'title' => 'required',
                'desc' => 'required',
                'date' => 'required',
                'time' => 'required',
                'location' => 'required',
                'coordinate' => 'required',
                'schedule_categories_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $groups_id = $request->groups_id;
            $coaches_id = $request->coaches_id;
            $title = $request->title;
            $desc = $request->desc;
            $date = $request->date;
            $time = $request->time;
            $location = $request->location;
            $coordinate = $request->coordinate;
            $schedule_categories_id = $request->schedule_categories_id;

            $checkgroup = Group::where('id', $groups_id)->first();

            if (!$checkgroup) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Group not found',
                ]);
            }

            $checkschedulecategory = ScheduleCategory::where('id', $schedule_categories_id)->first();

            if (!$checkschedulecategory) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule category not found',
                ]);
            }

            $schedule = Schedule::create([
                'groups_id' => $groups_id,
                'coaches_id' => $coaches_id,
                'title' => $title,
                'desc' => $desc,
                'date' => $date,
                'time' => $time,
                'location' => $location,
                'coordinate' => $coordinate,
                'schedule_categories_id' => $schedule_categories_id,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'schedule',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule created successfully',
                'data_old'          => null,
                'data_new'          => $schedule,
            ]);

            $schedule = Schedule::latest()->first();

            $timestamp = strtotime($schedule->created_at);

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule create successfully',
                'Data' => $schedule,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'schedule',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to update the schedule.
     *
     * @param Request request The request object.
     *
     * @return a response in JSON format.
     */
    /**
     * @OA\Post(
     *     path="/admin/schedule/update",
     *     tags={"Admin/Schedule"},
     *     summary="Update schedule or admin",
     *     description="Update schedule for admin",
     *     operationId="updateScheduleAdmin",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule update successfully",
     *     ),
     * )
     */
    public function updateschedule(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $schedule = Schedule::where('id', $id)->first();
            if (!$schedule) {
                return response()->json([
                    'Message' => 'Schedule not found'
                ], 404);
            }
            /* Used to create a copy of the schedule object. */
            $tempSchedule = $schedule->replicate();

            $validator = Validator::make($request->all(), [
                'groups_id' => 'required',
                'title' => 'required',
                'desc' => 'required',
                'date' => 'required',
                'time' => 'required',
                'location' => 'required',
                'coordinate' => 'required',
                'schedule_categories_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $groups_id = $request->groups_id;
            $title = $request->title;
            $desc = $request->desc;
            $date = $request->date;
            $time = $request->time;
            $location = $request->location;
            $coordinate = $request->coordinate;
            $schedule_categories_id = $request->schedule_categories_id;

            $checkgroup = Group::where('id', $groups_id)->first();

            if (!$checkgroup) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Group not found',
                ]);
            }

            $checkschedulecategory = ScheduleCategory::where('id', $schedule_categories_id)->first();

            if (!$checkschedulecategory) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule category not found',
                ]);
            }

            $schedule->update([
                'groups_id' => $groups_id,
                'title' => $title,
                'desc' => $desc,
                'date' => $date,
                'time' => $time,
                'location' => $location,
                'coordinate' => $coordinate,
                'schedule_categories_id' => $schedule_categories_id,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'schedule',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule deleted successfully',
                'data_old'          => $tempSchedule,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $schedule,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'schedule',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Schedule delete failed',
                'data_old'          => $schedule,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
