<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;


class DatabaseController extends Controller
{
    public function migratefresh()
    {
        /* php artisan migrate */

        Artisan::call('migrate:fresh');


        dd('all migration fresh run successfully');
    }

    public function migrate()
    {

        Artisan::call('migrate');


        dd('all migration run successfully');
    }

    public function seeder()
    {
        /* php artisan migrate */
        Artisan::call('db:seed', array('--class' => "DatabaseSeeder"));

        dd('Seeder run successfully');
    }

    public function config()
    {
        Artisan::call('config:clear');

        dd('config clear run successfully');
    }

    public function cache()
    {
        Artisan::call('cache:clear');

        dd('cache clear run successfully');
    }

    public function route()
    {
        Artisan::call('route:clear');

        dd('route clear run successfully');
    }

    public function clearall()
    {
        Artisan::call('route:clear');
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        dd('all clear run successfully');
    }

    public function createlink()
    {
        Artisan::call('storage:link');

        dd('storage link run successfully');
    }

    public function composerdump()
    {
        Artisan::call('composer:dump-autoload');

        dd('composer dump autoload run successfully');
    }
}
