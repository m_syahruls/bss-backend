<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use App\Models\Student;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentScheduleController extends Controller
{


    /**
     * The below function is used to get schedule for student.
     *
     * @param Request request The request object.
     *
     * @return schedule is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/schedule/get",
     *     tags={"Student Schedule"},
     *     summary="Get schedule for student",
     *     description="Get all schedule for student",
     *     operationId="getScheduleStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="start_date",
     *          description="For filter by start date",
     *          example="2022-02-01",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="end_date",
     *          description="For filter by end_date",
     *          example="2022-02-07",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="closest_date",
     *          description="For filter next 5 days",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Schedule not found",
     *     ),
     * )
     */
    public function getschedule(Request $request)
    {
        try {
            $limit = $request->limit;

            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $closest_date = $request->closest_date;
            $now_date = date('Y-m-d', time());
            $sub_date = date('Y-m-d', strtotime($now_date . ' +5 day'));

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $student = Student::with('Groups')->where('id', $user->Student->id)->first();
            $schedule = Schedule::with('Group')->with('Schedule_category')->where('groups_id', $student->Groups->id);

            if ($start_date && $end_date) {
                $schedule = $schedule->where('date', '>=', $start_date)->where('date', '<=', $end_date);
            }

            if ($closest_date) {
                $schedule = $schedule->where('date', '<=', $sub_date)->where('date', '>=', $now_date);
            }

            if (!$limit) {
                $limit = $schedule->count();
            }

            if (!$schedule->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule List not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule found',
                'Data' => $schedule->orderBy('date', 'ASC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
