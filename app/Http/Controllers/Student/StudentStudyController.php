<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Study;
use App\Models\User;
use App\Models\UserActivity;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentStudyController extends Controller
{


    /**
     * The below function is used to get study for student.
     *
     * @param Request request The request object.
     *
     * @return study is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/study/get",
     *     tags={"Student Study"},
     *     summary="Get study for student",
     *     description="Get all study material for student",
     *     operationId="getStudyStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="is_pdf",
     *          description="To choose whether pdf or not",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Study found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Study not found",
     *     ),
     * )
     */
    public function getstudy(Request $request)
    {
        try {

            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $is_pdf = $request->is_pdf;

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $student = Student::with('Groups')->where('id', $user->Student->id)->first();
            $study = Study::with('Coach')->with('Admin')->where('groups_id', $student->Groups->id);

            if ($is_pdf) {
                $study = $study->where('is_pdf', 1);
            } else {
                $study = $study->where('is_pdf', 0);
            }

            if ($month) {
                $study = $study->whereMonth('created_at', $month);
            }

            if ($year) {
                $study = $study->whereYear('created_at', $year);
            }

            if (!$study->exists()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Study not found'
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Study found',
                'Data' => $study->orderBy('created_at', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    public function pinaddstudy(Request $request)
    {
        try {
            $user_auth = User::with('Student')->where('id', Auth::id())->first();

            $id = $request->id;
            $is_pin = $request->is_pin;

            $study = Study::where('id', $id)->first();
            if (!$study) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 404);
            }
            /* Used to create a copy of the study object. */
            $tempStudy = $study->replicate();

            if ($study->is_pin == 0) {
                $cekstudypin = Study::where('is_pin', 1)->get();
                if (count($cekstudypin) == 6) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Cant create pin'
                    ], 404);
                }

                $study->update([
                    'is_pin' => 1,
                    'is_pin_time' => Carbon::now(),
                ]);
            } elseif ($study->is_pin  == 1) {
                $study->update([
                    'is_pin' => 0,
                    'is_pin_time' => null,
                ]);
            }

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Pin study added successfully',
                'data_old'          => $tempStudy,
                'data_new'          => $study,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Success',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'study',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Pin study add failed',
                'data_old'          => $study,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    public function pingetstudy(Request $request)
    {
        try {
            $is_pdf = $request->is_pdf;
            $study = Study::with('Admin:id,name,img_url', 'Coach:id,name,img_url', 'Group')->where('is_pin', 1);

            if ($is_pdf) {
                $study = $study->where('is_pdf', 1);
            } else {
                $study = $study->where('is_pdf', 0);
            }

            if (!$study->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Get pin successfully',
                'Data' => $study->orderBy('is_pin_time', 'desc')->get(),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
