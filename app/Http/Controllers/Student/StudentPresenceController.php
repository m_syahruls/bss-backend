<?php

namespace App\Http\Controllers\Student;

use App\Models\User;
use App\Models\Student;
use App\Models\Presence;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentPresenceController extends Controller
{
    /**
     * The below function is used to get presence for student.
     *
     * @param Request request The request object.
     *
     * @return presence is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/presence/get",
     *     tags={"Student Presence"},
     *     summary="Get presence for student",
     *     description="Get all presence for student",
     *     operationId="getPresenceStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Presence found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Presence not found",
     *     ),
     * )
     */
    public function getpresence(Request $request)
    {
        try {

            $limit = $request->limit;
            $datenow = date('Y-m-d', time());

            $user = User::with('Student')->where('id', Auth::user()->id)->first();

            $student = Student::with('Groups')->where('id', $user->student->id)->first();
            $presence = Schedule::with(['Presences' => function ($query) use ($user) {
                $query->where('students_id', $user->Student->id);
            }])->where('groups_id', $student->Groups->id)->where('date', '<=', $datenow);

            if (!$presence->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Presence List not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Presence found',
                'Data' => $presence->orderBy('date', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
