<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Jobs\SendWhatsappJob;
use App\Models\Reward;
use App\Models\RewardReceiver;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentRewardController extends Controller
{


    /**
     * The below function is used to get reward for student.
     *
     * @param Request request The request object.
     *
     * @return reward is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/reward/get",
     *     tags={"Student Reward"},
     *     summary="Get reward for student",
     *     description="Get all reward for student",
     *     operationId="getRewardStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="status",
     *          description="For filter by status",
     *          example="2022-02-01",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Reward found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Reward not found",
     *     ),
     * )
     */
    public function getreward(Request $request)
    {
        try {
            $status = $request->status;
            $limit = $request->limit;
            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $reward = RewardReceiver::with('Reward', 'Student')->where('students_id', $user->Student->id);

            if ($status) {
                $reward = $reward->where('status', $status);
            }

            if (!$reward->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward not found',
                ], 200);
            }
            return response()->json([
                'Status' => true,
                'Message' => 'Reward get successfully',
                'Data' => $reward->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to claim reward for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/reward/claim",
     *     tags={"Student Reward"},
     *     summary="Claim reward for student",
     *     description="Claim reward for student",
     *     operationId="ClaimRewardStudent",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For select reward by id",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Reward claimed successfully",
     *     ),
     * )
     */
    public function claimreward(Request $request)
    {
        try {
            $user_auth = User::with('Student')->where('id', Auth::id())->first();

            $id = $request->id;
            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $reward = RewardReceiver::with('Reward')->where('id', $id)->where('students_id', $user->Student->id)->first();
            if ($reward->status == 'WAITING_APPROVAL') {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Reward already claimed',
                ], 404);
            }
            /* Used to create a copy of the reward object. */
            $tempReward = $reward->replicate();

            $reward->update([
                'status' => 'WAITING_APPROVAL',
                'date_claimed' => now(),
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Reward claimed successfully',
                'data_old'          => $tempReward,
                'data_new'          => $reward,
            ]);

            $whatsappdata = [
                'target' => '082155198171',
                //'type' => 'text',
                'message' =>  $user->username .  ' is requesting reward ' . $reward->Reward->title . ' at ' . $reward->date_claimed . '.',
                'delay' => '0',
                'schedule' => '0'
            ];
            $jobwa = new SendWhatsappJob($whatsappdata);
            dispatch($jobwa);

            return response()->json([
                'Status' => true,
                'Message' => 'Reward claimed successfully',
                'Data' => $reward,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'rewardReceiver',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Reward claim failed',
                'data_old'          => $reward,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
