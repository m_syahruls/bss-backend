<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\FitnesList;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StudentFitnesListController extends Controller
{


    /**
     * The below function is used to get fitneslist for student.
     *
     * @param Request request The request object.
     *
     * @return fitneslist is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/fitneslist/get",
     *     tags={"Student Fitneslist"},
     *     summary="Get fitneslist for student",
     *     description="Get all fitneslist for student",
     *     operationId="getFitneslistStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Fitneslist found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Fitneslist not found",
     *     ),
     * )
     */
    public function getfitneslist(Request $request)
    {
        try {
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $fitneslist = FitnesList::where('students_id', $user->Student->id);


            if ($month) {
                $fitneslist = $fitneslist->whereMonth('created_at', $month);
            }

            if ($year) {
                $fitneslist = $fitneslist->whereYear('created_at', $year);
            }

            if (!$fitneslist->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Fitness List not found',
                ], 200);
            }


            return response()->json([
                'Status' => true,
                'Message' => 'Fitness List found',
                'Data' => $fitneslist->orderBy('is_fit', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to create fitneslist for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/fitneslist/create",
     *     tags={"Student Fitneslist"},
     *     summary="Create fitneslist for student",
     *     description="Create all fitneslist for student",
     *     operationId="CreateFitneslistStudent",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"students_id","point","answer" ,"is_fit"},
     *              @OA\Property(property="students_id", type="bigint", example="1"),
     *              @OA\Property(property="point", type="int",  example="10"),
     *              @OA\Property(property="answer", type="string", example="1,5,7"),
     *              @OA\Property(property="is_fit", type="tinyint", example="1"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Fitneslist create successfully",
     *     ),
     * )
     */
    public function createfitneslist(Request $request)
    {
        try {
            $user_auth = User::with('Student')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'point' => 'required',
                'answer' => 'required',
                'is_fit' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => false,
                        $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = User::with('Student')->where('id', Auth::id())->first();
            $point = $request->point;
            $answer = $request->answer;
            $is_fit = $request->is_fit;

            //check if the student has filled in the form today
            $cekstudent = FitnesList::where('students_id', $students_id->Student->id)->whereDay('created_at', date('d'))->whereMonth('created_at', date('m'))->first();

            if ($cekstudent) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'You already filled the form today',
                ], 400);
            }


            $fitneslist = FitnesList::create([
                'students_id' => $students_id->Student->id,
                'point' => $point,
                'answer' => $answer,
                'is_fit' => $is_fit,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'fitnesList',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Fitnes list created successfully',
                'data_old'          => null,
                'data_new'          => $fitneslist,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Fitnes list create successfully',
                'Data' => $fitneslist,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'fitnesList',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Fitnes list create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
