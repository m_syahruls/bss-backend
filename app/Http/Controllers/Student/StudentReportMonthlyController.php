<?php

namespace App\Http\Controllers\Student;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ReportMonthlys;
use App\Http\Controllers\Controller;
use App\Models\ReportMonthlyRolePositions;
use Illuminate\Support\Facades\Auth;

class StudentReportMonthlyController extends Controller
{


    /**
     * The below function is used to get monthly report for student.
     *
     * @param Request request The request object.
     *
     * @return monthly report is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/report/monthly/get",
     *     tags={"Student Report Monthly"},
     *     summary="Get report monthly for student",
     *     description="Get all report monthly for student",
     *     operationId="getReportMonthlyStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Report Monthly found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Report Monthly not found",
     *     ),
     * )
     */
    public function getreportmonthly(Request $request)
    {
        try {

            $id = $request->id;
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $reportmonthly = ReportMonthlys::with('Coach:id,name', 'Admin', 'Student')->with(['Report_monthly_role_positions' => function ($query) {
                $query->with('Role_position');
            }])->where('students_id', $user->Student->id);


            if ($id) {
                $reportmonthly = ReportMonthlys::with('Coach:id,name', 'Admin', 'Student')->with(['Report_monthly_role_positions' => function ($query) {
                    $query->with('Role_position',);
                }])->with('Report_monthly_skills')->where('id', $id)->where('students_id', $user->Student->id);
            }

            if ($month) {
                $reportmonthly = $reportmonthly->whereMonth('created_at', $month);
            }

            if ($year) {
                $reportmonthly = $reportmonthly->whereYear('created_at', $year);
            }

            if (!$reportmonthly->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Monthly not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Report monthly found',
                'Data' => $reportmonthly->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
