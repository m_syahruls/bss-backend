<?php

namespace App\Http\Controllers\Student;

use App\Models\User;
use App\Models\ReportExam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentExamReportController extends Controller
{


    /**
     * The below function is used to get exam report for student.
     *
     * @param Request request The request object.
     *
     * @return exam report is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/report/exam/get",
     *     tags={"Student Report Exam"},
     *     summary="Get report Exam for student",
     *     description="Get all report Exam for student",
     *     security={{"bearer_token":{}}},
     *     operationId="getReportExamStudent",
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report exam",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Report Exam found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Report Exam not found",
     *     ),
     * )
     */
    public function getreportexam(Request $request)
    {
        try {
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $id = $request->id;

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $reportexam = ReportExam::with('Coach:id,name', 'Admin', 'Student')->with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }])->where('students_id', $user->Student->id);

            if ($id) {
                $reportexam = $reportexam->where('id', $id);
            }

            if ($month) {
                $reportexam = $reportexam->whereMonth('date', $month);
            }

            if ($year) {
                $reportexam = $reportexam->whereYear('date', $year);
            }

            if (!$reportexam->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam not found',
                ], 200);
            }
            return response()->json([
                'Status' => true,
                'Message' => 'Report exam found',
                'Data' => $reportexam->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
