<?php

namespace App\Http\Controllers\Student;

use App\Models\User;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentPaymentController extends Controller
{


    /**
     * The below function is used to get payment for student.
     *
     * @param Request request The request object.
     *
     * @return payment is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/payment/get",
     *     tags={"Student Payment"},
     *     summary="Get payment for student",
     *     description="Get all payment for student",
     *     operationId="getPaymentStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Payment found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Payment not found",
     *     ),
     * )
     */
    public function getpayment(Request $request)
    {
        try {

            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;

            $student = User::with('Student')->where('id', Auth::user()->id)->first();
            $payment = Payment::with('Student:id,va_number')->where('students_id', $student->Student->id);


            if ($month) {
                $payment = $payment->where('month', $month);
            }

            if ($year) {
                $payment = $payment->where('year', $year);
            }

            if (!$payment->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Payment not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Payment found',
                'Data' => $payment->orderBy('year', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
