<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Mail\Bssmail;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Kreait\Firebase\Messaging\CloudMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{

    /**
     * The below function is used to login for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/loginstudent",
     *     tags={"Student Auth"},
     *     summary="Login student",
     *     description="Login student",
     *     operationId="LoginStudent",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username","password"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *              @OA\Property(property="password", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Login successfully",
     *     ),
     * )
     */
    public function loginstudent(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => false,
                        $validator->errors(),
                    ],
                    400
                );
            }


            $credentials = request(['username', 'password']);

            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username or password is wrong',
                ]);
            }

            $usercek = User::where('username', $request->username)->first();

            $cekstudent = Student::where('users_id', $usercek->id)->first();

            if (!$cekstudent) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Youre not student',
                    ]
                );
            }

            $userstudent = User::where('username', $request->username)->first();
            if (!Hash::check($request->password, $userstudent->password, [])) {
                throw new \Exception('Invalid Credentials');
            }

            $data = User::where('id', $userstudent->id)->with(['Student' => function ($query) {
                $query->with('Groups');
            }])->first();

            $token = $userstudent->createToken('authToken')->plainTextToken;


            return response()->json([
                'Status' => true,
                'Message' => 'Login successfully',
                'access_token' => $token,
                'token_type' => 'Bearer',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Cant access',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to logout for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/logoutstudent",
     *     tags={"Student Auth"},
     *     summary="Logout student",
     *     description="Logout student",
     *     operationId="LogoutStudent",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Logout successfully",
     *     ),
     * )
     */
    public function logoutstudent(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();
        return response()->json([
            'Status' => true,
            'Message' => 'Token deleted',
        ]);
    }


    /**
     * The below function is used to get profile student.
     *
     * @param Request request The request object.
     *
     * @return profile student is being returned.
     */
    /**
     * @OA\Get(
     *     path="/student/profile/get",
     *     tags={"Student Auth"},
     *     summary="Get Profile for student",
     *     description="Get profile for student",
     *     operationId="getProfileStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Profile student found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Profile student not found",
     *     ),
     * )
     */
    public function profilestudent(Request $request)
    {
        try {
            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $student = Student::with('User')->with('Groups')->where('id', $user->Student->id)->first();
            return response()->json([
                'Status' => true,
                'Message' => 'Profile Student',
                'Data' => $student,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to set new password when first login for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/update/password",
     *     tags={"Student Auth"},
     *     summary="Update password student",
     *     description="Update password student",
     *     operationId="UpdatePasswordStudent",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"newpassword"},
     *              @OA\Property(property="newpassword", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Update successfully",
     *     ),
     * )
     */
    public function newpassword(Request $request)
    {
        try {
            $user_auth = User::with('Student')->where('id', Auth::id())->first();

            $user = User::with('Student')->where('id', Auth::user()->id)->first();
            $student = Student::with('User')->where('id', $user->student->id)->first();
            /* Used to create a copy of the student object. */
            $tempStudent = $student->replicate();

            $newpassword = $request->newpassword;
            $hashpass = Hash::make($newpassword);

            if ($user->has_changed_password == 1) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'You have already changed your password',
                ]);
            }

            $validator = Validator::make($request->all(), [
                'newpassword' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $user->update([
                'password' => $hashpass,
                'has_changed_password' => 1,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Student updated successfully',
                'data_old'          => $tempStudent,
                'data_new'          => $student,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $student,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'student',
                'model_activity'    => 'student',
                'causer'            => $user_auth->Student->name,
                'log'               => 'Student update failed',
                'data_old'          => $student,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used reset password for student.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/student/resetpass",
     *     tags={"Student Auth"},
     *     summary="Reset password student",
     *     description="Reset password student",
     *     operationId="ResetPasswordStudent",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Reset successfully",
     *     ),
     * )
     */
    public function sendemailresetpassword(Request $request)
    {
        try {
            $username = $request->username;

            $user = User::with('Student')->where('username', $username)->first();

            $email = $user->email;

            if (!$user) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username not found',
                ], 400);
            }

            $resetpassword = [
                'parent' => $user->Student->father_name,
                'namestudent' => $user->Student->name,
                'email' => $email,
                'username' => $username,
                'role' => 'student',
            ];

            Mail::to($email)->send(new Bssmail($resetpassword));

            return response()->json([
                'Status' => true,
                'Message' => 'Email sent successfully',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
