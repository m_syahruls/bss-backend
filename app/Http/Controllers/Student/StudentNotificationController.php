<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationReceiver;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentNotificationController extends Controller
{
    /**
     * It gets the notification list of the currently logged in user
     *
     * @param Request request The request object.
     *
     * @return The notification list of the currently logged in user.
     */
    /**
     * @OA\Get(
     *     path="/student/notification/get",
     *     tags={"Student Notification"},
     *     summary="Get notification for student",
     *     description="Get all notification for student",
     *     operationId="getNotificationStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Notification found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Notification not found",
     *     ),
     * )
     */
    public function getNotification(Request $request)
    {
        /* Getting the user details of the currently logged in user. */
        $user_auth = User::with('Student')->where('id', Auth::id())->first();

        $user = $user_auth->Student->users_id;
        $limit = $request->limit;
        $notifications = Notification::whereHas('receivers', function ($query) use ($user) {
            $query->where('user_id', $user);
        })->orderBy('created_at', 'DESC')->paginate($limit);

        if (count($notifications) == 0) {
            return response()->json([
                'Status' => false,
                'Message' => 'Notification list not found',
                'Data'      => [],
            ], 200);
        }

        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        $receivers = NotificationReceiver::where('user_id', $user)->where('read_at', null);
        /* Used to create a copy of the receivers object. */
        $tempReceivers = clone $receivers->get();

        if (count($tempReceivers)) {
            try {
                /* Updating the `read_at` column of the `notification_receivers` table. */
                $receivers = $receivers->update([
                    'read_at' => $now,
                ]);

                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'student',
                    'model_activity'    => 'notificationReceiver',
                    'causer'            => $user_auth->Student->name,
                    'log'               => 'Notification read successfully',
                    'data_old'          => $tempReceivers,
                    'data_new'          => json_encode(['read_at' => $now]),
                ]);
            } catch (\Exception $error) {
                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'student',
                    'model_activity'    => 'notificationReceiver',
                    'causer'            => $user_auth->Student->name,
                    'log'               => 'Notification read failed',
                    'data_old'          => $receivers,
                    'data_new'          => $error,
                ]);
            }
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Notification list found',
            'Data' => $notifications,
        ]);
    }

    /**
     * It returns the number of unread notifications for the currently logged in user
     *
     * @return The number of unread notifications.
     */
    /**
     * @OA\Get(
     *     path="/student/notification/unread",
     *     tags={"Student Notification"},
     *     summary="Get notification unread for student",
     *     description="Get all notification unread for student",
     *     operationId="getNotificationUnreadStudent",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Notification unread found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Notification unread not found",
     *     ),
     * )
     */
    public function getUnreadNotification()
    {
        /* Getting the user details of the currently logged in user. */
        $user_auth = User::with('Student')->where('id', Auth::id())->first();

        $user = $user_auth->Student->users_id;
        $notifications = Notification::whereHas('receivers', function ($query) use ($user) {
            $query->where('user_id', $user)->where('read_at', null);
        })->count();

        return response()->json([
            'Status' => true,
            'Message' => 'Unread notification found',
            'Data' => $notifications,
        ]);
    }
}
