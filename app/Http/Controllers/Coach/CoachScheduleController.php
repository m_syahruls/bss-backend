<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\CoachGroup;
use App\Models\Group;
use App\Models\Schedule;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachScheduleController extends Controller
{

    /**
     * The below function is used to get schedule for coach.
     *
     * @param Request request The request object.
     *
     * @return schedule is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/schedule/get",
     *     tags={"Coach Schedule"},
     *     summary="Get schedule for coach",
     *     description="Get all schedule for coach",
     *     operationId="getScheduleCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For find specific schedule",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="start_date",
     *          description="For filter by start date",
     *          example="2022-02-01",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="end_date",
     *          description="For filter by end_date",
     *          example="2022-02-07",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="closest_date",
     *          description="For filter next 5 days",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Schedule found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Schedule not found",
     *     ),
     * )
     */
    public function getschedule(Request $request)
    {
        try {
            $id = $request->id;
            $groups_id = $request->groups_id;
            $month = $request->month;
            $year = $request->year;
            $day = $request->day;
            $limit = $request->limit;

            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $closest_date = $request->closest_date;
            $now_date = date('Y-m-d', time());
            $sub_date = date('Y-m-d', strtotime($now_date . ' +5 day'));

            $user = User::with('Coach')->where('id', Auth::user()->id)->first();
            $group = CoachGroup::where('coaches_id', $user->Coach->id);
            $schedule = Schedule::with('Group')->with('Schedule_category')->whereIn('groups_id', $group->pluck('groups_id'));

            if ($id) {
                $schedule = $schedule->where('id', $id);
            }

            if ($start_date && $end_date) {
                $schedule = $schedule->where('date', '>=', $start_date)->where('date', '<=', $end_date);
            }

            if ($closest_date) {
                $schedule = $schedule->where('date', '<=', $sub_date)->where('date', '>=', $now_date);
            }

            if ($groups_id) {
                $schedule = $schedule->where('groups_id', $groups_id);
            }

            if ($month) {
                $schedule = $schedule->whereMonth('date', $month);
            }

            if ($year) {
                $schedule = $schedule->whereYear('date', $year);
            }

            if ($day) {
                $schedule = $schedule->whereDay('date', $day);
            }

            if (!$limit) {
                $limit = $schedule->count();
            }

            if (!$schedule->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Schedule List not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Schedule found',
                'Data' => $schedule->orderBy('date', 'ASC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
