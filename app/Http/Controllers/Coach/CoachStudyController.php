<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\CoachGroup;
use App\Models\Group;
use App\Models\Study;
use App\Models\User;
use App\Models\UserActivity;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CoachStudyController extends Controller
{

    /**
     * The below function is used to get study for coach.
     *
     * @param Request request The request object.
     *
     * @return study is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/study/get",
     *     tags={"Coach Study"},
     *     summary="Get study for coach",
     *     description="Get all study material for coach",
     *     operationId="getStudyCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="2",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="is_pdf",
     *          description="To choose whether pdf or not",
     *          example="1",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Study found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Study not found",
     *     ),
     * )
     */
    public function getstudy(Request $request)
    {
        try {
            $groups_id = $request->groups_id;
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $is_pdf = $request->is_pdf;

            $coaches_id = User::with('Coach')->where('id', Auth::id())->first();
            $group = CoachGroup::where('coaches_id', $coaches_id->Coach->id);
            $study = Study::with('Coach')->with('Admin')->whereIn('groups_id', $group->pluck('groups_id'));

            if ($groups_id) {
                $study = $study->where('groups_id', $groups_id);
            }

            if ($month) {
                $study = $study->whereMonth('created_at', $month);
            }

            if ($year) {
                $study = $study->whereYear('created_at', $year);
            }

            if (!$study->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Study found',
                'Data' => $study->orderBy('created_at', 'DESC')->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    public function pingetstudy(Request $request)
    {
        try {
            $is_pdf = $request->is_pdf;
            $study = Study::with('Admin:id,name,img_url', 'Coach:id,name,img_url', 'Group')->where('is_pin', 1);

            if (!$study->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Study not found'
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Get pin successfully',
                'Data' => $study->orderBy('is_pin_time', 'desc')->get(),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
