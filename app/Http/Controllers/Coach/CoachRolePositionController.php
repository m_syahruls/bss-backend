<?php

namespace App\Http\Controllers\Coach;

use App\Models\RolePosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoachRolePositionController extends Controller
{

    /**
     * The below function is used to get schedule for coach.
     *
     * @param Request request The request object.
     *
     * @return schedule is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/roleposition/get",
     *     tags={"Coach Role Position"},
     *     summary="Get role position for coach",
     *     description="Get all role posititon for coach",
     *     operationId="getRolePositionCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For find specific role position",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="name",
     *          description="For filter by name",
     *          example="andi",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Role position found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Role position not found",
     *     ),
     * )
     */
    public function getroleposition(Request $request)
    {
        try {

            $id = $request->id;
            $limit = $request->limit;
            $name = $request->name;

            $roleposition = new RolePosition;

            if ($id) {
                $roleposition = $roleposition->where('id', $id);
            }

            if ($name) {
                $roleposition = $roleposition->where('name', 'like', '%' . $name . '%');
            }

            if (!$roleposition->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Role Position not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Role position found',
                'Data' => $roleposition->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
