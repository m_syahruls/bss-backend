<?php

namespace App\Http\Controllers\Coach;

use App\Models\User;
use App\Models\Student;
use App\Models\FitnesList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoachFitnesListController extends Controller
{

    /**
     * The below function is used to get fitneslist for coach.
     *
     * @param Request request The request object.
     *
     * @return fitneslist being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/fitneslist/get",
     *     tags={"Coach Fitneslist"},
     *     summary="Get fitneslist for coach",
     *     description="Get all fitneslist for coach",
     *     operationId="getFitnesListCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific fitneslist",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="10",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="groups_id",
     *          description="For filter by group",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *      @OA\Parameter(
     *          name="day",
     *          description="For filter by day",
     *          example="3",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Fitneslist found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Fitneslist  not found",
     *     ),
     * )
     */
    public function getfitneslist(Request $request)
    {
        try {

            $month = $request->month;
            $year = $request->year;
            $day = $request->day;

            $groups_id = $request->groups_id;

            $limit = $request->limit;

            $fitneslist =  new Student;

            if ($groups_id) {
                $fitneslist = $fitneslist->where('groups_id', $groups_id);
            }

            if ($year && $month) {
                $fitneslistmonth = FitnesList::whereMonth('created_at', $month)->whereYear('created_at', $year);
                $fitneslist = $fitneslist->with(['Fitnes_list' => function ($query) use ($year, $month) {
                    $query->orderBy('is_fit', 'asc')->whereYear('created_at', $year)->whereMonth('created_at', $month);
                }])->whereIn('id', $fitneslistmonth->pluck('students_id'));
            }

            if ($year && $month && $day) {
                $fitneslistdate = FitnesList::whereDay('created_at', $day)->whereMonth('created_at', $month)->whereYear('created_at', $year);
                $fitneslist = $fitneslist->with(['Fitnes_list' => function ($query) use ($year, $month, $day) {
                    $query->whereYear('created_at', $year)->whereMonth('created_at', $month)->whereDay('created_at', $day);
                }])->whereIn('id', $fitneslistdate->pluck('students_id'));
            }


            if (!$fitneslist->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Fitness List not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Fitness list found',
                'Data' => $fitneslist->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
