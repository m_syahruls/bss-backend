<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;

use Illuminate\Http\Request;

class CoachAdvertisementController extends Controller
{
    /**
     * It returns all the advertisements that are airing today and in the future.
     *
     * @param Request request The request object.
     */

    /**
     * @OA\Get(
     *     path="/coach/advertisement/get",
     *     tags={"Coach Advertisement"},
     *     summary="Get advertisement for coach",
     *     description="Get all advertisement for coach",
     *     operationId="getAdvetisementExamCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report exam template",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Advertisement found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Advertisement not found",
     *     ),
     * )
     */
    public function getAdvertisement(Request $request)
    {
        try {
            $id = $request->id;

            // query
            $advertisement = Advertisement::query();

            // if there is $id request
            if ($id) {
                $advertisement = $advertisement->find($id);
            } else {
                // query airing ads
                $advertisement = $advertisement->whereDate('date_airing', '>=', \Carbon\Carbon::now())->get();
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Advertisemend found',
                'Data' => $advertisement,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
