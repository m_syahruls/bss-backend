<?php

namespace App\Http\Controllers\Coach;

use App\Models\User;
use App\Models\Coach;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoachGroupController extends Controller
{

    /**
     * The below function is used to get group for coach.
     *
     * @param Request request The request object.
     *
     * @return group is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/group/get",
     *     tags={"Coach Group"},
     *     summary="Get group for coach",
     *     description="Get all group material for coach",
     *     operationId="getGroupCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Group found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Group not found",
     *     ),
     * )
     */
    public function getgroup()
    {
        try {
            $coach = User::with('Coach')->where('id', Auth::user()->id)->first();
            $group = Coach::with('Groups')->where('id', $coach->Coach->id)->first();

            if (!$group->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Group not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Group found',
                'Data' => $group,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
