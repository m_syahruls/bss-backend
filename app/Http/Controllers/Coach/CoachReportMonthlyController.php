<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\ReportExamSkill;
use App\Models\ReportMonthlys;
use App\Models\ReportMonthlySkill;
use App\Models\ReportMonthlyRolePositions;
use App\Models\RolePosition;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CoachReportMonthlyController extends Controller
{

    /**
     * The below function is used to get monthly report for coach.
     *
     * @param Request request The request object.
     *
     * @return monthly report is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/report/monthly/get",
     *     tags={"Coach Report Monthly"},
     *     summary="Get report monthly for coach",
     *     description="Get all report monthly for coach",
     *     operationId="getReportMonthlyCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report monthly",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Report Monthly found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Report Monthly not found",
     *     ),
     * )
     */
    public function getreportmonthly(Request $request)
    {
        try {
            $id = $request->id;
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $students_id =  $request->students_id;

            $reportmonthly = ReportMonthlys::with('Admin:id,name')->with(['Student' => function ($query) {
                $query->with('User');
            }])->with('Coach:id,name', 'Report_monthly_skills')->with(['Report_monthly_role_positions' => function ($query) {
                $query->with('Role_position');
            }]);

            if ($id) {
                $reportmonthly = $reportmonthly->where('id', $id);

                if (!$reportmonthly->exists()) {
                    return response()->json([
                        'Status' => false,
                        'Message' => 'Report monthly not found',
                    ], 200);
                }
            }

            if ($students_id) {
                $reportmonthly = $reportmonthly->where('students_id', $students_id);
            }

            if ($month) {
                $reportmonthly = $reportmonthly->where('month', $month);
            }

            if ($year) {
                $reportmonthly = $reportmonthly->where('year', $year);
            }

            if (!$reportmonthly->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report monthly not found',
                ], 200);
            }

            return response()->json([
                'Status' => true,
                'Message' => 'Report monthly found',
                'Data' => $reportmonthly->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to create monthly report for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/report/monthly/create",
     *     tags={"Coach Report Monthly"},
     *     summary="Create report monthly for coach",
     *     description="Create report monthly for coach",
     *     operationId="createReportMonthlyCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Report Monthly create successfully",
     *     ),
     * )
     */
    public function createreportmonthly(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'month' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $coaches_id =  User::with('Coach')->where('id', Auth::id())->first();
            $comment = $request->comment;
            $month = $request->month;
            $year = $request->year;
            $students = Student::with('User')->where('id', $students_id)->first();

            $age = $students->category;

            $reportmonthlycheck = ReportMonthlys::where('students_id', $students_id)->where('month', $month)->where('year', $year)->first();
            if ($reportmonthlycheck) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report monthly already exists',
                ], 400);
            }

            $reportmonthlyskill  = ReportMonthlys::create([
                'students_id' => $students_id,
                'coaches_id' => $coaches_id->Coach->id,
                'month' => $month,
                'year' => $year,
                'age' => $age,
            ]);

            $role_positions_id = $request->role_positions_id;

            foreach ($request->title as $key => $value) {
                $reportmonthlyskillarray = [
                    'report_monthlys_id' => $reportmonthlyskill->id,
                    'title' => $request->title[$key],
                    'category' => $request->category[$key],
                    'grade' => $request->grade[$key],
                    'desc' => $request->desc[$key],
                    'icon' =>  $request->icon[$key],
                ];
                ReportMonthlySkill::create($reportmonthlyskillarray);
            }

            $loop = ["loop1", "loop2"];

            foreach ($loop as $key => $value) {
                $roleposition = [
                    'role_positions_id' => $role_positions_id[$key],
                    'report_monthlys_id' => $reportmonthlyskill->id,
                ];

                ReportMonthlyRolePositions::create($roleposition);
            }


            $reportmonthlyskill->update([
                'comment' => $comment,
            ]);


            $reportmonthlyresult = ReportMonthlys::with('Report_monthly_skills', 'Report_monthly_role_positions')->latest()->first();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly created successfully',
                'data_old'          => null,
                'data_new'          => $reportmonthlyresult,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Report monthly created',
                'Data' => $reportmonthlyresult,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to update monthly report for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/report/monthly/update",
     *     tags={"Coach Report Monthly"},
     *     summary="Update report monthly for coach",
     *     description="Update report monthly for coach",
     *     operationId="ppdateReportMonthlyCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Report Monthly update successfully",
     *     ),
     * )
     */
    public function updatereportmonhtly(Request $request)
    {
        try {
            $user_auth = User::with('Admin')->where('id', Auth::id())->first();

            $id = $request->id;

            $reportmonthly = ReportMonthlys::where('id', $id)->with('Report_monthly_skills', 'Report_monthly_role_positions')->first();
            if (!$reportmonthly) {
                return response()->json([
                    'Message' => 'Report Monthly not found!
                    '
                ], 404);
            }
            /* Used to create a copy of the reportmonthly object. */
            $tempReportmonthly = $reportmonthly->replicate();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'month' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $coaches_id =  User::with('Coach')->where('id', Auth::id())->first();
            $comment = $request->comment;
            $month = $request->month;
            $year = $request->year;

            //get category from student to fill age
            $students = Student::with('User')->where('id', $students_id)->first();
            $age = $students->category;

            $reportmonthly->update([
                'students_id' => $students_id,
                'coaches_id' => $coaches_id->Coach->id,
                'month' => $month,
                'year' => $year,
                'age' => $age,
            ]);

            $reportmonthlyskillupdate = ReportMonthlySkill::where('report_monthlys_id', $reportmonthly->id)->get();

            foreach ($reportmonthlyskillupdate as $key => $value) {

                $reportmonthlyskillarray = [
                    'report_monthlys_id' => $reportmonthly->id,
                    'title' => $request->title[$key],
                    'category' => $request->category[$key],
                    'grade' => $request->grade[$key],
                    'desc' => $request->desc[$key],
                    'icon' => $request->icon[$key],
                ];
                ReportMonthlySkill::where('id', $value->id)->update($reportmonthlyskillarray);
            }

            $role_positions_id = $request->role_positions_id;

            $reportmonthlyroleposition = ReportMonthlyRolePositions::where('report_monthlys_id', $reportmonthly->id)->get();

            foreach ($reportmonthlyroleposition as $key => $value) {
                $rolepositionarray = [
                    'role_positions_id' => $role_positions_id[$key],
                    'report_monthlys_id' => $reportmonthly->id,
                ];

                ReportMonthlyRolePositions::where('id', $value->id)->update($rolepositionarray);
            }

            $reportmonthly->update([
                'comment' => $comment,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly updated successfully',
                'data_old'          => $tempReportmonthly,
                'data_new'          => $reportmonthly,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $reportmonthly,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'admin',
                'model_activity'    => 'reportMonthly',
                'causer'            => $user_auth->Admin->name,
                'log'               => 'Report monthly update failed',
                'data_old'          => $reportmonthly,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
