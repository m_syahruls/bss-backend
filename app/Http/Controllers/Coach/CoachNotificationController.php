<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationReceiver;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachNotificationController extends Controller
{
    /**
     * It gets the notification list of the currently logged in user
     *
     * @param Request request The request object.
     *
     * @return The notification list of the currently logged in user.
     */

    /**
     * @OA\Get(
     *     path="/coach/notification/get",
     *     tags={"Coach Notification"},
     *     summary="Get notification for coach",
     *     description="Get all notification for coach",
     *     operationId="getNotificationCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Notification found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Notification not found",
     *     ),
     * )
     */
    public function getNotification(Request $request)
    {
        /* This is used to get the user details of the logged in user. */
        $user_auth = User::with('Coach')->where('id', Auth::id())->first();

        $user = $user_auth->Coach->users_id;
        $limit = $request->limit;
        $notifications = Notification::whereHas('receivers', function ($query) use ($user) {
            $query->where('user_id', $user);
        })->orderBy('created_at', 'DESC')->paginate($limit);

        if (count($notifications) == 0) {
            return response()->json([
                'Status'    => false,
                'Message'   => 'Notification list not found',
                'Data'      => [],
            ], 200);
        }

        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        $receivers = NotificationReceiver::where('user_id', $user)->where('read_at', null);
        /* Used to create a copy of the receivers object. */
        $tempReceivers = clone $receivers->get();

        if (count($tempReceivers)) {
            try {
                /* Updating the `read_at` column of the `notification_receivers` table. */
                $receivers = $receivers->update([
                    'read_at' => $now,
                ]);

                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'coach',
                    'model_activity'    => 'notificationReceiver',
                    'causer'            => $user_auth->Coach->name,
                    'log'               => 'Notification read successfully',
                    'data_old'          => $tempReceivers,
                    'data_new'          => json_encode(['read_at' => $now]),
                ]);
            } catch (\Exception $error) {
                /* Used to create a log of the user activity. */
                UserActivity::create([
                    'model_user_role'   => 'coach',
                    'model_activity'    => 'notificationReceiver',
                    'causer'            => $user_auth->Coach->name,
                    'log'               => 'Notification read failed',
                    'data_old'          => $receivers,
                    'data_new'          => $error,
                ]);
            }
        }

        return response()->json([
            'Status' => true,
            'Message' => 'Notification list found',
            'Data' => $notifications,
        ]);
    }

    /**
     * It returns the number of unread notifications for the currently logged in user
     *
     * @return The number of unread notifications.
     */
    /**
     * @OA\Get(
     *     path="/coach/notification/unread",
     *     tags={"Coach Notification"},
     *     summary="Get notification unread for coach",
     *     description="Get all notification unread for coach",
     *     operationId="getNotificationUnreadCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Notification unread found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Notification unread not found",
     *     ),
     * )
     */
    public function getUnreadNotification()
    {
        /* Getting the user details of the currently logged in user. */
        $user_auth = User::with('Coach')->where('id', Auth::id())->first();

        $user = $user_auth->Coach->users_id;
        $notifications = Notification::whereHas('receivers', function ($query) use ($user) {
            $query->where('user_id', $user)->where('read_at', null);
        })->count();

        return response()->json([
            'Status' => true,
            'Message' => 'Unread notification found',
            'Data' => $notifications,
        ]);
    }
}
