<?php

namespace App\Http\Controllers\Coach;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\ExamLevel;
use App\Models\ExamSkill;
use App\Models\ExamSkillGroup;
use App\Models\ReportExam;
use App\Models\ReportExamLevel;
use App\Models\ReportExamSkill;
use App\Models\ReportExamSkillGroup;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CoachExamReportController extends Controller
{



    /**
     * The below function is used to get exam report template for coach.
     *
     * @param Request request The request object.
     *
     * @return exam report template being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/report/exam/template/get",
     *     tags={"Coach Report Exam"},
     *     summary="Get report exam template for coach",
     *     description="Get all report exam template for coach",
     *     operationId="getReportExamTemplateCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report exam template",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="10",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Report exam template found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Report exam template not found",
     *     ),
     * )
     */
    public function getexamtemplate(Request $request)
    {
        try {

            $limit = $request->limit;
            $id = $request->id;

            $examtemplate = ExamLevel::with(['Exam_skill_groups' => function ($query) {
                $query->with('Exam_skills');
            }]);

            if ($id) {
                $examtemplate = $examtemplate->where('id', $id);
            }

            if (!$examtemplate->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam not found',
                ], 200);
            }
            return response()->json([
                'Status' => true,
                'Message' => 'Exam template found',
                'Data' => $examtemplate->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to create exam report for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/report/exam/report/create",
     *     tags={"Coach Report Exam"},
     *     summary="Create report exam for coach",
     *     description="Create report exam for coach",
     *     operationId="createReportExamCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Report exam create successfully",
     *         @OA\JsonContent()
     *     ),
     * )
     */
    public function createexamreport(Request $request)
    {
        try {
            $user_auth = User::with('Coach')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $coaches_id = User::with('Coach')->where('id', Auth::id())->first();
            $admins_id = $request->admins_id;
            $date = $request->date;
            $is_passed = $request->is_passed;

            $title = $request->title;
            $desc = $request->desc;
            $level_logo_url = $request->level_logo_url;

            //this function for edit so this function will delete old report
            if ($request->id) {
                $examreport = ReportExam::where('id', $request->id)->first();
                $examreportlevel = ReportExamLevel::where('report_exams_id', $examreport->id)->get();
                $examskiilgroup = ReportExamSkillGroup::where('report_exam_levels_id', $examreportlevel->pluck('id'))->get();
                $examreportcheck = ReportExam::where('id', $request->id);

                if ($examreportcheck->exists()) {
                    ReportExamSkill::whereIn('report_exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                    ReportExamSkillGroup::whereIn('report_exam_levels_id', $examreportlevel->pluck('id'))->delete();
                    ReportExamLevel::where('report_exams_id', $examreport->id)->delete();
                    ReportExam::where('id', $request->id)->delete();
                }
            }

            $reportexamcheck = ReportExam::where('students_id', $students_id)->where('date', $date)->first();
            if ($reportexamcheck) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam already created',
                ], 404);
            }

            $examreport = ReportExam::create([
                'students_id' => $students_id,
                'coaches_id' => $coaches_id->Coach->id,
                'admins_id' => $admins_id,
                'date' => $date,
                'is_passed' => $is_passed,
            ]);

            $examlevelreport = ReportExamLevel::create([
                'report_exams_id' => $examreport->id,
                'title' => $title,
                'desc' => $desc,
                'level_logo_url' => $level_logo_url,
            ]);

            $index = 0;
            foreach ($request->title_skill_group as $key => $value) {
                $examskillgrouparray = [
                    'report_exam_levels_id' => $examlevelreport->id,
                    'title_skill_group' => $request->title_skill_group[$key],
                ];
                $examskillgroup = ReportExamSkillGroup::create($examskillgrouparray);

                $keys = count($request->name[$index]);

                for ($kunci = 0; $kunci < $keys; $kunci++) {
                    $examskillarray = [
                        'report_exam_skill_groups_id' => $examskillgroup->id,
                        'name' => $request->name[$index][$kunci],
                        'score' => $request->score[$index][$kunci],
                        'min_score' => $request->min_score[$index][$kunci],
                        'max_score' => $request->max_score[$index][$kunci],
                    ];
                    ReportExamSkill::create($examskillarray);
                }

                $index++;
            }

            $result = ReportExam::with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }])->where('id', $examreport->id)->get();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'reportExam',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Report exam created successfully',
                'data_old'          => null,
                'data_new'          => $result,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Exam report create successfully',
                'Data' => $result,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'reportExam',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Report exam create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The below function is used to get exam report for coach.
     *
     * @return exam report being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/report/exam/report/get",
     *     tags={"Coach Report Exam"},
     *     summary="Get report exam for coach",
     *     description="Get all report exam for coach",
     *     operationId="getReportExamCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="For get specific report exam template",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="students_id",
     *          description="For get report exam by students id",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="10",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Report exam found",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Report exam not found",
     *     ),
     * )
     */
    public function getexamreport(Request $request)
    {
        try {
            $month = $request->month;
            $year = $request->year;
            $limit = $request->limit;
            $group = $request->group;
            $id = $request->id;
            $students_id = $request->students_id;


            $reportexam = ReportExam::with('Student', 'Admin')->with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }]);

            if ($id) {
                $reportexam = $reportexam->where('id', $id)->whereMonth('date', $month)->whereYear('date', $year);
            }

            if ($month) {
                $reportexam = $reportexam->whereMonth('date', $month);
            }

            if ($year) {
                $reportexam = $reportexam->whereYear('date', $year);
            }

            if ($students_id) {
                $reportexam = $reportexam->where('students_id', $students_id);
            }

            if (!$reportexam->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Report Exam not found',
                ], 200);
            }
            return response()->json([
                'Status' => true,
                'Message' => 'Report exam found',
                'Data' => $reportexam->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function deletes the exam report.
     *
     * @param Request request The request object.
     *
     * @return a json response.
     */
    public function deleteexamreport(Request $request)
    {
        try {

            $examreport = ReportExam::where('id', $request->id)->first();
            $examreportlevel = ReportExamLevel::where('report_exams_id', $examreport->id)->get();
            $examskiilgroup = ReportExamSkillGroup::where('report_exam_levels_id', $examreportlevel->pluck('id'))->get();
            $examreportcheck = ReportExam::where('id', $request->id);

            $result = ReportExam::with(['Report_exam_levels' => function ($query) {
                $query->with(['Report_exam_skill_groups' => function ($query) {
                    $query->with('Report_exam_skills');
                }]);
            }])->where('id', $examreport->id)->get();


            if ($examreportcheck->exists()) {
                ReportExamSkill::whereIn('report_exam_skill_groups_id', $examskiilgroup->pluck('id'))->delete();
                ReportExamSkillGroup::whereIn('report_exam_levels_id', $examreportlevel->pluck('id'))->delete();
                ReportExamLevel::where('report_exams_id', $examreport->id)->delete();
                ReportExam::where('id', $request->id)->delete();
            }

            /* Used to create a log of the user activity. */


            return response()->json([
                'Status' => true,
                'Message' => 'Report exam delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
