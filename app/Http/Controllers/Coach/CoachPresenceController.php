<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\Group;
use App\Models\Presence;
use App\Models\Schedule;
use App\Models\Student;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CoachPresenceController extends Controller
{

    /**
     * The below function is used to get presence for coach.
     *
     * @param Request request The request object.
     *
     * @return presence is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/presence/get",
     *     tags={"Coach Presence"},
     *     summary="Get presence for coach",
     *     description="Get all presence for coach",
     *     operationId="getPresenceCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="For get specific presence",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="schedules_id",
     *          description="For filter by schedules",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="groups_id",
     *          description="For filter by group",
     *          example="1",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="month",
     *          description="For filter by month",
     *          example="5",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="year",
     *          description="For filter by year",
     *          example="2022",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *      @OA\Parameter(
     *          name="day",
     *          description="For filter by day",
     *          example="3",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *    @OA\Parameter(
     *          name="limit",
     *          description="For paginate",
     *          example="5",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Presence found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Presence not found",
     *     ),
     * )
     */
    public function getpresence(Request $request)
    {
        try {
            $id_presence = $request->id_presence;
            $month = $request->month;
            $year = $request->year;
            $day = $request->day;

            $id = $request->id;
            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $groups_id = $request->groups_id;
            $limit = $request->limit;
            $admin = $request->admin;

            $presence = new Student;

            if ($id_presence) {
                $presenceid = Presence::where('id', $id_presence);
                $presence = $presence->whereIn('id', $presenceid->pluck('students_id'));
            }

            if ($id) {
                $presence = Presence::with('Schedule')->where('id', $id);
            }

            if ($students_id) {
                $presence = $presence->where('id', $students_id);
            }

            if ($groups_id) {
                $presence = $presence->where('groups_id', $groups_id);
            }

            if ($year && $month && $day) {
                $presence = $presence->with(['Presence' => function ($query) use ($year, $month, $day) {
                    $query->whereYear('created_at', $year)->whereMonth('created_at', $month)->whereDay('created_at', $day);
                }]);
            }

            if ($schedules_id) {
                $presence = $presence->with(['Presence' => function ($query) use ($schedules_id) {
                    $query->where('schedules_id', $schedules_id);
                }]);
            }


            if (!$presence->exists()) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Presence not found',
                ], 200);
            }


            return response()->json([
                'Status' => true,
                'Message' => 'Presence found',
                'Data' => $presence->paginate($limit),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to create presence for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/presence/create",
     *     tags={"Coach Presence"},
     *     summary="Create presence for coach",
     *     description="Create presence for coach",
     *     operationId="createPresenceCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Presence create successfully",
     *     ),
     * )
     */
    public function createpresence(Request $request)
    {
        try {
            $user_auth = User::with('Coach')->where('id', Auth::id())->first();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'schedules_id' => 'required',
                'temperature' => 'required',
                'time_attended' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $coaches_id = User::with('Coach')->where('id', Auth::id())->first();
            $temperature = $request->temperature;
            $time_attended = $request->time_attended;
            $is_attended = $request->is_attended;

            $date_now = date('Y-m-d');

            $jadwal = Schedule::where('id', $schedules_id)->first();

            if ($jadwal->date != $date_now) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Cant create presence',
                ], 400);
            }


            $presence = Presence::create([
                'students_id' => $students_id,
                'schedules_id' => $schedules_id,
                'coaches_id' => $coaches_id->Coach->id,
                'temperature' => $temperature,
                'time_attended' => $time_attended,
                'is_attended' => $is_attended,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Presence created successfully',
                'data_old'          => null,
                'data_new'          => $presence,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Presence create successfully',
                'Data' => $presence,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Presence create failed',
                'data_old'          => null,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to update presence for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/presence/update",
     *     tags={"Coach Presence"},
     *     summary="Update presence for coach",
     *     description="Update presence for coach",
     *     operationId="updatePresenceCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Presence update successfully",
     *     ),
     * )
     */
    public function updatepresence(Request $request)
    {
        try {
            $user_auth = User::with('Coach')->where('id', Auth::id())->first();

            $id = $request->id;

            $presence = Presence::where('id', $id)->first();
            if (!$presence) {
                return response()->json([
                    'message' => 'Presence not found'
                ], 404);
            }
            /* Used to create a copy of the presence object. */
            $tempPresence = $presence->replicate();

            $validator = Validator::make($request->all(), [
                'students_id' => 'required',
                'schedules_id' => 'required',
                'temperature' => 'required',
                'time_attended' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $students_id = $request->students_id;
            $schedules_id = $request->schedules_id;
            $coaches_id = User::with('Coach')->where('id', Auth::id())->first();
            $temperature = $request->temperature;
            $time_attended = $request->time_attended;
            $is_attended = $request->is_attended;


            $presence->update([
                'students_id' => $students_id,
                'schedules_id' => $schedules_id,
                'coaches_id' => $coaches_id->Coach->id,
                'temperature' => $temperature,
                'time_attended' => $time_attended,
                'is_attended' => $is_attended,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Presence updated successfully',
                'data_old'          => $tempPresence,
                'data_new'          => $presence,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $presence,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'presence',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Presence update failed',
                'data_old'          => $presence,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
