<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Mail\Bssmail;
use App\Models\Coach;
use App\Models\User;
use App\Models\UserActivity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CoachController extends Controller
{

    /**
     * The below function is used to login for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/logincoach",
     *     tags={"Coach Auth"},
     *     summary="Login coach",
     *     description="Login coach",
     *     operationId="LoginCoach",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username","password"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *              @OA\Property(property="password", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Login successfully",
     *     ),
     * )
     */
    public function logincoach(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'status' => false,
                        $validator->errors(),
                    ],
                    400
                );
            }


            $credentials = request(['username', 'password']);

            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username or password is wrong',
                ]);
            }

            $usercek = User::where('username', $request->username)->first();

            $coachcek = Coach::where('users_id', $usercek->id)->first();

            if (!$coachcek) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Youre not coach',
                    ]
                );
            }

            $usercoach = User::where('username', $request->username)->first();
            if (!Hash::check($request->password, $usercoach->password, [])) {
                throw new \Exception('Invalid Credentials');
            }

            $data = User::where('id', $usercoach->id)->with(['Coach' => function ($query) {
                $query->with('Groups');
            }])->first();

            $token = $usercoach->createToken('authToken')->plainTextToken;


            return response()->json([
                'Status' => true,
                'Message' => 'Login successfully',
                'access_token' => $token,
                'token_type' => 'Bearer',
                'Data' => $data,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Cant access',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to logout for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/logoutcoach",
     *     tags={"Coach Auth"},
     *     summary="Logout coach",
     *     description="Logout coach",
     *     operationId="LogoutCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Logout successfully",
     *     ),
     * )
     */
    public function logoutcoach(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();
        return response()->json([
            'Status' => true,
            'Message' => 'Token deleted',
        ]);
    }


    /**
     * The below function is used to get profile coach.
     *
     * @param Request request The request object.
     *
     * @return profile coach is being returned.
     */
    /**
     * @OA\Get(
     *     path="/coach/profile/get",
     *     tags={"Coach Auth"},
     *     summary="Get Profile for coach",
     *     description="Get profile for coach",
     *     operationId="getProfileCoach",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Profile coach found",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Profile coach not found",
     *     ),
     * )
     */
    public function profilecoach(Request $request)
    {
        try {
            $user = User::with('Coach')->where('id', Auth::user()->id)->first();
            $coach = Coach::with('User')->where('id', $user->Coach->id)->first();
            return response()->json([
                'Status' => true,
                'Message' => 'Profile Coach',
                'Data' => $coach,
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }


    /**
     * The below function is used to set new password when first login for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/update/password",
     *     tags={"Coach Auth"},
     *     summary="Update password coach",
     *     description="Update password coach",
     *     operationId="UpdatePasswordCoach",
     *     security={{"bearer_token":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"newpassword"},
     *              @OA\Property(property="newpassword", type="string", example="123456"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Update successfully",
     *     ),
     * )
     */
    public function newpassword(Request $request)
    {
        try {
            $user_auth = User::with('Coach')->where('id', Auth::id())->first();

            $user = User::with('Coach')->where('id', Auth::user()->id)->first();
            $coach = Coach::with('User')->where('id', $user->coach->id)->first();
            /* Creating a copy of the coach object. */
            $tempCoach = $coach->replicate();

            $newpassword = $request->newpassword;
            $hashpass = Hash::make($newpassword);

            if ($user->has_changed_password == 1) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'You have already changed your password',
                ]);
            }

            $validator = Validator::make($request->all(), [
                'newpassword' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'Status' => false,
                        'Message' => 'Something wrong',
                        'Data' => $validator->errors(),
                    ],
                    400
                );
            }

            $user->update([
                'password' => $hashpass,
                'has_changed_password' => 1,
            ]);

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Coach password updated successfully',
                'data_old'          => $tempCoach,
                'data_new'          => $coach,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Update successfully',
                'Data' => $coach,
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Coach password update failed',
                'data_old'          => $coach,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The below function is used reset password for coach.
     *
     * @param Request request The request object.
     */
    /**
     * @OA\Post(
     *     path="/coach/resetpass",
     *     tags={"Coach Auth"},
     *     summary="Reset password coach",
     *     description="Reset password coach",
     *     operationId="ResetPasswordCoach",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"username"},
     *              @OA\Property(property="username", type="string", example="andisan"),
     *          ),
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Reset successfully",
     *     ),
     * )
     */
    public function sendemailresetpassword(Request $request)
    {
        try {
            $username = $request->username;

            $user = User::with('Coach')->where('username', $username)->first();

            $email = $user->email;

            if (!$user) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Username not found',
                ], 400);
            }

            $resetpassword = [
                'parent' => '',
                'namestudent' => $user->Coach->name,
                'email' => $email,
                'username' => $username,
                'role' => 'coach',
            ];

            Mail::to($email)->send(new Bssmail($resetpassword));

            return response()->json([
                'Status' => true,
                'Message' => 'Email sent successfully',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }

    /**
     * The above function is used to delete a coach.
     *
     * @param Request request The request object.
     *
     * @return The coach is being deleted from the database.
     */
    public function coachdelete(Request $request)
    {
        try {
            $user_auth = User::with('Coach')->where('id', Auth::id())->first();

            $id = $request->id;

            $coach = Coach::where('id', $id)->first();
            $user = User::where('id', $coach->users_id)->first();
            if (!$coach) {
                return response()->json([
                    'Status' => false,
                    'Message' => 'Coach not found',
                ], 200);
            }
            /* Used to create a copy of the coach object. */
            $tempCoach = $coach->replicate();

            $coach->delete();
            $user->delete();

            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Coach deleted successfully',
                'data_old'          => $tempCoach,
                'data_new'          => null,
            ]);

            return response()->json([
                'Status' => true,
                'Message' => 'Delete successfully',
            ]);
        } catch (\Exception $error) {
            /* Used to create a log of the user activity. */
            UserActivity::create([
                'model_user_role'   => 'coach',
                'model_activity'    => 'coach',
                'causer'            => $user_auth->Coach->name,
                'log'               => 'Coach delete failed',
                'data_old'          => $coach,
                'data_new'          => $error,
            ]);

            return response()->json([
                'Status' => 'error',
                'Message' => 'Something wrong',
                'error' => $error,
            ]);
        }
    }
}
