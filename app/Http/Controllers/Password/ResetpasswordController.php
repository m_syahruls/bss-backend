<?php

namespace App\Http\Controllers\Password;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ResetpasswordController extends Controller
{
    public function reset($email)
    {
        $emails = Crypt::decrypt($email);
        return view('email.resetpassword')->with('emails', $emails);
    }

    public function success()
    {
        return view('email.emailsuccess');
    }

    public function resetstore(Request $request)
    {
        $email = $request->email;
        $password = $request->newpassword;
        $password_confirmation = $request->confirmpassword;

        if (Str::length($password) < 6) {
            Session::flash('message', 'Password kurang dari 6 karakter!');
            Session::flash('alert-class', 'alert-danger');
            return redirect(route('reset', ['email' => Crypt::encrypt($email)]));
        }

        if ($password == $password_confirmation) {
            $user = User::where('email', $email)->first();
            $user->update([
                'password' => Hash::make($password),
            ]);
            return redirect(route('success'));
        } else {
            Session::flash('message', 'Password confirm tidak sama');
            Session::flash('alert-class', 'alert-danger');
            return redirect(route('reset', ['email' => Crypt::encrypt($email)]));
        }
    }
}
