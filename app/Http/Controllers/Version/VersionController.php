<?php

namespace App\Http\Controllers\Version;

use App\Http\Controllers\Controller;
use App\Models\Version;

use Illuminate\Http\Request;

class VersionController extends Controller
{
    /**
     * It returns the latest version of the app.
     *
     * @param Request request type
     *
     * @return The version of the app
     */
    /**
     * @OA\Tag(
     *     name="Version",
     *     description="API Endpoints of App Versions"
     * )
     * @OA\Get(
     *      path="/version",
     *      tags={"Version"},
     *      summary="Get latest version of the app",
     *      description="An endpoint that response the latest version of the app based on type",
     *      operationId="GetVersion",
     *      @OA\Parameter(
     *          name="type",
     *          description="type of selected version",
     *          example="student_android",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="App version found",
     *          @OA\JsonContent(),
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Type version not found",
     *          @OA\JsonContent(),
     *      ),
     * )
     */
    public function getVersion(Request $request)
    {
        $version = Version::select('version')->where('type', $request->type)->latest()->first();

        if (!$version) {
            return response()->json([
                'Status' => 'error',
                'Message' => 'Type version not found'
            ], 200);
        }

        return response()->json([
            'Status' => true,
            'Message' => 'App version found',
            'Data' => $version,
        ]);
    }
}
