<?php

namespace App\Http\Controllers\Manual;

use App\Models\Admin;
use App\Models\Coach;
use App\Models\Group;
use GuzzleHttp\Client;
use App\Models\Student;
use App\Models\Schedule;
use App\Models\ScheduleCategory;
use App\Models\CoachGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManualScheduleController extends Controller
{

    public function schedulepage()
    {

        $datagroup = Group::all();

        $dataadmin = Admin::all();

        $dataschedulecategory = ScheduleCategory::all();

        return view('manualadmin.schedule')->with('datagroup', $datagroup)
            ->with('dataadmin', $dataadmin)->with('dataschedulecategory',$dataschedulecategory);
    }

    public function getschedule($id)
    {

        $dataschedule = Schedule::where('groups_id', $id)->get();

        return response()->json(['schedule' => $dataschedule]);
    }

    public function getstudent($id)
    {
        $datastudent = Student::where('groups_id', $id)->get();

        return response()->json(['student' => $datastudent]);
    }

    public function getcoach($id)
    {

        $groupcoach = CoachGroup::where('groups_id', $id)->get();
        $datacoach = Coach::where('id', $groupcoach->pluck('coaches_id'))->get();

        return response()->json(['coach' => $datacoach]);
    }

    public function insertschedulemanual(Request $request)
    {
        $groups_id = $request->input('selectgroup');
        $title = $request->input('title');
        $desc = $request->input('desc');
        $date = $request->input('date');
        $time = $request->input('time');
        $location = $request->input('location');
        $coordinate = $request->input('coordinate');
        $schedule_categories_id = $request->input('schedule_categories_id');

        Schedule::insert([
            'groups_id' => $groups_id,
            'title' => $title,
            'desc' => $desc,
            'date' => $date,
            'time' => $time,
            'location' => $location,
            'coordinate' => $coordinate,
            'schedule_categories_id' => $schedule_categories_id,
        ]);

        return redirect()->route('mainpage');
    }
}
