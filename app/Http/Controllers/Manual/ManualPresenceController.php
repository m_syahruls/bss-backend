<?php

namespace App\Http\Controllers\Manual;

use App\Models\Admin;
use App\Models\Coach;
use App\Models\Group;
use GuzzleHttp\Client;
use App\Models\Student;
use App\Models\Presence;
use App\Models\Schedule;
use App\Models\CoachGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManualPresenceController extends Controller
{

    public function presencepage()
    {

        $datagroup = Group::all();

        $dataadmin = Admin::all();

        return view('manualadmin.presence')->with('datagroup', $datagroup)
            ->with('dataadmin', $dataadmin);
    }

    public function getschedule($id)
    {

        $dataschedule = Schedule::where('groups_id', $id)->get();

        return response()->json(['schedule' => $dataschedule]);
    }

    public function getstudent($id)
    {
        $datastudent = Student::where('groups_id', $id)->get();

        return response()->json(['student' => $datastudent]);
    }

    public function getcoach($id)
    {

        $groupcoach = CoachGroup::where('groups_id', $id)->get();
        $datacoach = Coach::where('id', $groupcoach->pluck('coaches_id'))->get();

        return response()->json(['coach' => $datacoach]);
    }

    public function insertpresencemanual(Request $request)
    {
        $students_id = $request->input('selectstudent');
        $schedules_id = $request->input('selectschedule');
        $temperature = $request->input('temperature');
        $attended = $request->input('timeattended');
        $date = $request->input('date');
        $admins_id = $request->input('select-admin');
        $coaches_id = $request->input('selectcoach');
        $isattended = 1;

        Presence::insert([
            'students_id' => $students_id,
            'schedules_id' => $schedules_id,
            'temperature' => $temperature,
            'time_attended' => $attended,
            'is_attended' => $isattended,
            'created_at' => $date,
            'admins_id' => $admins_id,
            'coaches_id' => $coaches_id,
        ]);

        return redirect()->route('presencepage');
    }
}
