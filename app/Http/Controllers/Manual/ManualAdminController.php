<?php

namespace App\Http\Controllers\Manual;

use App\Models\Coach;
use App\Models\Group;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\UserActivity;

class ManualAdminController extends Controller
{
    public function mainpage()
    {
        $logdata = UserActivity::orderBy('created_at', 'desc')->paginate(10);

        return view('manualadmin.main')->with('logdata', $logdata);
    }

    public function getstudent()
    {
        $student = Student::get();
        $studentcount = count($student);

        return response()->json(['studentcount' => $studentcount]);
    }

    public function getcoach()
    {
        $coach = Coach::get();
        $coachcount = count($coach);

        return response()->json(['coachcount' => $coachcount]);
    }

    public function getgroup()
    {
        $group = Group::get();
        $groupcount = count($group);

        return response()->json(['groupcount' => $groupcount]);
    }

    // public function getlogdata()
    // {
    //     $logdata = UserActivity::paginate(10);

    //     return response()->json(['logdata' => $logdata]);
    // }
}
