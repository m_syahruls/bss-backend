<?php

namespace App\Http\Controllers\Manual;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class ManualAuthAdminController extends Controller
{
    public function login()
    {
        return view('manualadmin.AuthManual.login');
    }

    public function actionlogin(Request $request)
    {
        $username = $request->input('username');
        $key = $request->input('key');

        if ($username == 'adminsynapsis' && $key == '12345678') {
            $request->session()->put('key', '12345678');
            return redirect('/manual/admin');
        } else {
            Session::flash('error', 'Username atau key salah');
            return redirect('/manual/admin/login');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('key');
        return redirect('/manual/admin/login');
    }
}
