<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use Illuminate\Http\Request;

class ExportController extends Controller
{
    /**
     * It creates a new instance of the UsersExport class, which is a class that extends the
     * Maatwebsite\Excel\Concerns\FromCollection trait, and then downloads the file
     *
     * @return A new instance of the UsersExport class.
     */
    public function download()
    {
        $now = \Carbon\Carbon::now()->format('Y-m-d h:i:s');
        return (new UsersExport())->download('BSS Users exported at ' . $now . '.xlsx');
    }
}
