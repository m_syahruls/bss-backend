<?php

namespace App\Http\Controllers\Log;

use App\Http\Controllers\Controller;
use App\Models\Log;

use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * It returns the log in descending order.
     *
     * @return A collection of all the logs in the database.
     */
    /**
     * @OA\Tag(
     *      name="Log",
     *      description="API Endpoints of Error Logs from App"
     * )
     * @OA\Get(
     *      path="/logs/get",
     *      tags={"Log"},
     *      summary="Get all error logs",
     *      description="An endpoint that response all error logs recorded",
     *      operationId="GetLog",
     *      @OA\Response(
     *          response="200",
     *          description="List of logs",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="id", type="int", example="1"),
     *                  @OA\Property(property="log", type="string", example="Error: @BadMethod something at line 69"),
     *                  @OA\Property(property="created_at", type="string", example="2022-02-02 20:02:20"),
     *                  @OA\Property(property="updated_at", type="string", example="2022-02-02 20:02:20"),
     *              ),
     *          ),
     *      ),
     * )
     */
    public function getLog()
    {
        return Log::orderBy('created_at', 'DESC')->get();
    }

    /**
     * It creates a new log entry in the database
     *
     * @param Request request The request object.
     *
     * @return A new log is being created with the log data from the request.
     */
    /**
     * @OA\Post(
     *      path="/logs/create",
     *      tags={"Log"},
     *      summary="Create new log",
     *      description="An endpoint that create new Log",
     *      operationId="CreateLog",
     *      @OA\Parameter(name="log", description="log that need to record", example="Error: @BadMethod something at line 69", required=true, in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Log created successfully",
     *          @OA\JsonContent(
     *              @OA\Property(property="log", type="string", example="Error: @BadMethod something at line 69"),
     *              @OA\Property(property="created_at", type="string", example="2022-02-02 20:02:20"),
     *              @OA\Property(property="updated_at", type="string", example="2022-02-02 20:02:20"),
     *              @OA\Property(property="id", type="int", example="1"),
     *          ),
     *      ),
     * )
     */
    public function createLog(Request $request)
    {
        return Log::create(['log' => $request->log]);
    }
}
