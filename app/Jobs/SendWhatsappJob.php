<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Whatsapp\SendWhatsappNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendWhatsappJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 7200; // 2 hours
    public $whatsappdata;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($whatsappdata)
    {
        $this->whatsappdata = $whatsappdata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        new SendWhatsappNotification($this->whatsappdata);
    }
}
