<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FitnesList extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'point',
        'students_id',
        'answer',
        'is_fit',
    ];

    protected $appends = ['Year', 'Month', 'Day', 'Date', 'Time'];

    public function getYearAttribute()
    {
        return substr($this->created_at, 0, 4);
    }

    public function getMonthAttribute()
    {
        return substr($this->created_at, 5, 2);
    }

    public function getDayAttribute()
    {
        return substr($this->created_at, 8, 2);
    }

    public function getDateAttribute()
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        return $date->setTimezone('Asia/Jakarta')->format('D, d-m-Y');
        // return $this->created_at->format('D, d-m-Y');
    }

    public function getTimeAttribute()
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        return $date->setTimezone('Asia/Jakarta')->format('H:i:s');
        // return $this->created_at->format('H:i:s');
    }

    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }
}
