<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Advertisement extends Model
{
    use HasFactory;

    protected $fillable = [
        'owner',
        'title',
        'desc',
        'date_airing',
        'url_link',
        'image_link',
        'image_name',
    ];
}
