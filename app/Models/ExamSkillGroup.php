<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamSkillGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'exam_levels_id',
        'title_skill_group',
    ];

    public function Exam_level()
    {
        return $this->belongsTo(ExamLevel::class, 'exam_levels_id', 'id');
    }


    public function Exam_skills()
    {
        return $this->hasMany(ExamSkill::class, 'exam_skill_groups_id', 'id');
    }
}
