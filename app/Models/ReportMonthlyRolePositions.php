<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportMonthlyRolePositions extends Model
{
    use HasFactory;

    protected $fillable = [
        'role_positions_id',
        // 'name',
        // 'image_link',
        // 'position',
        // 'role',
        'report_monthlys_id',
    ];



    public function Report_monthly()
    {
        return $this->belongsTo(ReportMonthlys::class, 'report_monthlys_id', 'id');
    }

    public function Role_position()
    {
        return $this->belongsTo(RolePosition::class, 'role_positions_id', 'id');
    }
}
