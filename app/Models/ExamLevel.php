<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamLevel extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'desc',
        'level_logo_url',
    ];

    protected $appends = [
        'url'
    ];

    public function Exam_skill_groups()
    {
        return $this->hasMany(ExamSkillGroup::class, 'exam_levels_id', 'id');
    }

    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->level_logo_url);
    }
}
