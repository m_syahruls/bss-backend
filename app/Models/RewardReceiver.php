<?php

namespace App\Models;

use App\Models\Reward;
use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RewardReceiver extends Model
{
    use HasFactory;

    protected $fillable = [
        'rewards_id',
        'students_id',
        'status',
        'date_claimed'
    ];

    public function Reward()
    {
        return $this->belongsTo(Reward::class, 'rewards_id', 'id');
    }

    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }
}
