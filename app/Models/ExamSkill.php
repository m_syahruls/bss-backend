<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'exam_skill_groups_id',
        'name',
        'min_score',
        'max_score',
    ];

    public function Exam_skill_group()
    {
        return $this->belongsTo(ExamSkillGroup::class, 'exam_skill_groups_id', 'id');
    }
}
