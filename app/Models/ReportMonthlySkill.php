<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportMonthlySkill extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'report_monthlys_id',
        'title',
        'category',
        'grade',
        'desc',
        'icon',
    ];

    public function Report_monthly()
    {
        return $this->belongsTo(ReportMonthlys::class, 'report_monthlys_id', 'id');
    }
}
