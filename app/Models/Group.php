<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function Students()
    {
        return $this->hasMany(Student::class, 'groups_id', 'id');
    }

    public function Studies()
    {
        return $this->hasMany(Study::class, 'groups_id', 'id');
    }

    public function Schedules()
    {
        return $this->hasMany(Schedule::class, 'groups_id', 'id');
    }

    public function Coaches()
    {
        return $this->belongsToMany(Coach::class, 'coach_groups', 'groups_id', 'coaches_id');
    }

    public function Presences()
    {
        return $this->hasManyThrough(Presence::class, Schedule::class, 'groups_id', 'schedules_id', 'id', 'id');
    }
}
