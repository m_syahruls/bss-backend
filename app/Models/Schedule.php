<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'groups_id',
        'title',
        'desc',
        'date',
        'time',
        'location',
        'coordinate',
        'schedule_categories_id',
    ];

    protected $appends = ['Year', 'Month', 'Day'];

    public function getYearAttribute()
    {
        return substr($this->date, 0, 4);
    }

    public function getMonthAttribute()
    {
        return substr($this->date, 5, 2);
    }

    public function getDayAttribute()
    {
        return substr($this->date, 8, 2);
    }


    public function Group()
    {
        return $this->belongsTo(Group::class, 'groups_id', 'id');
    }

    public function Presences()
    {
        return $this->hasMany(Presence::class, 'schedules_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo(Coach::class, 'coaches_id', 'id');
    }

    public function Schedule_category()
    {
        return $this->belongsTo(ScheduleCategory::class, 'schedule_categories_id', 'id');
    }
}
