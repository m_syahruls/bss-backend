<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'model_user_role',
        'model_activity',
        'causer',
        'log',
        'data_old',
        'data_new',
        'read_at',
    ];

    public function getCreatedAtAttribute($date)
    {
        $date = \Carbon\Carbon::parse($date);
        return $date->setTimezone('Asia/Jakarta')->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute($date)
    {
        $date = \Carbon\Carbon::parse($date);
        return $date->setTimezone('Asia/Jakarta')->format('Y-m-d H:i:s');
    }
}
