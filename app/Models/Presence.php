<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presence extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'students_id',
        'admins_id',
        'schedules_id',
        'coaches_id',
        'temperature',
        'time_attended',
        'is_attended',
        'created_at',
    ];

    protected $appends = ['Year', 'Month', 'Day', 'Date'];

    public function getYearAttribute()
    {
        return substr($this->created_at, 0, 4);
    }

    public function getMonthAttribute()
    {
        return substr($this->created_at, 5, 2);
    }

    public function getDayAttribute()
    {
        return substr($this->created_at, 8, 2);
    }

    public function getDateAttribute()
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        return $date->setTimezone('Asia/Jakarta')->format('D, d-m-Y');
        // return $this->created_at->format('D, d-m-Y');
    }

    public function Admin()
    {
        return $this->belongsTo(Admin::class, 'admins_id', 'id');
    }

    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }

    public function Schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedules_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo(Coach::class, 'coaches_id', 'id');
    }

    public function Groups()
    {
        return $this->hasManyThrough(Group::class, Schedule::class, 'groups_id', 'schedules_id', 'id', 'id');
    }
}
