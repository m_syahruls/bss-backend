<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportExamSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'report_exam_skill_groups_id',
        'name',
        'score',
        'min_score',
        'max_score',
    ];

    public function Report_exam_skill_group()
    {
        return $this->belongsTo(ReportExamSkillGroup::class, 'report_exam_skill_groups_id', 'id');
    }
}
