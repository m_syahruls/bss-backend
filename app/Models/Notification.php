<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'admins_id',
        'title',
        'desc',
        'date',
        'category',
    ];

    protected $appends = [
        'name',
    ];

    public function receivers()
    {
        return $this->hasMany(NotificationReceiver::class, 'notification_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admins_id', 'id');
    }

    public function getNameAttribute()
    {
        $query = $this->belongsTo(Admin::class, 'admins_id', 'id')->pluck('name')->first();
        if ($query) {
            return $query;
        }
        return 'System';
    }
}
