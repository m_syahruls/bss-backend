<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportExam extends Model
{
    use HasFactory;

    protected $fillable = [
        'students_id',
        'coaches_id',
        'admins_id',
        'date',
        'is_passed',
    ];

    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo(Coach::class, 'coaches_id', 'id');
    }

    public function Admin()
    {
        return $this->belongsTo(Admin::class, 'admins_id', 'id');
    }

    public function Report_exam_levels()
    {
        return $this->hasMany(ReportExamLevel::class, 'report_exams_id', 'id');
    }
}
