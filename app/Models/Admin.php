<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Admin extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'name',
        'birthday',
        'phone',
        'role',
        'img_url',
        'img_name',
        'is_active',
    ];

    protected $appends = [
        'url'
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function Studies()
    {
        return $this->hasMany(Study::class, 'admins_id', 'id');
    }

    public function Presences()
    {
        return $this->hasMany(Presence::class, 'admins_id', 'id');
    }

    public function Report_monthlys()
    {
        return $this->hasMany(Report_monthly::class, 'admins_id', 'id');
    }

    public function Report_exams()
    {
        return $this->hasMany(Report_exam::class, 'admins_id', 'id');
    }

    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->img_url);
    }
}
