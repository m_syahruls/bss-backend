<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'students_id',
        'status',
        'month',
        'year',
        'total',
        'date_payment_received',
    ];


    // protected $appends = ['Year', 'Month', 'Day'];

    // public function getYearAttribute()
    // {
    //     return substr($this->created_at, 0, 4);
    // }

    // public function getMonthAttribute()
    // {
    //     return substr($this->created_at, 5, 2);
    // }

    // public function getDayAttribute()
    // {
    //     return substr($this->created_at, 8, 2);
    // }


    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }
}
