<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoachGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'coaches_id',
        'groups_id',
    ];
}
