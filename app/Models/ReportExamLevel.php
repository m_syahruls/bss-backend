<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportExamLevel extends Model
{
    use HasFactory;


    protected $fillable = [
        'report_exams_id',
        'title',
        'desc',
        'level_logo_url',
    ];

    public function Report_exams()
    {
        return $this->belongsTo(ReportExam::class, 'report_exams_id', 'id');
    }

    public function Report_exam_skill_groups()
    {
        return $this->hasMany(ReportExamSkillGroup::class, 'report_exam_levels_id', 'id');
    }
}
