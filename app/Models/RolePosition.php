<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RolePosition extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'image_link',
        'img_name',
        'position',
        'role',
    ];

    protected $appends = [
        'url'
    ];

    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->image_link);
    }


    public function Report_monthly_role_models()
    {
        return $this->hasMany(ReportMonthlyRoleModels::class, 'role_position_id', 'id');
    }
}
