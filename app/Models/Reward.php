<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'desc',
        'reward_type',
        'image',
        'img_name',
    ];

    public function RewardReceivers()
    {
        return $this->hasMany(RewardReceiver::class, 'rewards_id', 'id');
    }
}
