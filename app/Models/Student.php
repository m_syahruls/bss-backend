<?php

namespace App\Models;

use App\Models\RewardReceiver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'groups_id',
        'registration_no',
        'name',
        'nick_name',
        'mother_name',
        'mother_phone',
        'father_name',
        'father_phone',
        'birthday',
        'img_url',
        'img_name',
        'joined_at',
        'category',
        'is_active',
        'va_number',
    ];

    protected $appends = [
        'url'
    ];

    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->img_url);
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function Groups()
    {
        return $this->belongsTo(Group::class, 'groups_id', 'id');
    }

    public function Fitnes_list()
    {
        return $this->hasMany(FitnesList::class, 'students_id', 'id');
    }

    public function Payment()
    {
        return $this->hasMany(Payment::class, 'students_id', 'id');
    }

    public function Presence()
    {
        return $this->hasMany(Presence::class, 'students_id', 'id');
    }

    public function Report_monthlys()
    {
        return $this->hasMany(ReportMonthlys::class, 'students_id', 'id');
    }

    public function Report_exams()
    {
        return $this->hasMany(ReportExam::class, 'students_id', 'id');
    }

    public function Reward()
    {
        return $this->belongsTo(RewardReceiver::class, 'students_id', 'id');
    }

    // public function getJoinedAtAttribute()
    // {
    //     $date = \Carbon\Carbon::parse($this->joined_at);
    //     return $date->setTimezone('Asia/Jakarta')->format('D, d-m-Y');
    //     // return $this->created_at->format('D, d-m-Y');
    // }
}
