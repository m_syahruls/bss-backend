<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportMonthlys extends Model
{
    use HasFactory;

    protected $fillable = [
        'students_id',
        'coaches_id',
        'admins_id',
        'comment',
        'month',
        'year',
        'age',
    ];


    // protected $appends = ['Year', 'Month', 'Day'];

    // public function getYearAttribute()
    // {
    //     return substr($this->created_at, 0, 4);
    // }

    // public function getMonthAttribute()
    // {
    //     return substr($this->created_at, 5, 2);
    // }

    // public function getDayAttribute()
    // {
    //     return substr($this->created_at, 8, 2);
    // }

    public function Student()
    {
        return $this->belongsTo(Student::class, 'students_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo(Coach::class, 'coaches_id', 'id');
    }

    public function Admin()
    {
        return $this->belongsTo(Admin::class, 'admins_id', 'id');
    }

    public function Report_monthly_role_positions()
    {
        return $this->hasMany(ReportMonthlyRolePositions::class, 'report_monthlys_id', 'id');
    }

    public function Report_monthly_skills()
    {
        return $this->hasMany(ReportMonthlySkill::class, 'report_monthlys_id', 'id');
    }
}
