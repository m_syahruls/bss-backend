<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Study extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'coaches_id',
        'admins_id',
        'groups_id',
        'title',
        'desc',
        'file_url',
        'file_name',
        'is_pdf',
        'is_pin',
        'is_pin_time',
    ];



    protected $appends = ['Year', 'Month', 'Day', 'Date', 'url'];

    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->file_url);
    }

    public function getYearAttribute()
    {
        return substr($this->created_at, 0, 4);
    }

    public function getMonthAttribute()
    {
        return substr($this->created_at, 5, 2);
    }

    public function getDayAttribute()
    {
        return substr($this->created_at, 8, 2);
    }

    public function getDateAttribute()
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        return $date->setTimezone('Asia/Jakarta')->format('D, d-m-Y H:i');
        // return $this->created_at->format('D, d-m-Y H:i');
    }

    public function Admin()
    {
        return $this->belongsTo(Admin::class, 'admins_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo(Coach::class, 'coaches_id', 'id');
    }

    public function Group()
    {
        return $this->belongsTo(Group::class, 'groups_id', 'id');
    }
}
