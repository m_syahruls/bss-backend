<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coach extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'name',
        'birthday',
        'phone',
        'img_url',
        'img_name',
        'is_active',
        'address',
    ];

    protected $appends = [
        'url'
    ];


    public function getUrlAttribute()
    {
        return config('app.url') . Storage::url($this->img_url);
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function Studies()
    {
        return $this->hasMany(Study::class, 'coaches_id', 'id');
    }

    public function Presences()
    {
        return $this->hasMany(Presence::class, 'coaches_id', 'id');
    }

    public function Schedules()
    {
        return $this->hasMany(Schedule::class, 'coaches_id', 'id');
    }

    public function Report_exams()
    {
        return $this->hasMany(ReportExam::class, 'coaches_id', 'id');
    }

    public function Groups()
    {
        return $this->belongsToMany(Group::class, 'coach_groups', 'coaches_id', 'groups_id');
    }
}
