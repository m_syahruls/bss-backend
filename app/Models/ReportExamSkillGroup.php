<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportExamSkillGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'report_exam_levels_id',
        'title_skill_group',
    ];

    public function Report_exam_level()
    {
        return $this->belongsTo(ReportExamLevel::class, 'report_exam_levels_id', 'id');
    }


    public function Report_exam_skills()
    {
        return $this->hasMany(ReportExamSkill::class, 'report_exam_skill_groups_id', 'id');
    }
}
