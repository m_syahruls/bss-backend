<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class FormatUang
{
    public static function format_uang($angka)
    {
        $hasil =  number_format($angka, 0, ',', '.');
        return $hasil;
    }
}
