<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ConvertBulan
{
    public static function convert_bulan($month)
    {
        if ($month == 'January') {
            $month = 'Januari';
        } else if ($month == 'February') {
            $month = 'Februari';
        } else if ($month == 'March') {
            $month = 'Maret';
        } else if ($month == 'April') {
            $month = 'April';
        } else if ($month == 'May') {
            $month = 'Mei';
        } else if ($month == 'June') {
            $month = 'Juni';
        } else if ($month == 'July') {
            $month = 'Juli';
        } else if ($month == 'August') {
            $month = 'Agustus';
        } else if ($month == 'September') {
            $month = 'September';
        } else if ($month == 'October') {
            $month = 'Oktober';
        } else if ($month == 'November') {
            $month = 'November';
        } else if ($month == 'December') {
            $month = 'Desember';
        }

        return $month;
    }
}
