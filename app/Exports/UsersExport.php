<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\BeforeExport;

class UsersExport implements WithMultipleSheets, WithEvents
{
    use Exportable;

    public function __construct()
    {
    }

    public function sheets(): array
    {
        $sheets[0] = new AdminsSheet();
        $sheets[1] = new CoachesSheet();
        $sheets[2] = new StudentsSheet();

        return $sheets;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Users of BSS Bintaro App');
                $event->writer->getProperties()->setSubject('Summarize from Users of BSS Bintaro App');
                $event->writer->getProperties()->setCreator('BSS Bintaro App');
                $event->writer->getProperties()->setCompany('Synapsis.id');
            },
        ];
    }
}
