<?php

namespace App\Exports;

use App\Models\Coach;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CoachesSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize, WithStyles
{
    private $i = 0;

    public function query()
    {
        return Coach::query();
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Birthday',
            'Phone Number',
            'Address',
            'Is Active',
            // 'Created At',
        ];
    }

    public function map($row): array
    {
        return [
            ++$this->i,
            $row->name,
            $row->birthday,
            $row->phone,
            $row->address,
            $row->is_active ? 'Active' : 'Inactive',
            // $row->created_at,
        ];
    }

    public function title(): string
    {
        return 'Coaches';
    }
}
