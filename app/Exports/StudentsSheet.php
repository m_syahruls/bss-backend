<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StudentsSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize, WithStyles
{
    private $i = 0;

    public function query()
    {
        return Student::query();
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Registration Number',
            'Name',
            'Nick Name',
            'Birthday',
            'Category',
            'Joined At',
            'Mother Name',
            'Mother Phone',
            'Father Name',
            'Father Phone',
            'VA Number',
            'Is Active',
            // 'Created At',
        ];
    }

    public function map($row): array
    {
        return [
            ++$this->i,
            $row->registration_no,
            $row->name,
            $row->nick_name,
            $row->birthday,
            $row->category,
            $row->joined_at,
            $row->mother_name,
            $row->mother_phone,
            $row->father_name,
            $row->father_phone,
            $row->va_number,
            $row->is_active ? 'Active' : 'Inactive',
            // $row->created_at,
        ];
    }

    public function title(): string
    {
        return 'Students';
    }
}
