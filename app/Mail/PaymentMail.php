<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentMail extends Mailable
{
    use Queueable, SerializesModels;
    public $emailpayment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailpayment)
    {

        // $month = strtoupper($emailpayment['month']);

        if ($emailpayment['markdown'] == 'create') {
            $emailpayment['subject'] = '[No Reply] Informasi Tagihan Monthly Fee ' .  $emailpayment['month'] . ' ' .  $emailpayment['year'] . ' ' . $emailpayment['name'];
            $emailpayment['page'] = 'email.emailtemplatepaymentcreate';
        } elseif ($emailpayment['markdown'] == 'paid') {
            $emailpayment['subject'] = '[No Reply] Selamat Pembayaran Monthly Fee ' .  $emailpayment['month'] . ' ' .  $emailpayment['year'] . ' ' . $emailpayment['name'] . ' Telah Kami Terima';
            $emailpayment['page'] = 'email.emailtemplatepaymentpaid';
        }

        $this->emailpayment = $emailpayment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->emailpayment['subject'])->markdown($this->emailpayment['page'], [
            'emailpayment' => $this->emailpayment
        ]);
    }
}
