<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Bssmail extends Mailable
{
    use Queueable, SerializesModels;
    public $resetpassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resetpassword)
    {
        $this->resetpassword = $resetpassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset password ' . $this->resetpassword['username'])->markdown('email.emailtemplate', [
            'resetpassword' => $this->resetpassword
        ]);
    }
}
