<?php

use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsCoach;
use App\Http\Middleware\IsStudent;

use App\Http\Controllers\Admin\AdminAdvertisementController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminCoachController;
use App\Http\Controllers\Admin\AdminCoachGroupController;
use App\Http\Controllers\Admin\AdminFitnesListController;
use App\Http\Controllers\Admin\AdminGroupController;
use App\Http\Controllers\Admin\AdminNotificationController;
use App\Http\Controllers\Admin\AdminPaymentController;
use App\Http\Controllers\Admin\AdminPresenceController;
use App\Http\Controllers\Admin\AdminReportExamController;
use App\Http\Controllers\Admin\AdminReportMonthlyController;
use App\Http\Controllers\Admin\AdminRolePositionController;
use App\Http\Controllers\Admin\AdminRewardController;
use App\Http\Controllers\Admin\AdminScheduleController;
use App\Http\Controllers\Admin\AdminStudentController;
use App\Http\Controllers\Admin\AdminStudyController;
use App\Http\Controllers\Admin\AdminUserActivityController;
use App\Http\Controllers\Admin\GetAllListStudentController;

use App\Http\Controllers\Coach\CoachAdvertisementController;
use App\Http\Controllers\Coach\CoachController;
use App\Http\Controllers\Coach\CoachExamReportController;
use App\Http\Controllers\Coach\CoachFitnesListController;
use App\Http\Controllers\Coach\CoachGroupController;
use App\Http\Controllers\Coach\CoachNotificationController;
use App\Http\Controllers\Coach\CoachPresenceController;
use App\Http\Controllers\Coach\CoachReportMonthlyController;
use App\Http\Controllers\Coach\CoachRolePositionController;
use App\Http\Controllers\Coach\CoachScheduleController;
use App\Http\Controllers\Coach\CoachStudentController;
use App\Http\Controllers\Coach\CoachStudyController;

use App\Http\Controllers\Student\StudentAdvertisementController;
use App\Http\Controllers\Student\StudentController;
use App\Http\Controllers\Student\StudentExamReportController;
use App\Http\Controllers\Student\StudentFitnesListController;
use App\Http\Controllers\Student\StudentNotificationController;
use App\Http\Controllers\Student\StudentPaymentController;
use App\Http\Controllers\Student\StudentPresenceController;
use App\Http\Controllers\Student\StudentReportMonthlyController;
use App\Http\Controllers\Student\StudentRewardController;
use App\Http\Controllers\Student\StudentScheduleController;
use App\Http\Controllers\Student\StudentStudyController;

use App\Http\Controllers\Log\LogController;
use App\Http\Controllers\Version\VersionController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//admin
Route::prefix('admin')->group(function () {
    Route::post('/registeradmin', [AdminController::class, 'register']);
    Route::post('/loginadmin', [AdminController::class, 'login']);
    Route::post('/deleteadmin', [AdminController::class, 'admindelete']);

    //reset password
    Route::post('/resetpass', [AdminController::class, 'sendemailresetpassword']);

    //for manual input admin
    Route::post('/presence/create/manual', [AdminPresenceController::class, 'createpresencemanual']);
    Route::post('/presence/delete/manual', [AdminPresenceController::class, 'deletepresencemanual']);
});





//admin middleware
Route::middleware(['auth:sanctum', IsAdmin::class])->group(function () {
    //admin
    Route::prefix('admin')->group(function () {
        Route::get('/getadmin', [AdminController::class, 'getadmin']);
        Route::post('/updateadmin', [AdminController::class, 'updateadmin']);
        Route::post('/logoutadmin', [AdminController::class, 'logout']);
        Route::post('/updatestatusadmin', [AdminController::class, 'updatestatusadmin']);

        //notification
        Route::get('/notification/get', [AdminNotificationController::class, 'getNotification']);
        Route::post('/notification/create', [AdminNotificationController::class, 'createAnnouncment']);

        //password
        Route::post('/update/password', [AdminController::class, 'newpassword']);
        Route::post('/reset/password', [AdminController::class, 'resetpassword']);

        //reward
        Route::get('/reward/get', [AdminRewardController::class, 'getreward']);
        Route::post('/reward/create', [AdminRewardController::class, 'createreward']);
        Route::post('/reward/update', [AdminRewardController::class, 'updatereward']);
        Route::post('/reward/delete', [AdminRewardController::class, 'deletereward']);

        Route::get('/reward/receiver/get', [AdminRewardController::class, 'getrewardreceiver']);
        Route::post('/reward/receiver/create', [AdminRewardController::class, 'createrewardreceiver']);
        Route::post('/reward/receiver/approve', [AdminRewardController::class, 'approverewardreceiver']);
        Route::post('/reward/receiver/delete', [AdminRewardController::class, 'deleterewardreceiver']);

        //coach
        Route::get('/coach/get', [AdminCoachController::class, 'getcoach']);
        Route::post('/coach/register', [AdminCoachController::class, 'registercoach']);
        Route::post('/coach/update', [AdminCoachController::class, 'updatecoach']);
        Route::post('/coach/update/status', [AdminCoachController::class, 'updatestatuscoach']);

        //student
        Route::get('/student/get', [AdminStudentController::class, 'getstudent']);
        Route::post('/student/register', [AdminStudentController::class, 'registerstudent']);
        Route::post('/student/update', [AdminStudentController::class, 'updatestudent']);
        Route::post('/student/update/status', [AdminStudentController::class, 'updatestatusstudent']);
        Route::post('/student/delete', [AdminStudentController::class, 'deletestudent']);

        //getallstudent
        Route::get('/all/student/get', [GetAllListStudentController::class, 'getallstudentreport']);

        //payment
        Route::get('/payment/get', [AdminPaymentController::class, 'getpayment']);
        Route::post('/payment/create', [AdminPaymentController::class, 'createpayment']);
        Route::post('/payment/create/all', [AdminPaymentController::class, 'createallpayment']);
        Route::post('/payment/update', [AdminPaymentController::class, 'updatepayment']);
        Route::post('/payment/notif/email', [AdminPaymentController::class, 'notifpaymentemail']);

        //group
        Route::get('/group/get', [AdminGroupController::class, 'getgroup']);
        Route::post('/group/create', [AdminGroupController::class, 'creategroup']);
        Route::post('/group/update', [AdminGroupController::class, 'updategroup']);
        Route::post('/group/delete', [AdminGroupController::class, 'deletegroup']);

        //schedulecategory
        Route::get('/schedule/category/get', [AdminScheduleController::class, 'getschedulecategory']);
        Route::post('/schedule/category/create', [AdminScheduleController::class, 'createschedulecategory']);
        Route::post('/schedule/category/update', [AdminScheduleController::class, 'updateschedulecategory']);
        Route::post('/schedule/category/delete', [AdminScheduleController::class, 'deleteschedulecategory']);

        //schedule
        Route::get('/schedule/get', [AdminScheduleController::class, 'getschedule']);
        Route::get('/schedule/get/7days', [AdminScheduleController::class, 'getscheduleafter7days']);
        Route::post('/schedule/create', [AdminScheduleController::class, 'createschedule']);
        Route::post('/schedule/update', [AdminScheduleController::class, 'updateschedule']);

        //study
        Route::get('/study/get', [AdminStudyController::class, 'getstudy']);
        Route::post('/study/create', [AdminStudyController::class, 'createstudy']);
        Route::post('/study/update', [AdminStudyController::class, 'updatestudy']);
        Route::post('/study/delete', [AdminStudyController::class, 'deletestudy']);

        Route::get('/study/pin/get', [AdminStudyController::class, 'pingetstudy']);
        Route::post('/study/pin/toggle', [AdminStudyController::class, 'pinaddstudy']);
        Route::post('/study/pin/remove', [AdminStudyController::class, 'pinremovestudy']);

        //coachgroup
        Route::post('/coachgroup/create', [AdminCoachGroupController::class, 'createcoachgroup']);
        Route::post('/coachgroup/update', [AdminCoachGroupController::class, 'updatecoachgroup']);

        //presence
        Route::get('/presence/get', [AdminPresenceController::class, 'getpresence']);
        Route::post('/presence/create', [AdminPresenceController::class, 'createpresence']);
        Route::post('/presence/update', [AdminPresenceController::class, 'updatepresence']);

        //roleposition
        Route::get('/roleposition/get', [AdminRolePositionController::class, 'getroleposition']);
        Route::post('/roleposition/create', [AdminRolePositionController::class, 'createroleposition']);
        Route::post('/roleposition/update', [AdminRolePositionController::class, 'updateroleposition']);
        Route::post('/roleposition/delete', [AdminRolePositionController::class, 'deleteroleposition']);

        //reportmonthly
        Route::get('/report/monthly/get', [AdminReportMonthlyController::class, 'getreportmonthly']);
        Route::post('/report/monthly/create', [AdminReportMonthlyController::class, 'createreportmonthly']);
        Route::post('/report/monthly/update', [AdminReportMonthlyController::class, 'updatereportmonthly']);

        //reportexam
        Route::get('/report/exam/template/get', [AdminReportExamController::class, 'getexamtemplate']);
        Route::post('/report/exam/template/create', [AdminReportExamController::class, 'createexamtemplate']);
        Route::post('/report/exam/template/update', [AdminReportExamController::class, 'updateexamtemplate']);
        Route::post('/report/exam/template/delete', [AdminReportExamController::class, 'deleteexamtemplate']);

        Route::get('/report/exam/report/get', [AdminReportExamController::class, 'getexamreport']);
        Route::post('/report/exam/report/create', [AdminReportExamController::class, 'createexamreport']);
        Route::post('/report/exam/report/delete', [AdminReportExamController::class, 'deleteexamreport']);

        //fitneslist
        Route::get('/fitneslist/get', [AdminFitnesListController::class, 'getfitneslist']);

        // advertisement
        Route::get('/advertisement/get', [AdminAdvertisementController::class, 'getAdvertisement']);
        Route::post('/advertisement/create', [AdminAdvertisementController::class, 'createAdvertisement']);
        Route::post('/advertisement/update', [AdminAdvertisementController::class, 'updateAdvertisement']);
        Route::post('/advertisement/delete', [AdminAdvertisementController::class, 'deleteAdvertisement']);

        // user activities
        Route::get('/activity/get', [AdminUserActivityController::class, 'getActivity']);
    });
});




//student
Route::prefix('student')->group(function () {
    Route::post('/loginstudent', [StudentController::class, 'loginstudent']);
    Route::post('/deletestudent', [StudentController::class, 'studentdelete']);

    //reset password
    Route::post('/resetpass', [StudentController::class, 'sendemailresetpassword']);
});
//student middleware
Route::middleware(['auth:sanctum', IsStudent::class])->group(function () {

    Route::prefix('student')->group(function () {
        Route::get('/profile/get', [StudentController::class, 'profilestudent']);
        Route::post('/logoutstudent', [StudentController::class, 'logoutstudent']);

        //notification
        Route::get('/notification/get', [StudentNotificationController::class, 'getNotification']);
        Route::get('/notification/unread', [StudentNotificationController::class, 'getUnreadNotification']);

        //password
        Route::post('/update/password', [StudentController::class, 'newpassword']);
        Route::post('/reset/password', [StudentController::class, 'resetpassword']);

        //reward
        Route::get('/reward/get', [StudentRewardController::class, 'getreward']);
        Route::post('/reward/claim', [StudentRewardController::class, 'claimreward']);


        //payment
        Route::get('/payment/get', [StudentPaymentController::class, 'getpayment']);

        //study
        Route::get('/study/get', [StudentStudyController::class, 'getstudy']);
        Route::post('/study/pin/toggle', [StudentStudyController::class, 'pinaddstudy']);
        Route::post('/study/pin/remove', [StudentStudyController::class, 'pinremovestudy']);
        Route::get('/study/pin/get', [StudentStudyController::class, 'pingetstudy']);

        //presence
        Route::get('/presence/get', [StudentPresenceController::class, 'getpresence']);

        //schedule
        Route::get('/schedule/get', [StudentScheduleController::class, 'getschedule']);

        //reportmonthly
        Route::get('/report/monthly/get', [StudentReportMonthlyController::class, 'getreportmonthly']);

        //reportexam
        Route::get('/report/exam/get', [StudentExamReportController::class, 'getreportexam']);

        //fitnes list
        Route::post('/fitneslist/create', [StudentFitnesListController::class, 'createfitneslist']);
        Route::get('/fitneslist/get', [StudentFitnesListController::class, 'getfitneslist']);

        // advertisement
        Route::get('/advertisement/get', [StudentAdvertisementController::class, 'getAdvertisement']);
    });
});



//coach
Route::prefix('coach')->group(function () {
    Route::post('/logincoach', [CoachController::class, 'logincoach']);
    Route::post('/deletecoach', [CoachController::class, 'coachdelete']);

    //reset password
    Route::post('/resetpass', [CoachController::class, 'sendemailresetpassword']);
});
//coach middleware
Route::middleware(['auth:sanctum', IsCoach::class])->group(function () {

    Route::prefix('coach')->group(function () {
        Route::get('/profile/get', [CoachController::class, 'profilecoach']);
        Route::post('/logoutcoach', [CoachController::class, 'logoutcoach']);

        //notification
        Route::get('/notification/get', [CoachNotificationController::class, 'getNotification']);
        Route::get('/notification/unread', [CoachNotificationController::class, 'getUnreadNotification']);

        //password
        Route::post('/update/password', [CoachController::class, 'newpassword']);
        Route::post('/reset/password', [CoachController::class, 'resetpassword']);

        //study
        Route::post('/study/create', [CoachStudyController::class, 'createstudy']);
        Route::post('/study/update', [CoachStudyController::class, 'updatestudy']);
        Route::get('/study/get', [CoachStudyController::class, 'getstudy']);
        Route::post('/study/pin/toggle', [CoachStudyController::class, 'pinaddstudy']);
        Route::post('/study/pin/remove', [CoachStudyController::class, 'pinremovestudy']);
        Route::get('/study/pin/get', [CoachStudyController::class, 'pingetstudy']);

        //presence
        Route::post('/presence/create', [CoachPresenceController::class, 'createpresence']);
        Route::post('/presence/update', [CoachPresenceController::class, 'updatepresence']);
        Route::get('/presence/get', [CoachPresenceController::class, 'getpresence']);

        //group
        Route::get('/group/get', [CoachGroupController::class, 'getgroup']);

        //schedule
        Route::get('/schedule/get', [CoachScheduleController::class, 'getschedule']);

        //student
        // Route::get('/getstudentcoach', [CoachStudentController::class, 'getstudent']);

        //reportmonthly
        Route::post('/report/monthly/create', [CoachReportMonthlyController::class, 'createreportmonthly']);
        Route::post('/report/monthly/update', [CoachReportMonthlyController::class, 'updatereportmonhtly']);
        Route::get('/report/monthly/get', [CoachReportMonthlyController::class, 'getreportmonthly']);

        //reportexam
        Route::get('/report/exam/template/get', [CoachExamReportController::class, 'getexamtemplate']);
        Route::post('/report/exam/report/create', [CoachExamReportController::class, 'createexamreport']);
        Route::get('/report/exam/report/get', [CoachExamReportController::class, 'getexamreport']);
        Route::post('/report/exam/report/delete', [CoachExamReportController::class, 'deleteexamreport']);

        //student
        Route::get('/all/student/get', [CoachStudentController::class, 'getallstudentreport']);

        //roleposition
        Route::get('/roleposition/get', [CoachRolePositionController::class, 'getroleposition']);

        //fitnes list
        Route::get('/fitneslist/get', [CoachFitnesListController::class, 'getfitneslist']);

        // advertisement
        Route::get('/advertisement/get', [CoachAdvertisementController::class, 'getAdvertisement']);
    });
});

// get app versions
Route::get('/version', [VersionController::class, 'getVersion']);

// logs
Route::get('/logs/get', [LogController::class, 'getLog']);
Route::post('/logs/create', [LogController::class, 'createLog']);
