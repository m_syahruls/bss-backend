<?php

use App\Mail\Bssmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AuthManualAdmin;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\Database\DatabaseController;
use App\Http\Controllers\Manual\ManualAdminController;
use App\Http\Controllers\Manual\ManualPresenceController;
use App\Http\Controllers\Manual\ManualScheduleController;
use App\Http\Controllers\Manual\ManualAuthAdminController;
use App\Http\Controllers\Password\ResetpasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/* A route to the database controller. */
Route::get('/database/migrate/fresh', [DatabaseController::class, 'migratefresh']);
Route::get('/database/migrate', [DatabaseController::class, 'migrate']);
Route::get('/database/seeder', [DatabaseController::class, 'seeder']);
Route::get('/database/config', [DatabaseController::class, 'config']);
Route::get('/database/cache', [DatabaseController::class, 'cache']);
Route::get('/database/clearall', [DatabaseController::class, 'clearall']);
Route::get('/database/route', [DatabaseController::class, 'route']);
Route::get('/database/createlink', [DatabaseController::class, 'createlink']);
Route::get('/database/composerdump', [DatabaseController::class, 'composerdump']);

/* A route to reset password. */
Route::get('/reset/password/{email}', [ResetpasswordController::class, 'reset'])->name('reset');
Route::post('/reset/store', [ResetpasswordController::class, 'resetstore'])->name('reset.store');
Route::get('/reset/success', [ResetpasswordController::class, 'success'])->name('success');

/* Exporting the users table to an excel file. */
Route::get('/export/users', [ExportController::class, 'download']);


/* A route to manual page for admin */
Route::get('/manual/admin/login', [ManualAuthAdminController::class, 'login'])->name('login.authmanual');
Route::post('/manual/admin/actionlogin', [ManualAuthAdminController::class, 'actionlogin'])->name('actionlogin.authmanual');



Route::middleware([AuthManualAdmin::class])->group(function () {
    //main
    Route::get('/manual/admin', [ManualAdminController::class, 'mainpage'])->name('mainpage');
    Route::get('/manual/admin/getstudent', [ManualAdminController::class, 'getstudent'])->name('getstudent');
    Route::get('/manual/admin/getcoach', [ManualAdminController::class, 'getcoach'])->name('getcoach');
    Route::get('/manual/admin/getgroup', [ManualAdminController::class, 'getgroup'])->name('getgroup');
    // Route::get('/manual/admin/getlogdata', [ManualAdminController::class, 'getlogdata'])->name('getlogdata');

    //presence
    Route::get('/manual/admin/schedule', [ManualScheduleController::class, 'schedulepage'])->name('schedulepage');
    Route::get('/manual/admin/presence', [ManualPresenceController::class, 'presencepage'])->name('presencepage');
    Route::get('/manual/admin/presence/schedule/{id}', [ManualPresenceController::class, 'getschedule'])->name('schedule.get');
    Route::get('/manual/admin/presence/student/{id}', [ManualPresenceController::class, 'getstudent'])->name('student.get');
    Route::get('/manual/admin/presence/coach/{id}', [ManualPresenceController::class, 'getcoach'])->name('coach.get');

    Route::post('/manual/admin/schedule/store', [ManualScheduleController::class, 'insertschedulemanual'])->name('schedule.store');
    Route::post('/manual/admin/presence/store', [ManualPresenceController::class, 'insertpresencemanual'])->name('presence.store');

    Route::get('/manual/admin/logout', [ManualAuthAdminController::class, 'logout'])->name('logout.authmanual');
});
