<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    <h3 style="text-align: center; margin-top:20px;">See our website for more information about BSS
                        Indonesia and BSS Bintaro.
                        <a href="www.bssindonesia.co.id">www.bssindonesia.co.id</a>
                    </h3>
                    <div class="social-images-container">
                        <a href="https://tinyurl.com/yckj52zd"><img src="https://assets.synapsis.id/projects/bss/img/wa.png" width="50px" class="wa" alt="Whatssapp"></a>
                        <a href="mailto:bintaro@bssindonesia.co.id"> <img src="https://assets.synapsis.id/projects/bss/img/mail.png" width="50px" class="logo" alt="Email"></a>
                        <a href="https://www.facebook.com/braziliansoccerschoolsbintaro/"><img src="https://assets.synapsis.id/projects/bss/img/fb.png" width="50px" class="logo" alt="Facebook"></a>
                        <a href="https://www.instagram.com/bssbintaro/"> <img src="https://assets.synapsis.id/projects/bss/img/ig.png" width="50px" class="logo" alt="Instagram"></a>
                    </div>


                    <h3 style="text-align: left; margin-top:30px;">CONFIDENTIALITY</h3>
                    <h4 style="text-align: left">
                        The information in this e-mail and any attachments is confidential and intended only for the
                        named recipient(s). This e-mail may not be disclosed or used by any person other than the
                        addressee, nor may it be copied in any way. If you are not the recipient(s) of this e-mail,
                        please notify BSS Bintaro immediately and delete any copies of this message and contact us at
                        bintaro@bssindonesia.co.id
                    </h4>
                    <a href="https://tinyurl.com/2p9bbmjb"><img src="https://assets.synapsis.id/projects/bss/img/wa.png" width="50px" class="bssback" alt="Whatssapp"></a>
                    <h1 style="text-align: center; margin-top:30px;">Download BSS Bintaro App now!</h1>
                    <div class="download-images-container">
                        <a href="https://play.google.com/store/apps/details?id=com.synapsisid.bss_student"><img class="playstore" src="https://assets.synapsis.id/projects/bss/img/play.png" width="100px" alt="playstore"></a>
                        <a href="https://apps.apple.com/id/app/bss-bintaro/id1614410288"><img class="appstore" src="https://assets.synapsis.id/projects/bss/img/apple.png" width="100px" alt="appstore"></a>
                    </div>

                </td>
            </tr>
        </table>
    </td>
</tr>

<style>
    .social-images-container {
        display: flex;
        margin-left: auto;
        margin-right: auto;
        align-content: center;
        width: 70%;
    }

    .social-images-container img {
        margin-left: 25px;
        margin-right: 25px;
        height: 40px;
        width: 40px;
    }

    .download-images-container {
        display: flex;
        margin-left: auto;
        margin-right: auto;
    }

    .playstore {
        width: 600px;
    }

    .appstore {
        width: 550px;
        margin-top: 10px;
    }

    .wa {
        height: 80px;
        max-height: 80px;
        width: 80px;
    }
</style>