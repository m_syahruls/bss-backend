@component('mail::message')

Dear Bapak/ibu {{ $emailpayment['parent_name'] }} orangtua dari {{ $emailpayment['name'] }},

Berikut kami informasikan jumlah tagihan Monthly Fee bulan <strong style="text-transform:capitalize;">{{ $emailpayment['month'] }}</strong>
untuk <strong>{{ $emailpayment['name'] }}</strong> kelas {{ $emailpayment['group'] }} Brazilian Soccer Schools Bintaro
sejumlah <strong>IDR {{ $emailpayment['total'] }}</strong> dengan periode pembayaran jatuh
tempo paling lambat tanggal 15 {{ $emailpayment['month'] }} {{ $emailpayment['year'] }}.

Transaksi dapat dilakukan via transfer ke rekening <strong>Bank Maybank 2145 263656</strong> (Kode bank 016)
<strong>a.n. PT. Brasilindo Soccer</strong> dan mohon untuk mengirimkan bukti transaksi ke no. 082 1551 98 171
(WA BSS Bintaro).

<strong>Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses
    pada aplikasi BSS Bintaro.</strong>

Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran Monthly Fee melalui
aplikasi <strong>BSS Bintaro Student</strong> pada menu bagian <strong>Payment</strong> atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student <strong>(untuk Android)</strong>.

atau

https://apps.apple.com/id/app/bss-bintaro/id1614410288 <strong>(untuk IOS)</strong>

Atas perhatian dan kepercayaan anda kepada Brazilian Soccer Schools Bintaro, kami ucapkan
terima kasih.


<strong>Best Regards,</strong>

<strong>Brazilian Soccer Schools Bintaro</strong>

@endcomponent