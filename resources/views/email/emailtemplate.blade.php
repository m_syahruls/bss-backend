@component('mail::message')

@if ($resetpassword['role'] == 'student')
Dear Bapak/ibu {{$resetpassword['parent']}} , parent of {{$resetpassword['namestudent']}}
@elseif ($resetpassword['role'] == 'coach')
Dear Bapak/ibu {{$resetpassword['namestudent']}}
@elseif ($resetpassword['role'] == 'admin')
Dear Bapak/ibu {{$resetpassword['namestudent']}}
@endif

We have sent you this email in response to your request to reset your password on BSS bintaro app.

To reset your password , click the link below.

@component('mail::button', ['url' => route('reset' , ['email' => Crypt::encrypt($resetpassword['email']) ])])
Click here to reset your password
@endcomponent

We recommend you to keep your password safe and not to share it with
anyone. If you feel that you are not the person who made this request. please
contact us immediately at +6282155198171 (available on call/WA).

-BSS Bintaro-
@endcomponent