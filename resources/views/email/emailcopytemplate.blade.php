<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>


    <tr>
        <td class="header" style="background-color: #EDF2F7;">
            <img src="https://bss-api.anosign.com/logo/bss.png" style="width: 300px;" alt="BSS logo">
            </a>
        </td>
    </tr>

    <tr>
        <table>
            <td>
                <div class="justify">
                    <p style="font-size: 15px; ">Dear Bapak/ibu {{ $emailpayment['parent_name'] }} orangtua dari {{ $emailpayment['name'] }},</p>

                    <p style="font-size: 15px;">Berikut kami informasikan jumlah tagihan Monthly Fee bulan <strong style="text-transform:capitalize;">{{ $emailpayment['month'] }}</strong> untuk <strong>{{ $emailpayment['name'] }}<strong> kelas {{ $emailpayment['group'] }} Brazilian Soccer Schools Bintaro sejumlah <strong>IDR {{ $emailpayment['total'] }}</strong> dengan periode pembayaran jatuh tempo paling lambat tanggal 15 {{ $emailpayment['month'] }} {{ $emailpayment['year'] }}.</p>

                    <p style="font-size: 15px;">Transaksi dapat dilakukan via transfer ke rekening <strong>Bank Maybank 2145 263656</strong>(Kode bank 016) <strong>a.n. PT. Brasilindo Soccer</strong> dan mohon untuk mengirimkan bukti transaksi ke no. 082 1551 98 171 (WA BSS Bintaro).
                    </p>

                    <p style="font-size: 15px;"><strong>Keterlambatan pembayaran yang melebihi tanggal 15 akan menyebabkan tertutupnya akses pada aplikasi BSS Bintaro.</strong></p>

                    <p style="font-size: 15px;">Untuk informasi lebih lanjut, anda dapat melihat panduan cara pembayaran Monthly Fee melalui aplikasi <strong>BSS Bintaro Student</strong> pada menu bagian <strong>Payment</strong> atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student <strong>(untuk Android)</strong>.</p>

                    <p style="font-size: 15px;">atau</p>

                    <p style="font-size: 15px;">https://apps.apple.com/id/app/bss-bintaro/id1614410288 <strong>(untuk IOS)</strong></p>

                    <p style="font-size: 15px;">Atas perhatian dan kepercayaan anda kepada Brazilian Soccer Schools Bintaro, kami ucapkan terima kasih.</p>

                    <p style="font-size: 15px;"><strong>Best Regards,</strong></p>
                    <p style="font-size: 15px;"><strong>Brazilian Soccer Schools Bintaro</strong></p>


                </div>
            </td>
        </table>
    </tr>

    <tr>
        <td style="background-color: #EDF2F7;">
            <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td class="content-cell" align="center">
                        <h3 style="text-align: center; margin-top:20px;">See our website for more information about BSS
                            Indonesia and BSS Bintaro.
                            <a href="www.bssindonesia.co.id">www.bssindonesia.co.id</a>
                        </h3>
                        <div class="social-images-container">
                            <a href="https://tinyurl.com/yckj52zd"><img src="https://bss-api.anosign.com/logo/wanew.png" class="wa" alt="Whatssapp"></a>
                            <a href="mailto:bintaro@bssindonesia.co.id"> <img src="https://bss-api.anosign.com/logo/mail.png" class="logo" alt="Email"></a>
                            <a href="https://www.facebook.com/braziliansoccerschoolsbintaro/"><img src="https://asset.brandfetch.io/idpKX136kp/iddsfgq1tB.png" class="logo" alt="Facebook"></a>
                            <a href="https://www.instagram.com/bssbintaro/"> <img src="https://asset.brandfetch.io/ido5G85nya/idzq93VyOG.png" class="logo" alt="Instagram"></a>
                        </div>


                        <h3 style="text-align: left; margin-top:30px;">CONFIDENTIALITY</h3>
                        <h4 style="text-align: left">
                            The information in this e-mail and any attachments is confidential and intended only for the
                            named recipient(s). This e-mail may not be disclosed or used by any person other than the
                            addressee, nor may it be copied in any way. If you are not the recipient(s) of this e-mail,
                            please notify BSS Bintaro immediately and delete any copies of this message and contact us at
                            bintaro@bssindonesia.co.id
                        </h4>
                        <a href="https://tinyurl.com/2p9bbmjb"><img src="https://bss-api.anosign.com/logo/bssback.png" class="bssback" alt="Whatssapp"></a>
                        <h1 style="text-align: center; margin-top:30px;">Download BSS Bintaro App now!</h1>
                        <div class="download-images-container">
                            <a href="https://play.google.com/store/apps/details?id=com.synapsisid.bss_student"><img class="playstore" src="https://bss-api.anosign.com/logo/play.png" alt="playstore"></a>
                            <a href="https://apps.apple.com/id/app/bss-bintaro/id1614410288"><img class="appstore" src="https://bss-api.anosign.com/logo/appstore.png" alt="appstore"></a>
                        </div>

                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <style>
        .justify p {
            text-align: justify;
        }

        .social-images-container {
            display: flex;
            margin-left: auto;
            margin-right: auto;
            align-content: center;
            width: 70%;
        }

        .social-images-container img {
            margin-left: 25px;
            margin-right: 25px;
            height: 40px;
            width: 40px;
        }

        .download-images-container {
            display: flex;
            margin-left: auto;
            margin-right: auto;
        }

        .playstore {
            width: 600px;
        }

        .appstore {
            width: 550px;
            margin-top: 10px;
        }

        .wa {
            height: 80px;
            max-height: 80px;
            width: 80px;
        }
    </style>

</body>

</html>