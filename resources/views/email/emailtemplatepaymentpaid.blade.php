@component('mail::message')

Dear Bapak/ibu {{ $emailpayment['parent_name'] }} orangtua dari {{ $emailpayment['name'] }},

Kami informasikan bahwa pembayaran tagihan Monthly Fee bulan <strong style="text-transform:capitalize;">{{ $emailpayment['month'] }}</strong>
atas nama <strong>{{ $emailpayment['name'] }}</strong> kelas {{ $emailpayment['group'] }} Brazilian Soccer Schools Bintaro
sejumlah <strong>IDR {{ $emailpayment['total'] }} telah kami terima</strong> pada
<strong>{{ $emailpayment['date_payment_received'] }}.</strong>

Untuk informasi lebih lanjut, anda dapat melihat di dalam
aplikasi <strong>BSS Bintaro Student</strong> pada menu bagian <strong>Payment</strong> atau klik https://play.google.com/store/apps/details?id=com.synapsisid.bss_student <strong>(untuk Android)</strong>.

atau

https://apps.apple.com/id/app/bss-bintaro/id1614410288 <strong>(untuk IOS)</strong>

Atas perhatian dan kepercayaan anda kepada Brazilian Soccer Schools Bintaro, kami ucapkan
terima kasih.

<strong>Best Regards,</strong>

<strong>Brazilian Soccer Schools Bintaro</strong>

@endcomponent