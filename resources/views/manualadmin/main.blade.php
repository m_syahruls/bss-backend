<!doctype html>
<html lang="en">

<head>
    <title>Main</title>
    @include('manualadmin/template/base')
</head>

<body>
    @include('manualadmin/template/header')

    <section class="menu mt-5">
        <div class="container">
            <div class="row">
                <div class="col col-menu">
                    <div class="card card-menu student-menu">
                        <div class="card-body">
                            <h5 class="card-title judul-menu-student">Student</h5>
                            <h1 id="student_count" class="angka-menu-student"></h1>
                            <a href="#" class="btn btn-primary tombol-menu-student disabled">Manage (not yet functional)</a>
                        </div>
                    </div>
                </div>
                <div class="col col-menu">
                    <div class="card card-menu coach-menu">
                        <div class="card-body">
                            <h5 class="card-title judul-menu-coach">Coach</h5>
                            <h1 id="coach_count" class="angka-menu-coach"></h1>
                            <a href="#" class="btn btn-primary tombol-menu-coach disabled">Manage (not yet functional)</a>
                        </div>
                    </div>
                </div>
                <div class="col col-menu">
                    <div class="card card-menu class-menu">
                        <div class="card-body">
                            <h5 class="card-title judul-menu-class">Class</h5>
                            <h1 id="group_count" class="angka-menu-class"></h1>
                            <a href="#" class="btn btn-primary tombol-menu-class disabled">Manage (not yet functional)</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="report mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <a href="{{route('presencepage')}}" class="btn btn-primary card-function">
                                        <p class="button-text-function">Insert Presence</p>
                                    </a>
                                    <a href="{{route('schedulepage')}}" class="btn btn-primary card-function">
                                        <p class="button-text-function">Insert Event</p>
                                    </a>
                                </div>
                                <!-- <div class="row">
                                    <a href="#" class="btn btn-primary card-function">
                                        <p class="button-text-function">Insert Presence</p>
                                    </a>
                                </div>
                                <div class="row">
                                    <a href="#" class="btn btn-primary card-function">
                                        <p class="button-text-function">Insert Presence</p>
                                    </a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <table class="table" id="logtable">
                                <thead>
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Log</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($logdata as $log)
                                    <tr>
                                        <td>{{$log->created_at}}</td>
                                        <td>{{$log->log}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- {!! $logdata->links() !!} -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        @include('manualadmin/template/basefoot')
    </footer>
</body>

</html>