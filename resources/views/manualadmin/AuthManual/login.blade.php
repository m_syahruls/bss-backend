<!doctype html>
<html lang="en">

<head>
    <title>Login</title>
    @include('manualadmin/template/base')
</head>

<body>
    <section class="login section-login">
        <div class="container">
            <div class="card card-login login-text">
                <div class="card-body">
                    @if(session('error'))
                    <div class="alert alert-danger">
                        <b>Opps!</b> {{session('error')}}
                    </div>
                    @endif
                    <form action="{{ route('actionlogin.authmanual') }}" method="post">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" name="username" class="form-control" id="username" aria-describedby="emailHelp" required>
                        </div>
                        <div class="mb-3">
                            <label for="key" class="form-label">Key</label>
                            <input type="text" name="key" class="form-control" id="key" required>
                        </div>
                        <button type="submit" class="btn btn-primary tombol-login">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer">
        @include('manualadmin/template/basefoot')
    </footer>
</body>

</html>