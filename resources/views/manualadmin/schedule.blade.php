<!doctype html>
<html lang="en">

<head>
    <title>Schedule</title>
    @include('manualadmin/template/base')
</head>

<body>
    @include('manualadmin/template/header')

    <section class="presence mt-5">
        <div class="container">
            <div class="row">
                <div>
                    <div class="card card-presence mb-4">
                        <div class="card-body text-presence">
                            <form action="{{route('schedule.store')}}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <select id="select_group" name="selectgroup" class="form-select" aria-label="Default select example">
                                    <option value="0">Select Class</option>
                                    @foreach ($datagroup as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                    @endforeach
                                </select>
                                <div class="mb-3 mt-4">
                                    <label for="temperature" class="form-label">Title</label>
                                    <input name="title" type="text" class="form-control" id="title" required>
                                </div>
                                <div class="mb-3">
                                    <label for="attended" class="form-label">Description</label>
                                    <textarea name="desc"class="form-control" id="desc" required></textarea>
                                </div>
                                 <div class="mb-3">
                                    <label for="date" class="form-label">Date</label>
                                    <input name="date" type="date" class="form-control" id="date" required>
                                </div>
                                <div class="mb-3">
                                    <label for="time" class="form-label">Time</label>
                                    <input name="time" type="time" class="form-control" id="time" required>
                                </div>
                                <div class="mb-3">
                                    <label for="location" class="form-label">Location</label>
                                    <input name="location" type="text" class="form-control" id="location" required>
                                </div>
                                <div class="mb-3">
                                    <label for="coordinate" class="form-label">Coordinate</label>
                                    <input name="coordinate" type="text" class="form-control" id="coordinate" required>
                                </div>
                                <div class="mb-3 mt-4">
                                    <label for="temperature" class="form-label">Schedule Catgory</label>
                                 <select id="schedule_categories_id" name="schedule_categories_id" class="form-select" aria-label="Default select example">
                                    <option value="0">Select Schedule Category</option>
                                    @foreach ($dataschedulecategory as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
</div>
                                <button type="submit" class="btn btn-primary tombol-presence">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer">
        @include('manualadmin/template/basefoot')
    </footer>
</body>

</html>