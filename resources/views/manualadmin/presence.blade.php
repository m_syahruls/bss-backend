<!doctype html>
<html lang="en">

<head>
    <title>Presence</title>
    @include('manualadmin/template/base')
</head>

<body>
    @include('manualadmin/template/header')

    <section class="presence mt-5">
        <div class="container">
            <div class="row">
                <div>
                    <div class="card card-presence mb-4">
                        <div class="card-body text-presence">
                            <form action="{{route('presence.store')}}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <select id="select_group" name="selectgroup" class="form-select" aria-label="Default select example">
                                    <option value="0">Select Class</option>
                                    @foreach ($datagroup as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                    @endforeach
                                </select>
                                <select id="select_schedule" name="selectschedule" class="form-select mt-4" aria-label="Default select example" required>
                                    <option value="0">Select Schedule</option>
                                </select>
                                <div class="row">
                                    <div class="col form-student">
                                        <select id="select_student" name="selectstudent" class="form-select  mt-4" aria-label="Default select example" required>
                                            <option value="0">Select Student</option>
                                        </select>
                                    </div>
                                    <!-- <div class="col form-student-tombol">
                                        <button disabled type="submit" class="btn btn-primary tombol-student mt-4">Add</button>
                                    </div> -->
                                </div>
                                <div class="mb-3 mt-4">
                                    <label for="temperature" class="form-label">Temperature</label>
                                    <input name="temperature" type="text" class="form-control" id="temperature" required>
                                </div>
                                <div class="mb-3">
                                    <label for="attended" class="form-label">Time attended</label>
                                    <input name="timeattended" type="time" class="form-control" id="attended" required>
                                </div>
                                <div class="mb-3">
                                    <label for="date" class="form-label">Date</label>
                                    <input name="date" type="date" class="form-control" id="date" required>
                                </div>
                                <div class="mt-4">
                                    <p>You can only choose one (admin or coach)</p>
                                </div>
                                <div class="mb-1 form-check">
                                    <input type="checkbox" class="form-check-input" id="admin" onclick="admincheck(this)">
                                    <label class="form-check-label" for="admin">Admin</label>
                                </div>
                                <select disabled id="select-admin" name="select-admin" class="form-select" aria-label="Default select example">
                                    <option selected>Select Admin</option>
                                    @foreach ($dataadmin as $admin)
                                    <option value="{{ $admin->id }}">{{ $admin->name }} ({{ $admin->id }})</option>
                                    @endforeach
                                </select>
                                <div class="mb-1 mt-4 form-check">
                                    <input type="checkbox" class="form-check-input" id="coach" onclick="coachcheck(this)">
                                    <label class="form-check-label" for="coach">Coach</label>
                                </div>
                                <select disabled id="select_coach" name="selectcoach" class="form-select mb-4" aria-label="Default select example">
                                    <option value="0">Select Coach</option>
                                </select>
                                <button type="submit" class="btn btn-primary tombol-presence">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer">
        @include('manualadmin/template/basefoot')
    </footer>
</body>

</html>