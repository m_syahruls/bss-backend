
function admincheck(admin) {
    if (document.getElementById("admin").checked) {
        document.getElementById("select-admin").disabled = false;
        document.getElementById("coach").disabled = true;
    } else {
        document.getElementById("select-admin").disabled = true;
        document.getElementById("coach").disabled = false;
    }
}

function coachcheck(coach) {
    if (document.getElementById("coach").checked) {
        document.getElementById("select_coach").disabled = false;
        document.getElementById("admin").disabled = true;
    } else {
        document.getElementById("select_coach").disabled = true;
        document.getElementById("admin").disabled = false;
    }
}


//get schedule
$(document).ready(function(){


    $('#select_group').change(function(){
    var id = $(this).val();


    $('#select_schedule').find('option').not(':first').remove();


    $.ajax({
        url: '/manual/admin/presence/schedule/'+id,
        type: 'get',
        dataType: 'json',
        success: function(response){

        var len = 0;
        if(response.schedule != null){
            len = response.schedule.length;
        }

        if(len > 0){
            for(var i=0; i<len; i++){

            var id = response.schedule[i].id;
            var title = response.schedule[i].title;
            var date = response.schedule[i].date;

            var option = "<option value='"+id+"'>"+title+ " (" +date+")"+ " (" +id+")</option>"; 

            $("#select_schedule").append(option); 
            
            }
        }

        }
    });
});

});


//get student
$(document).ready(function(){


    $('#select_group').change(function(){
    var id = $(this).val();


    $('#select_student').find('option').not(':first').remove();


    $.ajax({
        url: '/manual/admin/presence/student/'+id,
        type: 'get',
        dataType: 'json',
        success: function(response){

        var len = 0;
        if(response.student != null){
            len = response.student.length;
        }

        if(len > 0){
            for(var i=0; i<len; i++){

            var id = response.student[i].id;
            var name = response.student[i].name;

            var option = "<option value='"+id+"'>"+name+"(" +id+")</option>"; 

            $("#select_student").append(option); 
            
            }
        }

        }
    });
});

});


//get coach
$(document).ready(function(){


    $('#select_group').change(function(){
    var id = $(this).val();


    $('#select_coach').find('option').not(':first').remove();


    $.ajax({
        url: '/manual/admin/presence/coach/'+id,
        type: 'get',
        dataType: 'json',
        success: function(response){

        var len = 0;
        if(response.coach != null){
            len = response.coach.length;
        }

        if(len > 0){
            for(var i=0; i<len; i++){

            var id = response.coach[i].id;
            var name = response.coach[i].name;

            var option = "<option value='"+id+"'>"+name+"(" +id+")</option>"; 

            $("#select_coach").append(option); 
            
            }
        }

        }
    });
});

});






