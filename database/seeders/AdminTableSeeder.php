<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'users_id' => 3,
                'name' => 'Admin',
                'birthday' => '1993-01-01',
                'target' => '01838384848',
                'role' => 'Admin',
                'img_url' => '',
                'is_active' => true,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            // [
            //     'users_id' => 2,
            //     'name' => 'Admin super',
            //     'birthday' => '1992-01-01',
            //     'target' => '01838384848',
            //     'role' => 'SuperAdmin',
            //     'img_url' => '',
            //     'is_active' => true,
            //     'created_at' => date('Y-m-d H:i:s', time()),
            //     'updated_at' => date('Y-m-d H:i:s', time()),
            // ]
        ];

        Admin::insert($admins);
    }
}
