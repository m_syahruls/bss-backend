<?php

namespace Database\Seeders;

use App\Models\ExamSkill;
use Illuminate\Database\Seeder;

class ExamSkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examskill = [
            //dribbling
            [
                'exam_skill_groups_id' => 1,
                'name' => 'lari',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'exam_skill_groups_id' => 1,
                'name' => 'gocek',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //smash
            [
                'exam_skill_groups_id' => 2,
                'name' => 'lompat',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'exam_skill_groups_id' => 2,
                'name' => 'dash',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //smash bultang
            [
                'exam_skill_groups_id' => 3,
                'name' => 'lompat bultang',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'exam_skill_groups_id' => 3,
                'name' => 'dash bultang',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //mantul
            [
                'exam_skill_groups_id' => 4,
                'name' => 'lari mantul',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'exam_skill_groups_id' => 4,
                'name' => 'dash mantul',
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ExamSkill::insert($examskill);
    }
}
