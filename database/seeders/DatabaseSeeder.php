<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            UserTableSeeder::class,
            GroupTableSeeder::class,
            StudentTableSeeder::class,
            // CoachTableSeeder::class,
            // PaymentTableSeeder::class,
            // FitnesListTableSeeder::class,
            // ScheduleCategoryTableSeeder::class,
            // ScheduleTableSeeder::class,
            // PresenceTableSeeder::class,
            // StudyTableSeeder::class,
            AdminTableSeeder::class,
            // CoachGroupTableSeeder::class,
            // RolePositionSeeder::class,
            // ReportExamsTableSeeder::class,
            // ReportExamLevelTableSeeder::class,
            // ReportExamSkillGroupTableSeeder::class,
            // ReportExamSkillTableSeeder::class,
            // ExamLevelTableSeeder::class,
            // ExamSkillGroupTableSeeder::class,
            // ExamSkillTableSeeder::class,
            VersionTableSeeder::class,
        ]);
    }
}
