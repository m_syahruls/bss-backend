<?php

namespace Database\Seeders;

use App\Models\ExamSkillGroup;
use Illuminate\Database\Seeder;

class ExamSkillGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examskillgroup = [
            //bola
            [
                'exam_levels_id' => 1,
                'title_skill_group' => 'dribbling',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //tennis
            [
                'exam_levels_id' => 2,
                'title_skill_group' => 'smash',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //bultang
            [
                'exam_levels_id' => 3,
                'title_skill_group' => 'smash bultang',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //basket
            [
                'exam_levels_id' => 4,
                'title_skill_group' => 'mantul',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ExamSkillGroup::insert($examskillgroup);
    }
}
