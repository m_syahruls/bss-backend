<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' => 'anderson',
                'email' => 'anderson@gmail.com',
                'password' => \bcrypt('passanderson'),
                'status' => 'student',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'username' => 'nijack',
                'email' => 'nickjackn@gmail.com',
                'password' => \bcrypt('passnick'),
                'status' => 'student',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            // [
            //     'username' => 'MR anam',
            //     'email' => 'anam@gmail.com',
            //     'password' => \bcrypt('passnam'),
            //     'status' => 'coach',
            //     'created_at' => date('Y-m-d H:i:s', time()),
            //     'updated_at' => date('Y-m-d H:i:s', time()),
            // ],
            // [
            //     'username' => 'Mr saiful',
            //     'email' => 'saiful@gmail.com',
            //     'password' => \bcrypt('passsaiful'),
            //     'status' => 'coach',
            //     'created_at' => date('Y-m-d H:i:s', time()),
            //     'updated_at' => date('Y-m-d H:i:s', time()),
            // ],
            [
                'username' => 'admin1',
                'email' => 'admin@gmail.com',
                'password' => \bcrypt('passminoke'),
                'status' => 'admin',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            // [
            //     'username' => 'adminsuper',
            //     'email' => 'adminsuper@gmail.com',
            //     'password' => \bcrypt('passsuper'),
            //     'status' => 'admin',
            //     'created_at' => date('Y-m-d H:i:s', time()),
            //     'updated_at' => date('Y-m-d H:i:s', time()),
            // ],
        ];

        User::insert($users);
    }
}
