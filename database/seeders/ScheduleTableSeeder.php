<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedules = [
            [
                'groups_id' => 1,
                'schedule_categories_id' => 2,
                'title' => 'Latihan rutin',
                'desc' => 'Latihan rutin bermain bola',
                'date' => date('Y-m-d', time()),
                'time' => '08:00',
                'location' => 'Stadion GBK',
                'coordinate' => '1.123,1.123',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'groups_id' => 1,
                'schedule_categories_id' => 1,
                'title' => 'Tournament',
                'desc' => 'Toutnament tahunan',
                'date' => date('Y-m-d ', time()),
                'time' => '10:00',
                'location' => 'Stadion GBK',
                'coordinate' => '1.123,1.123',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'groups_id' => 2,
                'schedule_categories_id' => 2,
                'title' => 'Latihan rutin mang',
                'desc' => 'Latihan rutin bermain bola',
                'date' => date('Y-m-d ', time()),
                'time' => '10:00',
                'location' => 'Stadion GBK',
                'coordinate' => '1.123,1.123',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'groups_id' => 2,
                'schedule_categories_id' => 2,
                'title' => 'Sparring',
                'desc' => 'Sparring dengan musuh',
                'date' => date('Y-m-d ', time()),
                'time' => '16:00',
                'location' => 'Stadion GBK',
                'coordinate' => '1.123,1.123',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        Schedule::insert($schedules);
    }
}
