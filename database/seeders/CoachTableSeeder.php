<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Coach;

class CoachTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coaches = [
            [
                'users_id' => 3,
                'name' => 'Mr anam mana',
                'birthday' => '1993-01-01',
                'target' => '01838384848',
                'img_url' => '',
                'is_active' => true,
                'address' => 'Depok',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'users_id' => 4,
                'name' => 'Mr saiful ful',
                'birthday' => '1992-01-01',
                'target' => '01838384848',
                'img_url' => '',
                'is_active' => true,
                'address' => 'Depok',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ]
        ];

        Coach::insert($coaches);
    }
}
