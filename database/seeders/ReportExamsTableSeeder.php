<?php

namespace Database\Seeders;

use App\Models\ReportExam;
use Illuminate\Database\Seeder;

class ReportExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reportexams = [
            [
                'students_id' => 1,
                'coaches_id' => 2,
                'is_passed' => 1,
                'date' => date('Y-m-d H:i:s', time()),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'students_id' => 2,
                'coaches_id' => 1,
                'is_passed' => 0,
                'date' => date('Y-m-d H:i:s', time()),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ReportExam::insert($reportexams);
    }
}
