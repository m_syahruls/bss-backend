<?php

namespace Database\Seeders;

use DB;
use App\Models\CoachGroup;
use Illuminate\Database\Seeder;

class CoachGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coach_group = [
            [
                'coaches_id' => 1,
                'groups_id' => 1,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 2,
                'groups_id' => 1,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 1,
                'groups_id' => 2,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 2,
                'groups_id' => 2,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        CoachGroup::insert($coach_group);
    }
}
