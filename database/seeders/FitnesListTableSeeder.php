<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FitnesList;

class FitnesListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fitnes_lists = [
            [
                'point' => 3,
                'students_id' => 1,
                'answer' => '1 , 1 , 1',
                'is_fit' => true,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'point' => 2,
                'students_id' => 2,
                'answer' => '1 , 0 , 1',
                'is_fit' => false,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ]
        ];

        FitnesList::insert($fitnes_lists);
    }
}
