<?php

namespace Database\Seeders;

use App\Models\ReportExamSkill;
use App\Models\ReportExamSkillGroup;
use Illuminate\Database\Seeder;

class ReportExamSkillGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reportexamskillgroup = [
            //bola
            [
                'report_exam_levels_id' => 1,
                'title_skill_group' => 'dribbling',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //tennis
            [
                'report_exam_levels_id' => 2,
                'title_skill_group' => 'smash',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //bultang
            [
                'report_exam_levels_id' => 3,
                'title_skill_group' => 'smash bultang',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //basket
            [
                'report_exam_levels_id' => 4,
                'title_skill_group' => 'mantul',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ReportExamSkillGroup::insert($reportexamskillgroup);
    }
}
