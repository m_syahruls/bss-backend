<?php

namespace Database\Seeders;

use App\Models\RolePosition;
use Illuminate\Database\Seeder;

class RolePositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_positions = [
            [
                'name' => 'Ronaldo',
                'image_link' => 'wwww',
                'position' => 'kiper',
                'role' => 'kiper',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'name' => 'Messi',
                'image_link' => 'wwww',
                'position' => 'Bek',
                'role' => 'Bek',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        RolePosition::insert($role_positions);
    }
}
