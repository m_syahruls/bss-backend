<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Presence;

class PresenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $presences = [
            [
                'students_id' => 1,
                'schedules_id' => 1,
                'coaches_id' => 2,
                'temperature' => '26,7',
                'time_attended' => '08:00',
                'is_attended' => '1',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'students_id' => 1,
                'schedules_id' => 2,
                'coaches_id' => 2,
                'temperature' => '26,7',
                'time_attended' => '10:00',
                'is_attended' => '1',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'students_id' => 2,
                'schedules_id' => 3,
                'coaches_id' => 1,
                'temperature' => '26,7',
                'time_attended' => '10:00',
                'is_attended' => '1',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'students_id' => 2,
                'schedules_id' => 4,
                'coaches_id' => 1,
                'temperature' => '26,7',
                'time_attended' => '16:00',
                'is_attended' => '1',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        Presence::insert($presences);
    }
}
