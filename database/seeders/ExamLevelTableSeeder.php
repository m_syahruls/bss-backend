<?php

namespace Database\Seeders;

use App\Models\ExamLevel;
use Illuminate\Database\Seeder;

class ExamLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examlevel = [
            [
                'title' => 'main bola',
                'desc' => 'main bola',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main tenis',
                'desc' => 'main tenis',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main bultang',
                'desc' => 'main bultang',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main basket',
                'desc' => 'main basket',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ExamLevel::insert($examlevel);
    }
}
