<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = [
            [
                'users_id' => 1,
                'groups_id' => 1,
                'registration_no' => '12345678',
                'name' => 'Anderson sanches',
                'nick_name' => 'Andersonsan',
                'mother_name' => 'Sunny',
                'mother_phone' => '081315737817',
                'father_name' => 'Passion',
                'father_phone' => '081315737817',
                'birthday' => '2000-01-01',
                'img_url' => '',
                'category' => 'U-22',
                'is_active' => true,
                'va_number' => '',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'users_id' => 2,
                'groups_id' => 2,
                'registration_no' => '22345678',
                'name' => 'Nick jack',
                'nick_name' => 'Nickja',
                'mother_name' => 'Liella',
                'mother_phone' => '081315737817',
                'father_name' => 'brando',
                'father_phone' => '081315737817',
                'birthday' => '2000-01-02',
                'img_url' => '',
                'category' => 'U-22',
                'is_active' => true,
                'va_number' => '',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        Student::insert($students);
    }
}
