<?php

namespace Database\Seeders;

use App\Models\ReportExamLevel;
use Illuminate\Database\Seeder;

class ReportExamLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reportexamlevel = [
            [
                'title' => 'main bola',
                'report_exams_id' => 1,
                'desc' => 'main bola',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main tenis',
                'report_exams_id' => 1,
                'desc' => 'main tenis',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main bultang',
                'report_exams_id' => 2,
                'desc' => 'main bultang',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'title' => 'main basket',
                'report_exams_id' => 2,
                'desc' => 'main basket',
                'level_logo_url' => 'www',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ReportExamLevel::insert($reportexamlevel);
    }
}
