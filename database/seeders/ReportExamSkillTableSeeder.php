<?php

namespace Database\Seeders;

use App\Models\ReportExamSkill;
use Illuminate\Database\Seeder;

class ReportExamSkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reportexamskill = [
            //dribbling
            [
                'report_exam_skill_groups_id' => 1,
                'name' => 'lari',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'report_exam_skill_groups_id' => 1,
                'name' => 'gocek',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //smash
            [
                'report_exam_skill_groups_id' => 2,
                'name' => 'lompat',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'report_exam_skill_groups_id' => 2,
                'name' => 'dash',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //smash bultang
            [
                'report_exam_skill_groups_id' => 3,
                'name' => 'lompat bultang',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'report_exam_skill_groups_id' => 3,
                'name' => 'dash bultang',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            //mantul
            [
                'report_exam_skill_groups_id' => 4,
                'name' => 'lari mantul',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],

            [
                'report_exam_skill_groups_id' => 4,
                'name' => 'dash mantul',
                'score' => 90,
                'min_score' => 0,
                'max_score' => 100,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ReportExamSkill::insert($reportexamskill);
    }
}
