<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payment;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payments = [
            [
                'students_id' => 1,
                'month' => 'January',
                'year' => '2022',
                'total' => 100000,
                'date_payment_received' => '2022-01-01',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'students_id' => 2,
                'month' => 'January',
                'year' => '2022',
                'total' => 100000,
                'date_payment_received' => '',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        Payment::insert($payments);
    }
}
