<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Study;

class StudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studies = [
            [
                'coaches_id' => 1,
                'groups_id' => 1,
                'title' => 'Materi latihan',
                'desc' => 'intinya materi',
                'file_url' => 'www',
                'file_name' => 'www',
                'is_pdf' => false,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 1,
                'groups_id' => 1,
                'title' => 'PDF latihan',
                'desc' => 'intinya materi',
                'file_url' => 'www',
                'file_name' => 'www',
                'is_pdf' => true,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 2,
                'groups_id' => 2,
                'title' => 'PDF passing',
                'desc' => 'intinya materi passing',
                'file_url' => 'www',
                'file_name' => 'www',
                'is_pdf' => true,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'coaches_id' => 2,
                'groups_id' => 2,
                'title' => 'video latihan lari',
                'desc' => 'intinya materi',
                'file_url' => 'www',
                'file_name' => 'www',
                'is_pdf' => false,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        Study::insert($studies);
    }
}
