<?php

namespace Database\Seeders;

use App\Models\ScheduleCategory;
use Illuminate\Database\Seeder;

class ScheduleCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedule_categories = [
            [
                'name' => 'Tournament',
                'color' => 'Red',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'name' => 'Training',
                'color' => 'Blue',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        ScheduleCategory::insert($schedule_categories);
    }
}
