<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportMonthlyRolePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_monthly_role_positions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('role_positions_id')->unsigned();
            $table->bigInteger('report_monthlys_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_positions_id')->references('id')->on('role_positions')->unsigned();
            $table->foreign('report_monthlys_id')->references('id')->on('report_monthlys')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_monthly_role_positions');
    }
}
