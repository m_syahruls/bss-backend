<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamSkillGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_skill_groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('exam_levels_id')->unsigned();
            $table->string('title_skill_group');
            $table->timestamps();

            $table->foreign('exam_levels_id')->references('id')->on('exam_levels')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_skill_groups');
    }
}
