<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportExamSkillGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_exam_skill_groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('report_exam_levels_id')->unsigned();
            $table->string('title_skill_group');
            $table->timestamps();

            $table->foreign('report_exam_levels_id')->references('id')->on('report_exam_levels')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_exam_skill_groups');
    }
}
