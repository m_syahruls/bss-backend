<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coaches_id')->unsigned()->nullable();
            $table->bigInteger('groups_id')->unsigned();
            $table->string('title');
            $table->text('desc');
            $table->string('file_url')->nullable();
            $table->string('file_name');
            $table->boolean('is_pdf')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('coaches_id')->references('id')->on('coaches')->unsigned();
            $table->foreign('groups_id')->references('id')->on('groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studies');
    }
}
