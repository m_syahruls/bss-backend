<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presences', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('students_id')->unsigned();
            $table->bigInteger('schedules_id')->unsigned();
            $table->bigInteger('coaches_id')->unsigned()->nullable();
            $table->string('temperature')->nullable();
            $table->time('time_attended')->nullable();
            $table->boolean('is_attended')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('students_id')->references('id')->on('students')->unsigned();
            $table->foreign('schedules_id')->references('id')->on('schedules')->unsigned();
            $table->foreign('coaches_id')->references('id')->on('coaches')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presences');
    }
}
