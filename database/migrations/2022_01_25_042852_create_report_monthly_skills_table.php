<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportMonthlySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_monthly_skills', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('report_monthlys_id')->unsigned();
            $table->string('title');
            $table->string('category');
            $table->string('grade');
            $table->text('desc');
            $table->string('icon');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('report_monthlys_id')->references('id')->on('report_monthlys')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_monthly_skills');
    }
}
