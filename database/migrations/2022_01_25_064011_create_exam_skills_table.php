<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_skills', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('exam_skill_groups_id')->unsigned();
            $table->string('name');
            $table->integer('min_score');
            $table->integer('max_score');
            $table->timestamps();

            $table->foreign('exam_skill_groups_id')->references('id')->on('exam_skill_groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_skills');
    }
}
