<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('groups_id')->unsigned();
            $table->string('title');
            $table->text('desc');
            $table->date('date')->nullable();
            $table->string('time');
            $table->string('location');
            $table->string('coordinate');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('groups_id')->references('id')->on('groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
