<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminsIdToReportExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_exams', function (Blueprint $table) {
            $table->bigInteger('admins_id')->unsigned()->nullable()->after('coaches_id');
            $table->foreign('admins_id')->references('id')->on('admins')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_exams', function (Blueprint $table) {
            $table->dropForeign('report_exams_admins_id_foreign');
            $table->dropColumn('admins_id');
        });
    }
}
