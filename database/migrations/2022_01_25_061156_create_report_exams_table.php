<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_exams', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('students_id')->unsigned();
            $table->bigInteger('coaches_id')->unsigned()->nullable();
            $table->date('date');
            $table->string('is_passed');
            $table->timestamps();

            $table->foreign('students_id')->references('id')->on('students')->unsigned();
            $table->foreign('coaches_id')->references('id')->on('coaches')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_exams');
    }
}
