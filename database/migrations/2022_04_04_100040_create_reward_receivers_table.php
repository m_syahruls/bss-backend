<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRewardReceiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_receivers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rewards_id')->unsigned();
            $table->bigInteger('students_id')->unsigned();
            $table->string('status')->nullable();
            $table->string('date_claimed')->nullable();
            $table->timestamps();

            $table->foreign('rewards_id')->references('id')->on('rewards')->unsigned();
            $table->foreign('students_id')->references('id')->on('students')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_receivers');
    }
}
