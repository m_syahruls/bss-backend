<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coaches_id')->unsigned();
            $table->bigInteger('groups_id')->unsigned();
            $table->timestamps();

            $table->foreign('coaches_id')->references('id')->on('coaches')->unsigned();
            $table->foreign('groups_id')->references('id')->on('groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_groups');
    }
}
