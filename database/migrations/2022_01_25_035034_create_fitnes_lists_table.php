<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFitnesListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitnes_lists', function (Blueprint $table) {
            $table->id();
            $table->integer('point');
            $table->bigInteger('students_id')->unsigned();
            $table->string('answer');
            $table->boolean('is_fit')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('students_id')->references('id')->on('students')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fitnes_lists');
    }
}
