<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportExamLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_exam_levels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('report_exams_id')->unsigned();
            $table->string('title');
            $table->text('desc');
            $table->string('level_logo_url');
            $table->timestamps();

            $table->foreign('report_exams_id')->references('id')->on('report_exams')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_exam_levels');
    }
}
