<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportExamSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_exam_skills', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('report_exam_skill_groups_id')->unsigned();
            $table->string('name');
            $table->integer('score')->nullable();
            $table->integer('min_score');
            $table->integer('max_score');
            $table->timestamps();

            $table->foreign('report_exam_skill_groups_id')->references('id')->on('report_exam_skill_groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_exam_skills');
    }
}
