<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateReportMonthlysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_monthlys', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('students_id')->unsigned();
            $table->bigInteger('coaches_id')->unsigned()->nullable();
            $table->string('comment')->nullable();
            $table->string('month');
            $table->string('year');
            $table->string('age');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('students_id')->references('id')->on('students')->unsigned();
            $table->foreign('coaches_id')->references('id')->on('coaches')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_monthlys');
    }
}
