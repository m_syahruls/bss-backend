<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('users_id')->unsigned();
            $table->bigInteger('groups_id')->unsigned()->nullable();
            $table->string('registration_no');
            $table->string('name');
            $table->string('nick_name');
            $table->string('mother_name')->nullable();
            $table->string('mother_phone')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_phone')->nullable();
            $table->date('birthday');
            $table->string('img_url')->nullable();
            $table->string('joined_at')->nullable();
            $table->string('category')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('va_number')->nullable();
            $table->timestamps();


            $table->foreign('users_id')->references('id')->on('users')->unsigned();
            $table->foreign('groups_id')->references('id')->on('groups')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
